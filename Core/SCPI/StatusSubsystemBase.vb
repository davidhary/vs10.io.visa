﻿Imports NationalInstruments
Imports isr.Core.Primitives
Namespace Scpi

    ''' <summary> Defines the contract that must be implemented by SCPI Status Subsystem. </summary>
    ''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/26/2012" by="David" revision="1.0.4652"> Created. </history>
    Public MustInherit Class StatusSubsystemBase
        Inherits Visa.StatusSubsystemBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary> Initializes a new instance of the <see cref="StatusSubsystemBase" /> class. </summary>
        ''' <param name="visaSession"> A reference to a <see cref="Visa.Session">message based
        ''' session</see>. </param>
        Protected Sub New(ByVal visaSession As Visa.Session)
            MyBase.New(visaSession)

            ' check for query and other errors reported by the standard event register
            Me.StandardDeviceErrorAvailableBits = StandardEvents.CommandError Or StandardEvents.DeviceDependentError Or
                                                  StandardEvents.ExecutionError Or StandardEvents.QueryError
            ' Me.StandardServiceEnableCommandFormat = Ieee488.Syntax.StandardServiceEnableCommandFormat
            ' Me.StandardServiceEnableCompleteCommandFormat = Ieee488.Syntax.StandardServiceEnableCompleteCommandFormat

        End Sub

#End Region

#Region " I PRESETTABLE "

        ''' <summary> Performs a reset and additional custom setting for the subsystem. </summary>
        ''' <remarks> Override this method to customize the reset.<para>
        '''           Additional Actions: </para><para>
        '''           Clears Error Queue.
        '''           </para></remarks>
        Public Overrides Sub InitializeKnownState()
            Me.ClearErrorQueue()
            MyBase.InitializeKnownState()
        End Sub

        ''' <summary> Gets the subsystem value to their known execution preset state. </summary>
        Public Overrides Sub PresetKnownState()
            MyBase.PresetKnownState()
            Me.OperationEventEnableBitmask = 0
            Me.QuestionableEventEnableBitmask = 0
        End Sub

        ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
        Public Overrides Sub ResetKnownState()
            MyBase.ResetKnownState()
            Me.DeviceErrors = ""
            Me.MeasurementEventEnableBitmask = 0
            Me.MeasurementEventStatus = 0
            Me.OperationEventEnableBitmask = 0
            Me.OperationEventStatus = 0
            Me.QuestionableEventEnableBitmask = 0
            Me.QuestionableEventStatus = 0
            Me.StandardEventStatus = 0
            Me.StandardEventEnableBitmask = 0
        End Sub

#End Region

#Region " DEVICE REGISTERS "

        ''' <summary> Reads the standard registers based on the service request register status. </summary>
        Public Overrides Sub ReadRegisters()
            MyBase.ReadRegisters()
            If Me.StandardEventAvailable Then
                Me.QueryStandardEventStatus()
            End If
            If Me.MeasurementAvailable Then
                Me.QueryMeasurementEventStatus()
            End If
            If (Me.ServiceRequestStatus And ServiceRequests.OperationEvent) <> 0 Then
                Me.QueryOperationEventStatus()
            End If
            If (Me.ServiceRequestStatus And ServiceRequests.QuestionableEvent) <> 0 Then
                Me.QueryQuestionableEventStatus()
            End If
        End Sub

#End Region

#Region " MEASUREMENT REGISTER "

#Region " BITMASK"

        ''' <summary> The measurement event enable bitmask. </summary>
        Private _MeasurementEventEnableBitmask As Integer?

        ''' <summary> Gets or sets the cached value of the measurement register event enable bit mask. </summary>
        ''' <remarks> The returned value could be cast to the measurement events type that is specific to
        ''' the instrument The enable register gates the corresponding events for registration by the
        ''' Status Byte register. When an event bit is set and the corresponding enable bit is set, the
        ''' output (summary) of the register will set to 1, which in turn sets the summary bit of the
        ''' Status Byte Register. </remarks>
        ''' <value> The mask to use for enabling the events. </value>
        Public Property MeasurementEventEnableBitmask() As Integer?
            Get
                Return Me._MeasurementEventEnableBitmask
            End Get
            Protected Set(ByVal value As Integer?)
                If Not Nullable.Equals(Me.MeasurementEventEnableBitmask, value) Then
                    Me._MeasurementEventEnableBitmask = value
                    MyBase.AsyncNotifyPropertySetChanged(System.Reflection.MethodInfo.GetCurrentMethod.Name)
                End If
            End Set
        End Property

        ''' <summary> Programs and reads back the measurement register events enable bit mask. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> The bit mask or nothing if not known. </returns>
        Public Function ApplyMeasurementEventEnableBitmask(ByVal value As Integer) As Integer?
            Me.WriteMeasurementEventEnableBitmask(value)
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Return Me.MeasurementEventEnableBitmask
            Else
                Return Me.QueryMeasurementEventEnableBitmask()
            End If
        End Function

        ''' <summary> Reads back the measurement register event enable bit mask. </summary>
        ''' <remarks> The returned value could be cast to the measurement events type that is specific to
        ''' the instrument The enable register gates the corresponding events for registration by the
        ''' Status Byte register. When an event bit is set and the corresponding enable bit is set, the
        ''' output (summary) of the register will set to 1, which in turn sets the summary bit of the
        ''' Status Byte Register. </remarks>
        ''' <returns>  The mask used for enabling the events. </returns>
        Public Function QueryMeasurementEventEnableBitmask() As Integer?
            If Me.IsSessionOpen Then
                Me.MeasurementEventEnableBitmask = Me.Session.Query(0I, ":STAT:MEAS:ENAB?")
            End If
            Return Me.MeasurementEventEnableBitmask
        End Function

        ''' <summary> Programs the measurement register events enable bit mask without updating the value from
        ''' the device. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> The bit mask or nothing if not known. </returns>
        Public Function WriteMeasurementEventEnableBitmask(ByVal value As Integer) As Integer?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":STAT:MEAS:ENAB {0:D}", value)
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.MeasurementEventEnableBitmask = New Integer?
            Else
                Me.MeasurementEventEnableBitmask = value
            End If
            Return Me.MeasurementEventEnableBitmask
        End Function

#End Region

#Region " CONDITION "

        ''' <summary> The measurement event Condition. </summary>
        Private _MeasurementEventCondition As Integer?

        ''' <summary> Gets or sets the cached Condition of the measurement register events. </summary>
        ''' <value> <c>null</c> if value is not known;. </value>
        Public Property MeasurementEventCondition() As Integer?
            Get
                Return Me._MeasurementEventCondition
            End Get
            Protected Set(ByVal value As Integer?)
                If Not Nullable.Equals(Me.MeasurementEventCondition, value) Then
                    Me._MeasurementEventCondition = value
                    MyBase.AsyncNotifyPropertySetChanged(System.Reflection.MethodInfo.GetCurrentMethod.Name)
                End If
            End Set
        End Property

        ''' <summary> Gets or sets the measurement event condition query command. </summary>
        ''' <remarks> SCPI: ":STAT:MEAS:COND?".
        ''' <see cref="Scpi.Syntax.MeasurementEventConditionQueryCommand"> </see> </remarks>
        ''' <value> The measurement event condition query command. </value>
        Protected MustOverride ReadOnly Property MeasurementEventConditionQueryCommand As String

        ''' <summary> Reads the condition of the measurement register event. </summary>
        ''' <returns> System.Nullable{System.Int32}. </returns>
        Public Overridable Function QueryMeasurementEventCondition() As Integer?
            If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me.MeasurementEventConditionQueryCommand) Then
                Me.MeasurementEventCondition = Me.Session.Query(0I, Me.MeasurementEventConditionQueryCommand)
            End If
            Return Me.MeasurementEventCondition
        End Function

#End Region

#Region " STATUS "

        ''' <summary> The measurement event status. </summary>
        Private _MeasurementEventStatus As Integer?

        ''' <summary> Gets or sets the cached status of the measurement register events. </summary>
        ''' <value> <c>null</c> if value is not known;. </value>
        Public Property MeasurementEventStatus() As Integer?
            Get
                Return Me._MeasurementEventStatus
            End Get
            Protected Set(ByVal value As Integer?)
                Me._MeasurementEventStatus = value
                MyBase.AsyncNotifyPropertySetChanged(System.Reflection.MethodInfo.GetCurrentMethod.Name)
            End Set
        End Property

        ''' <summary> Gets or sets the measurement status query command. </summary>
        ''' <remarks> SCPI: ":STAT:MEAS:EVEN?".
        ''' <see cref="Scpi.Syntax.MeasurementEventQueryCommand"> </see> </remarks>
        ''' <value> The measurement status query command. </value>
        Protected MustOverride ReadOnly Property MeasurementStatusQueryCommand As String

        ''' <summary> Reads the status of the measurement register events. </summary>
        ''' <returns> System.Nullable{System.Int32}. </returns>
        Public Overridable Function QueryMeasurementEventStatus() As Integer?
            If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me.MeasurementStatusQueryCommand) Then
                Me.MeasurementEventStatus = Me.Session.Query(0I, Me.MeasurementStatusQueryCommand)
            End If
            Return Me.MeasurementEventStatus
        End Function
#End Region

#End Region

#Region " OPERATION REGISTER "

#Region " ENABLE BITMASK "

        ''' <summary> The operation event enable bitmask. </summary>
        Private _OperationEventEnableBitmask As Integer?

        ''' <summary> Gets or sets the cached value of the Operation register event enable bit mask. </summary>
        ''' <remarks> The returned value could be cast to the Operation events type that is specific to the
        ''' instrument The enable register gates the corresponding events for registration by the Status
        ''' Byte register. When an event bit is set and the corresponding enable bit is set, the output
        ''' (summary) of the register will set to 1, which in turn sets the summary bit of the Status
        ''' Byte Register. </remarks>
        ''' <value> The mask to use for enabling the events. </value>
        Public Property OperationEventEnableBitmask() As Integer?
            Get
                Return Me._OperationEventEnableBitmask
            End Get
            Protected Set(ByVal value As Integer?)
                If Not Nullable.Equals(Me.OperationEventEnableBitmask, value) Then
                    Me._OperationEventEnableBitmask = value
                    MyBase.AsyncNotifyPropertySetChanged(System.Reflection.MethodInfo.GetCurrentMethod.Name)
                End If
            End Set
        End Property

        ''' <summary> Programs and reads back the Operation register events enable bit mask. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> The bit mask or nothing if not known. </returns>
        Public Function ApplyOperationEventEnableBitmask(ByVal value As Integer) As Integer?
            Me.WriteOperationEventEnableBitmask(value)
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Return Me.OperationEventEnableBitmask
            Else
                Return Me.QueryOperationEventEnableBitmask()
            End If
        End Function

        ''' <summary> Gets or sets the operation event enable query command. </summary>
        ''' <remarks> SCPI: ":STAT:OPER:ENAB?". </remarks>
        ''' <value> The operation event enable command format. </value>
        Protected MustOverride ReadOnly Property OperationEventEnableQueryCommand As String

        ''' <summary> Queries the Operation register event enable bit mask. </summary>
        ''' <remarks> The returned value could be cast to the Operation events type that is specific to the
        ''' instrument The enable register gates the corresponding events for registration by the Status
        ''' Byte register. When an event bit is set and the corresponding enable bit is set, the output
        ''' (summary) of the register will set to 1, which in turn sets the summary bit of the Status
        ''' Byte Register. </remarks>
        ''' <returns> The mask to use for enabling the events; nothing if unknown </returns>
        Public Function QueryOperationEventEnableBitmask() As Integer?
            If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me.OperationEventEnableQueryCommand) Then
                Me.OperationEventEnableBitmask = Me.Session.Query(0I, Me.OperationEventEnableQueryCommand)
            End If
            Return Me.OperationEventEnableBitmask
        End Function

        ''' <summary> Gets or sets the operation event enable command format. </summary>
        ''' <remarks> SCPI: ":STAT:OPER:ENAB {0:D}".
        ''' <see cref="Scpi.Syntax.OperationEventEnableCommandFormat"> </see> </remarks>
        ''' <value> The operation event enable command format. </value>
        Protected MustOverride ReadOnly Property OperationEventEnableCommandFormat As String

        ''' <summary> Programs the Operation register event enable bit mask. </summary>
        ''' <remarks> When an event bit is set and the corresponding enable bit is set, the output
        ''' (summary) of the register will set to 1, which in turn sets the summary bit of the Status
        ''' Byte Register. </remarks>
        ''' <returns> The mask to use for enabling the events; nothing if unknown </returns>
        Public Overridable Function WriteOperationEventEnableBitmask(ByVal value As Integer) As Integer?
            If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me.OperationEventEnableCommandFormat) Then
                Me.Session.WriteLine(Me.OperationEventEnableCommandFormat, value)
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.OperationEventEnableBitmask = New Integer?
            Else
                Me.OperationEventEnableBitmask = value
            End If
            Return Me.OperationEventEnableBitmask
        End Function

#End Region

#Region " NEGATIVE TRANSITION EVENT ENABLE BITMASK "

        ''' <summary> The operation register negative transition events enable bitmask. </summary>
        Private _OperationNegativeTransitionEventEnableBitmask As Integer?

        ''' <summary> Gets or sets the operation negative transition event request. </summary>
        ''' <remarks> At this time this reads back the event status rather than the enable status. The
        ''' returned value could be cast to the Operation Transition events type that is specific to the
        ''' instrument. </remarks>
        ''' <value> The Operation Transition Events mask to use for enabling the events. </value>
        Public Property OperationNegativeTransitionEventEnableBitmask() As Integer?
            Get
                Return Me._OperationNegativeTransitionEventEnableBitmask
            End Get
            Protected Set(ByVal value As Integer?)
                If Not Nullable.Equals(Me.OperationNegativeTransitionEventEnableBitmask, value) Then
                    Me._OperationNegativeTransitionEventEnableBitmask = value
                    MyBase.AsyncNotifyPropertySetChanged(System.Reflection.MethodInfo.GetCurrentMethod.Name)
                End If
            End Set
        End Property

        ''' <summary> Queries the operation register negative transition events enable bit mask. </summary>
        ''' <remarks> The returned value could be cast to the Operation events type that is specific to the
        ''' instrument The enable register gates the corresponding events for registration by the Status
        ''' Byte register. When an event bit is set and the corresponding enable bit is set, the output
        ''' (summary) of the register will set to 1, which in turn sets the summary bit of the Status
        ''' Byte Register. </remarks>
        ''' <returns> The mask to use for enabling the events; nothing if unknown </returns>
        Public Overridable Function QueryOperationNegativeTransitionEventEnableBitmask() As Integer?
            If Me.IsSessionOpen Then
                Me.OperationNegativeTransitionEventEnableBitmask = Me.Session.Query(0I, ":STAT:OPER:NTR?")
            End If
            Return Me.OperationNegativeTransitionEventEnableBitmask
        End Function

        ''' <summary> Programs and reads back the operation register negative transition events enable bit mask. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> The bit mask or nothing if not known. </returns>
        Public Function ApplyOperationNegativeTransitionEventEnableBitmask(ByVal value As Integer) As Integer?
            Me.WriteOperationNegativeTransitionEventEnableBitmask(value)
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Return Me.OperationNegativeTransitionEventEnableBitmask
            Else
                Return Me.QueryOperationNegativeTransitionEventEnableBitmask()
            End If
        End Function

        ''' <summary> Programs the operation register negative transition events enable bit mask. </summary>
        ''' <remarks> When an event bit is set and the corresponding enable bit is set, the output
        ''' (summary) of the register will set to 1, which in turn sets the summary bit of the Status
        ''' Byte Register. </remarks>
        ''' <returns> The mask to use for enabling the events; nothing if unknown </returns>
        Public Overridable Function WriteOperationNegativeTransitionEventEnableBitmask(ByVal value As Integer) As Integer?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":STAT:OPER:NTR {0:D}", value)
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.OperationNegativeTransitionEventEnableBitmask = New Integer?
            Else
                Me.OperationNegativeTransitionEventEnableBitmask = value
            End If
            Return Me.OperationNegativeTransitionEventEnableBitmask
        End Function

#End Region

#Region " POSITIVE TRANSITION EVENT ENABLE BITMASK "

        ''' <summary> The operation register Positive transition events enable bitmask. </summary>
        Private _OperationPositiveTransitionEventEnableBitmask As Integer?

        ''' <summary> Gets or sets the cached value of the operation Positive transition event request. </summary>
        ''' <remarks> At this time this reads back the event status rather than the enable status. The
        ''' returned value could be cast to the Operation Transition events type that is specific to the
        ''' instrument. </remarks>
        ''' <value> The Operation Transition Events mask to use for enabling the events. </value>
        Public Property OperationPositiveTransitionEventEnableBitmask() As Integer?
            Get
                Return Me._OperationPositiveTransitionEventEnableBitmask
            End Get
            Protected Set(ByVal value As Integer?)
                If Not Nullable.Equals(Me.OperationPositiveTransitionEventEnableBitmask, value) Then
                    Me._OperationPositiveTransitionEventEnableBitmask = value
                    MyBase.AsyncNotifyPropertySetChanged(System.Reflection.MethodInfo.GetCurrentMethod.Name)
                End If
            End Set
        End Property

        ''' <summary> Queries the operation register Positive transition events enable bit mask. </summary>
        ''' <remarks> The returned value could be cast to the Operation events type that is specific to the
        ''' instrument The enable register gates the corresponding events for registration by the Status
        ''' Byte register. When an event bit is set and the corresponding enable bit is set, the output
        ''' (summary) of the register will set to 1, which in turn sets the summary bit of the Status
        ''' Byte Register. </remarks>
        ''' <returns> The mask to use for enabling the events; nothing if unknown </returns>
        Public Overridable Function QueryOperationPositiveTransitionEventEnableBitmask() As Integer?
            If Me.IsSessionOpen Then
                Me.OperationPositiveTransitionEventEnableBitmask = Me.Session.Query(0I, ":STAT:OPER:PTR?")
            End If
            Return Me.OperationPositiveTransitionEventEnableBitmask
        End Function

        ''' <summary> Programs and reads back the operation register Positive transition events enable bit mask. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> The bit mask or nothing if not known. </returns>
        Public Function ApplyOperationPositiveTransitionEventEnableBitmask(ByVal value As Integer) As Integer?
            Me.WriteOperationPositiveTransitionEventEnableBitmask(value)
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Return Me.OperationPositiveTransitionEventEnableBitmask
            Else
                Return Me.QueryOperationPositiveTransitionEventEnableBitmask()
            End If
        End Function

        ''' <summary> Programs the operation register Positive transition events enable bit mask. </summary>
        ''' <remarks> When an event bit is set and the corresponding enable bit is set, the output
        ''' (summary) of the register will set to 1, which in turn sets the summary bit of the Status
        ''' Byte Register. </remarks>
        ''' <returns> The mask to use for enabling the events; nothing if unknown </returns>
        Public Overridable Function WriteOperationPositiveTransitionEventEnableBitmask(ByVal value As Integer) As Integer?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":STAT:OPER:PTR {0:D}", value)
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.OperationPositiveTransitionEventEnableBitmask = New Integer?
            Else
                Me.OperationPositiveTransitionEventEnableBitmask = value
            End If
            Return Me.OperationPositiveTransitionEventEnableBitmask
        End Function

#End Region

#Region " CONDITION "

        ''' <summary> The operation event Condition. </summary>
        Private _OperationEventCondition As Integer?

        ''' <summary> Gets the Operation event register cached condition of the instrument. </summary>
        ''' <remarks> This value reflects the cached condition of the instrument. The returned value
        ''' could be cast to the Operation events type that is specific to the instrument The condition
        ''' register is a real-time, read-only register that constantly updates to reflect the present
        ''' operating conditions of the instrument. </remarks>
        ''' <value> The operation event condition. </value>
        Public Property OperationEventCondition() As Integer?
            Get
                Return Me._OperationEventCondition
            End Get
            Protected Set(ByVal value As Integer?)
                If Not Nullable.Equals(Me.OperationEventCondition, value) Then
                    Me._OperationEventCondition = value
                    MyBase.AsyncNotifyPropertySetChanged(System.Reflection.MethodInfo.GetCurrentMethod.Name)
                End If
            End Set
        End Property

        ''' <summary> Queries the Operation event register condition from the instrument. </summary>
        ''' <remarks> This value reflects the real time condition of the instrument. The returned value
        ''' could be cast to the Operation events type that is specific to the instrument The condition
        ''' register is a real-time, read-only register that constantly updates to reflect the present
        ''' operating conditions of the instrument. </remarks>
        ''' <returns> The operation event condition. </returns>
        Public Overridable Function QueryOperationEventCondition() As Integer?
            If Me.IsSessionOpen Then
                Me.OperationEventCondition = Me.Session.Query(0I, ":STAT:OPER:COND?")
            End If
            Return Me.OperationEventCondition
        End Function

#End Region

#Region " STATUS "

        ''' <summary> The operation event status. </summary>
        Private _OperationEventStatus As Integer?

        ''' <summary> Gets or sets the cached status of the Operation Register events. </summary>
        ''' <remarks> This query commands Queries the contents of the status event register. This value
        ''' indicates which bits are set. An event register bit is set to 1 when its event occurs. The
        ''' bit remains latched to 1 until the register is reset. Reading an event register clears the
        ''' bits of that register. *CLS resets all four event registers. </remarks>
        ''' <value> <c>null</c> if value is not known;. </value>
        Public Property OperationEventStatus() As Integer?
            Get
                Return Me._OperationEventStatus
            End Get
            Protected Set(ByVal value As Integer?)
                Me._OperationEventStatus = value
                MyBase.AsyncNotifyPropertySetChanged(System.Reflection.MethodInfo.GetCurrentMethod.Name)
            End Set
        End Property

        ''' <summary> Gets or sets the operation event status query command. </summary>
        ''' <remarks> SCPI: ":STAT:OPER:EVEN?".
        ''' <see cref="Scpi.Syntax.OperationEventQueryCommand"> </see> </remarks>
        ''' <value> The operation event status query command. </value>
        Protected MustOverride ReadOnly Property OperationEventStatusQueryCommand As String

        ''' <summary> Queries the status of the Operation Register events. </summary>
        ''' <remarks> This query commands Queries the contents of the status event register. This value
        ''' indicates which bits are set. An event register bit is set to 1 when its event occurs. The
        ''' bit remains latched to 1 until the register is reset. Reading an event register clears the
        ''' bits of that register. *CLS resets all four event registers. </remarks>
        ''' <returns> <c>null</c> if value is not known;. </returns>
        Public Overridable Function QueryOperationEventStatus() As Integer?
            If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me.OperationEventStatusQueryCommand) Then
                Me.OperationEventStatus = Me.Session.Query(0I, Me.OperationEventStatusQueryCommand)
            End If
            Return Me.OperationEventStatus
        End Function
#End Region

#End Region

#Region " QUESTIONABLE REGISTER "

#Region " BITMASK "

        ''' <summary> The questionable event enable bitmask. </summary>
        Private _QuestionableEventEnableBitmask As Integer?

        ''' <summary> Gets or sets the cached value of the Questionable register event enable bit mask. </summary>
        ''' <remarks> The returned value could be cast to the Questionable events type that is specific to
        ''' the instrument The enable register gates the corresponding events for registration by the
        ''' Status Byte register. When an event bit is set and the corresponding enable bit is set, the
        ''' output (summary) of the register will set to 1, which in turn sets the summary bit of the
        ''' Status Byte Register. </remarks>
        ''' <value> The mask to use for enabling the events. </value>
        Public Property QuestionableEventEnableBitmask() As Integer?
            Get
                Return Me._QuestionableEventEnableBitmask
            End Get
            Protected Set(ByVal value As Integer?)
                If Not Nullable.Equals(Me.QuestionableEventEnableBitmask, value) Then
                    Me._QuestionableEventEnableBitmask = value
                    Me.AsyncNotifyPropertySetChanged(System.Reflection.MethodInfo.GetCurrentMethod.Name)
                End If
            End Set
        End Property

        ''' <summary> Queries the status of the Questionable Register events. </summary>
        ''' <remarks> This query commands Queries the contents of the status event register. This value
        ''' indicates which bits are set. An event register bit is set to 1 when its event occurs. The
        ''' bit remains latched to 1 until the register is reset. Reading an event register clears the
        ''' bits of that register. *CLS resets all four event registers. </remarks>
        ''' <returns> <c>null</c> if value is not known;. </returns>
        Public Overridable Function QueryQuestionableEventEnableBitmask() As Integer?
            If Me.IsSessionOpen Then
                Me.QuestionableEventEnableBitmask = Me.Session.Query(0I, ":STAT:QUES:ENAB?")
            End If
            Return Me.QuestionableEventEnableBitmask
        End Function

        ''' <summary> Programs and reads back the Questionable register events enable bit mask. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> The bit mask or nothing if not known. </returns>
        Public Function ApplyQuestionableEventEnableBitmask(ByVal value As Integer) As Integer?
            Me.WriteQuestionableEventEnableBitmask(value)
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Return Me.QuestionableEventEnableBitmask
            Else
                Return Me.QueryQuestionableEventEnableBitmask()
            End If
        End Function

        ''' <summary> Writes the Questionable register events enable bit mask without updating the value from
        ''' the device. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> The bit mask or nothing if not known. </returns>
        Public Overridable Function WriteQuestionableEventEnableBitmask(ByVal value As Integer) As Integer?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":STAT:QUES:ENAB {0:D}", value)
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.QuestionableEventEnableBitmask = New Integer?
            Else
                Me.QuestionableEventEnableBitmask = value
            End If
            Return Me.QuestionableEventEnableBitmask
        End Function

#End Region

#Region " CONDITION "

        ''' <summary> The questionable event Condition. </summary>
        Private _QuestionableEventCondition As Integer?

        ''' <summary> Gets or sets the cached value of the Questionable event register condition of the instrument. </summary>
        ''' <remarks> This value reflects the real time condition of the instrument. The returned value
        ''' could be cast to the Questionable events type that is specific to the instrument The
        ''' condition register is a real-time, read-only register that constantly updates to reflect the
        ''' present operating conditions of the instrument. </remarks>
        ''' <value> The questionable event condition. </value>
        Public Property QuestionableEventCondition() As Integer?
            Get
                Return Me._QuestionableEventCondition
            End Get
            Protected Set(ByVal value As Integer?)
                If Not Nullable.Equals(Me.QuestionableEventCondition, value) Then
                    Me._QuestionableEventCondition = value
                    Me.AsyncNotifyPropertySetChanged(System.Reflection.MethodInfo.GetCurrentMethod.Name)
                End If
            End Set
        End Property

        ''' <summary> Gets the Questionable event register condition from the instrument. </summary>
        ''' <remarks> This value reflects the real time condition of the instrument. The returned value
        ''' could be cast to the Questionable events type that is specific to the instrument The
        ''' condition register is a real-time, read-only register that constantly updates to reflect the
        ''' present operating conditions of the instrument. </remarks>
        ''' <returns> The questionable event condition. </returns>
        Public Overridable Function QueryQuestionableEventCondition() As Integer?
            If Me.IsSessionOpen Then
                Me.QuestionableEventCondition = Me.Session.Query(0I, ":STAT:QUES:COND?")
            End If
            Return Me.QuestionableEventCondition
        End Function

#End Region

#Region " STATUS "

        ''' <summary> The questionable event status. </summary>
        Private _QuestionableEventStatus As Integer?

        ''' <summary> Gets or sets the status of the Questionable register events. </summary>
        ''' <value> <c>null</c> if value is not known;. </value>
        Public Property QuestionableEventStatus() As Integer?
            Get
                Return Me._QuestionableEventStatus
            End Get
            Protected Set(ByVal value As Integer?)
                If Not Nullable.Equals(Me.QuestionableEventStatus, value) Then
                    Me._QuestionableEventStatus = value
                    Me.AsyncNotifyPropertySetChanged(System.Reflection.MethodInfo.GetCurrentMethod.Name)
                End If
            End Set
        End Property

        ''' <summary> Gets or sets the questionable status query command. </summary>
        ''' <remarks> SCPI: ":STAT:QUES:EVEN?"
        ''' <see cref="Scpi.Syntax.QuestionableEventQueryCommand"> </see> </remarks>
        ''' <value> The questionable status query command. </value>
        Protected MustOverride ReadOnly Property QuestionableStatusQueryCommand As String

        ''' <summary> Reads the status of the Questionable register events. </summary>
        ''' <returns> System.Nullable{System.Int32}. </returns>
        Public Overridable Function QueryQuestionableEventStatus() As Integer?
            If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me.QuestionableStatusQueryCommand) Then
                Me.QuestionableEventStatus = Me.Session.Query(0I, Me.QuestionableStatusQueryCommand)
            End If
            Return Me.QuestionableEventStatus
        End Function

#End Region

#End Region

    End Class

End Namespace
