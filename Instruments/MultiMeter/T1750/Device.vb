﻿Imports NationalInstruments
Namespace T1750

    ''' <summary> Implements a Tegam 1750 Resistance Measuring System device. </summary>
    ''' <remarks> An instrument is defined, for the purpose of this library, as a device with a front
    ''' panel. </remarks>
    ''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="10/7/2013" by="David" revision=""> Created. </history>
    Public Class Device
        Inherits DeviceBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary> Initializes a new instance of the <see cref="K2700.Device" /> class. </summary>
        Public Sub New()
            MyBase.New()
            Me.InitializeTimeout = TimeSpan.FromMilliseconds(5000)
            Me.ResourcesSearchPattern = Visa.InstrumentSearchPattern(VisaNS.HardwareInterfaceType.Gpib)
        End Sub

#Region "IDisposable Support"

        ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        ''' <remarks> Executes in two distinct scenarios as determined by its disposing parameter.  If True,
        ''' the method has been called directly or indirectly by a user's code--managed and unmanaged
        ''' resources can be disposed. If disposing equals False, the method has been called by the
        ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
        ''' resources can be disposed. </remarks>
        ''' <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged resources;
        ''' False if this method releases only unmanaged resources. </param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Protected Overrides Sub Dispose(disposing As Boolean)
            Try
                If Not MyBase.IsDisposed Then
                    If disposing Then
                        ' dispose managed state (managed objects).
                        Me.OnClosing(New ComponentModel.CancelEventArgs)
                    End If
                End If
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, "Exception disposing device", "Exception details: {0}", ex)
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

#End Region

#End Region

#Region " I PRESETTABLE "

        ''' <summary> Clears the Device. Issues <see cref="StatusSubsystemBase.ClearActiveState">Selective
        ''' Device Clear</see>. </summary>
        Public Overrides Sub ClearActiveState()
            Me.StatusSubsystem.ClearActiveState()
        End Sub

#End Region

#Region " PUBLISHER "

        ''' <summary> Publishes all values by raising the property changed events. </summary>
        Public Overrides Sub Publish()
            Me.Subsystems.Publish()
            If Me.Publishable Then
                For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                    Me.AsyncNotifyPropertyChanged(p.Name)
                Next
            End If
        End Sub

#End Region

#Region " SESSION "

        Private _IsDeviceOpen As Boolean
        ''' <summary> Gets or sets a value indicating whether the device is open. This is
        '''           required when the device is used in emulation. </summary>
        ''' <value> <c>True</c> if the device has an open session; otherwise, <c>False</c>. </value>
        Public Overrides Property IsDeviceOpen As Boolean
            Get
                Return Me._IsDeviceOpen
            End Get
            Set(ByVal value As Boolean)
                If Not Me.IsDeviceOpen.Equals(value) Then
                    Me._IsDeviceOpen = value
                    Me.AsyncNotifyPropertyChanged("IsDeviceOpen")
                    If Me.StatusSubsystem IsNot Nothing Then
                        Me.StatusSubsystem.IsDeviceOpen = Me.IsDeviceOpen
                    End If
                End If
            End Set
        End Property

        ''' <summary> Allows the derived device to take actions after closing. </summary>
        Protected Overrides Sub OnClosed()
            MyBase.OnClosed()
        End Sub

        ''' <summary> Allows the derived device to take actions before closing. Removes subsystems and
        ''' event handlers. </summary>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        Protected Overrides Sub OnClosing(e As System.ComponentModel.CancelEventArgs)
            MyBase.OnClosing(e)
            If e Is Nothing OrElse Not e.Cancel Then
                If Me._MeasureSubsystem IsNot Nothing Then
                    'RemoveHandler Me._MeasureSubsystem.PropertyChanged, AddressOf Me.MeasureSubsystemPropertyChanged
                    Me._MeasureSubsystem.Dispose()
                    Me._MeasureSubsystem = Nothing
                End If
                If Me._SystemSubsystem IsNot Nothing Then
                    'RemoveHandler Me.SystemSubsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
                    AddHandler Me.SystemSubsystem.TraceMessageAvailable, AddressOf Me.OnTraceMessageAvailable
                    Me._SystemSubsystem.Dispose()
                    Me._SystemSubsystem = Nothing
                End If
                If Me._StatusSubsystem IsNot Nothing Then
                    Me.StatusSubsystem.IsDeviceOpen = False
                    RemoveHandler Me.StatusSubsystem.PropertyChanged, AddressOf Me.StatusSubsystemPropertyChanged
                    AddHandler Me.StatusSubsystem.TraceMessageAvailable, AddressOf Me.OnTraceMessageAvailable
                    Me._StatusSubsystem.Dispose()
                    Me._StatusSubsystem = Nothing
                End If
            End If
        End Sub

        ''' <summary> Allows the derived device to take actions after opening. Adds subsystems and event
        ''' handlers. </summary>
        Protected Overrides Sub OnOpened()

            ' prevent adding multiple times
            If MyBase.Subsystems IsNot Nothing AndAlso MyBase.Subsystems.Count <> 0 Then
                Debug.Assert(Not Debugger.IsAttached, "Unexpected attempt to add subsystems on top of subsystems")
            Else
                ' STATUS must be the first subsystem.
                Me._StatusSubsystem = New StatusSubsystem(MyBase.Session)
                MyBase.AddSubsystem(Me.StatusSubsystem)
                AddHandler Me.StatusSubsystem.PropertyChanged, AddressOf Me.StatusSubsystemPropertyChanged
                AddHandler Me.StatusSubsystem.TraceMessageAvailable, AddressOf Me.OnTraceMessageAvailable

                Me._SystemSubsystem = New SystemSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.SystemSubsystem)
                'AddHandler Me.SystemSubsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
                AddHandler Me.SystemSubsystem.TraceMessageAvailable, AddressOf Me.OnTraceMessageAvailable

                Me._MeasureSubsystem = New MeasureSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.MeasureSubsystem)
                'AddHandler Me._MeasureSubsystem.PropertyChanged, AddressOf Me.MeasureSubsystemPropertyChanged

            End If

            Me.StatusSubsystem.EnableServiceRequest(Me.StatusSubsystem.ServiceRequestEnableBitmask.Value) 
            MyBase.OnOpened()

        End Sub

        ''' <summary> Allows the derived device to take actions before opening. </summary>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        Protected Overrides Sub OnOpening(e As System.ComponentModel.CancelEventArgs)
            MyBase.OnOpening(e)
        End Sub

#End Region

#Region " SUBSYSTEMS "

#Region " MEASURE "

        ''' <summary> Gets or sets the Measure Subsystem. </summary>
        ''' <value> The Measure Subsystem. </value>
        Public Property MeasureSubsystem As MeasureSubsystem

#If False Then
        ''' <summary> Handle the Measure subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As MeasureSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                End Select
            End If
        End Sub

        ''' <summary> Measure subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub MeasureSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, MeasureSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling property '{0}'. Details: {1}.",
                             e.PropertyName, ex.Message)
            End Try
        End Sub
#End If

#End Region

#Region " STATUS "

        ''' <summary>
        ''' Gets or sets the Status Subsystem.
        ''' </summary>
        ''' <value>The Status Subsystem.</value>
        Public Property StatusSubsystem As StatusSubsystem

        ''' <summary> Handle the Status subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As StatusSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "Identity"
                        If Not String.IsNullOrWhiteSpace(subsystem.Identity) Then
                            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "{0} identified as {1}.",
                                                       Me.ResourceName, subsystem.Identity)

                        End If
                    Case "DeviceErrors"
                        If Not String.IsNullOrWhiteSpace(subsystem.DeviceErrors) Then
                            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "{0} Device errors: {1}",
                                                       Me.ResourceName, subsystem.DeviceErrors)
                        End If
                End Select
            End If
        End Sub

        ''' <summary> Status subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub StatusSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, StatusSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling property '{0}'. Details: {1}.",
                             e.PropertyName, ex.Message)
            End Try
        End Sub

#End Region

#Region " SYSTEM "

        ''' <summary>
        ''' Gets or sets the System Subsystem.
        ''' </summary>
        ''' <value>The System Subsystem.</value>
        Public Property SystemSubsystem As SystemSubsystem

#If False Then
        ''' <summary> Handle the System subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As SystemSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "LastError"
                        If subsystem.LastError.ErrorNumber > 0 Then
                            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                                       "Last error: {0}", subsystem.LastError.CompoundErrorMessage)
                        End If
                End Select
            End If
        End Sub

        ''' <summary> System subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub SystemSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, ProberSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling property '{0}'. Details: {1}.",
                             e.PropertyName, ex.Message)
            End Try
        End Sub
#End If

#End Region

#End Region

#Region " SERVICE REQUEST "

        ''' <summary> Reads the event registers after receiving a service request. </summary>
        Protected Overrides Sub ProcessServiceRequest()
            Me.StatusSubsystem.ReadRegisters()
            If Me.StatusSubsystem.ErrorAvailable Then
                Me.SystemSubsystem.QueryLastError()
            End If
        End Sub

#End Region

    End Class

End Namespace
