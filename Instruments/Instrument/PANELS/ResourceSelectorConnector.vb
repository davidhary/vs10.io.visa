Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Core.Controls.CheckBoxExtensions

''' <summary> A user interface for selecting and connecting to a VISA resource. </summary>
''' <remarks> Requires the use of the <see cref="ResourceSelectorConnectorWrapper">wrapper base form</see> with the designer.
''' <example> <code>
''' #Const designMode1 = True
''' #Region " BASE FROM WRAPPER "
'''     ' Designing requires changing the condition to True.
''' #If designMode Then
'''     Inherits ResourceSelectorConnectorWrapper
''' #Else
'''     Inherits ResourceSelectorConnector
''' #End If
''' #End Region
''' </code> </example> </remarks>
''' <license> (c) 2006 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/20/2006" by="David" revision="1.0.2242.x"> created. </history>
<Description("Resource Selector and Connector Base Control"), DefaultEvent("Connect")>
<System.Drawing.ToolboxBitmap(GetType(ResourceSelectorConnectorBase), "ResourceSelectorConnector"), ToolboxItem(True)>
Public MustInherit Class ResourceSelectorConnectorBase
    #Const designMode1 = True
#Region " BASE FROM WRAPPER "
    ' Designing requires changing the condition to True.
#If designMode Then
    Inherits isr.Core.Diagnosis.TracePublisherControlBaseWrapper
#Else
    Inherits isr.Core.Diagnosis.TracePublisherControlBase
#End If
#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Default constructor. </summary>
    Protected Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions
        'onInitialize()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        'onInstantiate()
        Me._ConnectToggle.Enabled = False
        Me._ClearButton.Enabled = False
        Me._FindButton.Enabled = True

    End Sub

    ''' <summary> Disposes managed resources. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub onDisposeManagedResources()

        If Me.ClearEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.ClearEvent.GetInvocationList
                Try
                    RemoveHandler Me.Clear, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                Catch
                End Try
            Next
        End If

        If Me.ConnectEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.ConnectEvent.GetInvocationList
                Try
                    RemoveHandler Me.Connect, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                Catch
                End Try
            Next
        End If

        If Me.DisconnectEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.DisconnectEvent.GetInvocationList
                Try
                    RemoveHandler Me.Disconnect, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                Catch
                End Try
            Next
        End If

        If Me.FindNamesEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.FindNamesEvent.GetInvocationList
                Try
                    RemoveHandler Me.FindNames, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                Catch
                End Try
            Next
        End If
    End Sub

#End Region

#Region " TYPES "

    ''' <summary> Enumerates the image indexes. </summary>
    Private Enum ImageIndex
        None
        Clear
        Connect
        Connected
        Search
    End Enum

#End Region

#Region " BROWSABLE PROPERTIES "

    Private _clearable As Boolean

    ''' <summary> Gets or sets the value indicating if the clear button is visible and can be enabled.
    ''' An item can be cleared only if it is connected. </summary>
    ''' <value> The clearable. </value>
    <Category("Appearance"), Description("Shows or hides the Clear button"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(True)>
    Public Property Clearable() As Boolean
        Get
            Return Me._clearable
        End Get
        Set(ByVal value As Boolean)
            If Not Me.Clearable.Equals(value) Then
                Me._clearable = value
                Me.AsyncNotifyPropertyChanged("Clearable")
            End If
            Me._ClearButton.Visible = value
        End Set
    End Property

    Private _connectible As Boolean
    ''' <summary> Gets or sets the value indicating if the connect button is visible and can be
    ''' enabled. An item can be connected only if it is selected. </summary>
    ''' <value> The connectible. </value>
    <Category("Appearance"), Description("Shows or hides the Connect button"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
    RefreshProperties(RefreshProperties.All), DefaultValue(True)>
    Public Property Connectible() As Boolean
        Get
            Return Me._connectible
        End Get
        Set(ByVal value As Boolean)
            If Not Me.Connectible.Equals(value) Then
                Me._connectible = value
                Me.AsyncNotifyPropertyChanged("Connectible")
            End If
            Me._ConnectToggle.Visible = value
            If Not value Then
                ' cannot clear a device if we do not connect to it.
                Me.Clearable = False
            End If
        End Set
    End Property

    Private _searchable As Boolean

    ''' <summary> Gets or sets the condition determining if the control can be searchable. The elements
    ''' can be searched only if not connected. </summary>
    ''' <value> The searchable. </value>
    <Category("Appearance"), Description("Shows or hides the Search (Find) button"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(True)>
    Public Property Searchable() As Boolean
        Get
            Return Me._searchable
        End Get
        Set(ByVal value As Boolean)
            If Not Me.Searchable.Equals(value) Then
                Me._searchable = value
                Me.AsyncNotifyPropertyChanged("Searchable")
            End If
            Me._FindButton.Visible = value
        End Set
    End Property

#End Region

#Region " CONNECT "

    ''' <summary> Gets or sets the connected status and enables the clear button. </summary>
    ''' <value> The is connected. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property IsConnected() As Boolean
        Get
            Return Me._ConnectToggle.Checked
        End Get
        Set(ByVal Value As Boolean)
            If Me._ConnectToggle.Checked <> Value Then
                Me._ConnectToggle.SilentCheckedSetter(Value)
                Me.AsyncNotifyPropertyChanged("IsConnected")
            End If

            ' enable or disable based on the connection status.
            Me._ResourceNamesComboBox.Enabled = Not Value
            Me._ClearButton.Enabled = Value
            Me._FindButton.Enabled = Not Value

        End Set
    End Property

#End Region

#Region " RESOURCE NAMES "

    Private _HasResources As Boolean
    ''' <summary> Gets or sets the has resources. </summary>
    ''' <value> The has resources. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property HasResources As Boolean
        Get
            Return _HasResources
        End Get
        Set(ByVal value As Boolean)
            If Not Me.HasResources.Equals(value) Then
                Me._HasResources = value
                Me.AsyncNotifyPropertyChanged("HasResources")
            End If
        End Set
    End Property

    Private _ResourcesSearchPattern As String
    ''' <summary> Gets or sets the resources search pattern. </summary>
    ''' <value> The resources search pattern. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ResourcesSearchPattern As String
        Get
            Return Me._ResourcesSearchPattern
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = ""
            If Not value.Equals(Me.ResourcesSearchPattern) Then
                Me._ResourcesSearchPattern = value
                Me._toolTip.SetToolTip(Me._FindButton, "Search using the search pattern '" & value & "'")
                Me.AsyncNotifyPropertyChanged("ResourcesSearchPattern")
            End If
        End Set
    End Property

    ''' <summary> Displays the resource names based on the <see cref="ResourcesSearchPattern">search pattern</see>. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Sub DisplayResourceNames()

        Dim resources() As String = New String() {}
        Me._ConnectToggle.Enabled = False
        Try
            Me._ErrorProvider.Clear()
            If String.IsNullOrWhiteSpace(Me.ResourcesSearchPattern) Then
                resources = NationalInstruments.VisaNS.ResourceManager.GetLocalManager.FindResources()
            Else
                resources = NationalInstruments.VisaNS.ResourceManager.GetLocalManager.FindResources(Me.ResourcesSearchPattern)
            End If

            If resources.Length = 0 Then

                Me.HasResources = False
                Dim tip As String = "No resources to select from"
                Me._toolTip.SetToolTip(Me._ResourceNamesComboBox, tip)

                Me._ErrorProvider.SetIconPadding(Me._FindButton, -15)
                Dim message As String = "VISA Manager found no hardware resources. Make sure at least on VISA instrument is connected. If this still fails, try entering a resource name such as GPIB0:16:INSTR and select OK."
                Me._ErrorProvider.SetError(Me._FindButton, message)
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, message)

            Else

                ' clear the interface names
                Me._ResourceNamesComboBox.DataSource = Nothing
                Me._ResourceNamesComboBox.Items.Clear()

                ' set the list of available names
                Me._ResourceNamesComboBox.DataSource = resources

                Me.HasResources = True
                Dim tip As String = "Select a name from the list"
                Me._toolTip.SetToolTip(Me._ResourceNamesComboBox, tip)

            End If

        Catch ex As Exception
            Me.HasResources = False
            Me._ErrorProvider.SetIconPadding(Me._FindButton, -15)
            Me._ErrorProvider.SetError(Me._FindButton, ex.Message)
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                       "The VISA resource manager failed fetching the list of local resources;. Make sure at least on VISA instrument is connected. If this still fails, try entering a resource name such as GPIB0:16:INSTR and select OK.{1}Details: {0}.",
                                       ex, Environment.NewLine)
        Finally
        End Try

    End Sub

    ''' <summary> Displays the search patters. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Sub DisplayResourceNamePatterns()

        ' clear the interface names
        Me._ResourceNamesComboBox.DataSource = Nothing
        Me._ResourceNamesComboBox.Items.Clear()

        Dim resourceList As New List(Of String)
        resourceList.Add("GPIB[board]::number[::INSTR]")
        resourceList.Add("GPIB[board]::INTFC")
        resourceList.Add("TCPIP[board]::host address[::LAN device name][::INSTR]")
        resourceList.Add("TCPIP[board]::host address::port::SOCKET")

        ' set the list of available names
        Me._ResourceNamesComboBox.DataSource = resourceList.ToArray

    End Sub

    ''' <summary> Gets the name of the entered resource. </summary>
    ''' <value> The name of the entered resource. </value>
    Private ReadOnly Property EnteredResourceName As String
        Get
            Return Me._ResourceNamesComboBox.Text.Trim
        End Get
    End Property

    Private _SelectedResourceName As String
    ''' <summary> Returns the selected resource name. </summary>
    ''' <value> The name of the selected. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property SelectedResourceName() As String
        Get
            Return Me._SelectedResourceName
        End Get
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification:="ok")>
        Set(ByVal Value As String)
            If String.IsNullOrWhiteSpace(Value) Then Value = ""
            If Not Value.Equals(Me.SelectedResourceName) Then
                Me._SelectedResourceName = Value
                Me.AsyncNotifyPropertyChanged("SelectedResourceName")
                If Not String.IsNullOrWhiteSpace(Value) Then
                    Try
                        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                        Me._ErrorProvider.Clear()
                        Me.SelectedResourceExists = NationalInstruments.VisaNS.ResourceManager.GetLocalManager.Exists(Value)
                    Catch ex As NationalInstruments.VisaNS.VisaException
                        Me._ErrorProvider.SetIconPadding(_ResourceNamesComboBox, -15)
                        Me._ErrorProvider.SetError(_ResourceNamesComboBox, ex.Message)
                        Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception setting selected resource;. Details:{0}.", ex)
                    Finally
                        Me.Cursor = System.Windows.Forms.Cursors.Default
                    End Try
                End If
            End If
            If Not Value.Equals(Me.EnteredResourceName) Then
                Me._ResourceNamesComboBox.Text = Value
            End If
        End Set
    End Property

    Private _SelectedResourceExists As Boolean
    ''' <summary> Gets or sets the has resources. </summary>
    ''' <value> The has resources. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SelectedResourceExists As Boolean
        Get
            Return _SelectedResourceExists
        End Get
        Private Set(ByVal value As Boolean)
            ' not checking for changed value here because this needs to be refreshed if a new
            ' resource was selected.
            Me._ConnectToggle.Enabled = Me.Connectible AndAlso value
            Me._SelectedResourceExists = value
            Me.AsyncNotifyPropertyChanged("SelectedResourceExists")
        End Set
    End Property

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Event handler. Called by _ResourceNamesComboBox for validated events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _ResourceNamesComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ResourceNamesComboBox.Validated
        Me._ErrorProvider.Clear()
        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Selected {0};. ", Me.EnteredResourceName)
        Me.SelectedResourceName = Me.EnteredResourceName
    End Sub

    ''' <summary> Selects a resource. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification:="ok")>
    Private Sub _ResourceNamesComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ResourceNamesComboBox.SelectedIndexChanged
        Me._ErrorProvider.Clear()
        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Selected {0};. ", Me.EnteredResourceName)
        Me.SelectedResourceName = Me.EnteredResourceName
    End Sub

    ''' <summary> Synchronously Invokes <see cref="OnClear">clear</see> to clear resources or whatever else needs
    ''' clearing. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification:="ok")>
    Private Sub _ClearButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ClearButton.Click
        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me._ErrorProvider.Clear()
            Me.OnClear(New EventArgs)
        Catch ex As Exception
            Dim c As Control = TryCast(sender, Control)
            If c IsNot Nothing Then
                Me._ErrorProvider.SetIconPadding(c, -15)
                Me._ErrorProvider.SetError(c, ex.Message)
            End If
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception clearing resource '{0}';. Details: {1}",
                                       Me.SelectedResourceName, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Synchronously Invokes <see cref="OnConnect">connecting</see> or
    ''' <see cref="OnDisconnect">disconnecting</see> </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification:="ok")>
    Private Sub _ConnectToggle_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ConnectToggle.CheckStateChanged
        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me._ErrorProvider.Clear()
            If Me._ConnectToggle.Enabled Then

                If Me._ConnectToggle.Checked Then
                    Me.OnConnect(New EventArgs)
                Else
                    Me.OnDisconnect(New EventArgs)
                End If

            End If

        Catch ex As Exception
            Dim c As Control = TryCast(sender, Control)
            If c IsNot Nothing Then
                Me._ErrorProvider.SetIconPadding(c, -15)
                Me._ErrorProvider.SetError(c, ex.Message)
            End If
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception connecting resource '{0}';. Details: {1}",
                                       Me.SelectedResourceName, ex)
        Finally

            If Me.IsConnected <> Me._ConnectToggle.Checked Then
                Me._ConnectToggle.Enabled = False
                Me._ConnectToggle.Checked = Me.IsConnected
                Me._ConnectToggle.Enabled = True
            End If
            If Me._ConnectToggle.Checked Then
                Me._toolTip.SetToolTip(Me._ConnectToggle, "Release to disconnect")
                Me._ConnectToggle.ImageIndex = ResourceSelectorConnectorBase.ImageIndex.Connected - 1
            Else
                Me._toolTip.SetToolTip(Me._ConnectToggle, "Press to connect")
                Me._ConnectToggle.ImageIndex = ResourceSelectorConnectorBase.ImageIndex.Connect - 1
            End If

            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Get the container to update the resource names. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification:="ok")>
    Private Sub _FindButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FindButton.Click
        Try
            Me._ErrorProvider.Clear()
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me.OnFindNames(New EventArgs)
        Catch ex As Exception
            Dim c As Control = TryCast(sender, Control)
            If c IsNot Nothing Then
                Me._ErrorProvider.SetIconPadding(c, -15)
                Me._ErrorProvider.SetError(c, ex.Message)
            End If
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception finding resources;. Details: {0}", ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

#End Region

#Region " EVENT HANDLERS "

    ''' <summary> Occurs when the clear button is clicked. </summary>
    Public Event Clear As EventHandler(Of EventArgs)

    ''' <summary> Raises the clear. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnClear(ByVal e As EventArgs)
        Dim evt As EventHandler(Of EventArgs) = Me.ClearEvent
        If evt IsNot Nothing Then evt.Invoke(Me, e)
    End Sub

    ''' <summary> Occurs when the connect button is depressed. </summary>
    Public Event Connect As EventHandler(Of EventArgs)

    ''' <summary> Raises the Connect event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnConnect(ByVal e As EventArgs)
        Dim evt As EventHandler(Of EventArgs) = Me.ConnectEvent
        If evt IsNot Nothing Then evt.Invoke(Me, e)
    End Sub

    ''' <summary> Occurs when the connect button is release. </summary>
    Public Event Disconnect As EventHandler(Of EventArgs)

    ''' <summary> Raises the Disconnect event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnDisconnect(ByVal e As EventArgs)
        Dim evt As EventHandler(Of EventArgs) = Me.DisconnectEvent
        If evt IsNot Nothing Then evt.Invoke(Me, e)
        Me.Searchable = True
    End Sub

    ''' <summary> Occurs when the find button is clicked. </summary>
    Public Event FindNames As EventHandler(Of EventArgs)

    ''' <summary> Raises the FindNames event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnFindNames(ByVal e As EventArgs)
        ' clear the selected resource to make sure a new selection is 
        ' made after find.
        Me.SelectedResourceName = ""
        Dim evt As EventHandler(Of EventArgs) = Me.FindNamesEvent
        If evt IsNot Nothing Then evt.Invoke(Me, e)
    End Sub

#End Region

End Class

''' <summary> Resource Panel base wrapper. </summary>
''' <remarks> This class is required to permit the use of the
''' <see cref="ResourceSelectorConnectorBase">base control</see> with the designer. or when using
''' the control in a form. </remarks>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="2/22/2014" by="David" revision=""> Created. </history>
''' <history date="02/08/2014" by="David">            > Created. </history>
<Description("Resource Selector Connector Control"), DefaultEvent("Connect")>
<System.Drawing.ToolboxBitmap(GetType(ResourceSelectorConnectorWrapper), "ResourceSelectorConnector"), ToolboxItem(True)>
Public Class ResourceSelectorConnectorWrapper
    Inherits ResourceSelectorConnectorBase

    ''' <summary> Displays the <paramref name="value">message</paramref>. </summary>
    ''' <param name="value"> The <see cref="Core.Diagnosis.TraceMessage">message</see> to display and log. </param>
    Protected Overrides Sub DisplayMessage(value As Core.Diagnosis.TraceMessage)
    End Sub

    ''' <summary> Displays the <paramref name="value">synopsis</paramref>. </summary>
    ''' <param name="value"> The synopsis to display. </param>
    Protected Overrides Sub DisplaySynopsis(value As String)
    End Sub

End Class
