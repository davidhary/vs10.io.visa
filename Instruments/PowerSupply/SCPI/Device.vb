﻿Imports NationalInstruments
Namespace SCPI

    ''' <summary> Implements a Power Supply device. </summary>
    ''' <remarks> An instrument is defined, for the purpose of this library, as a device with a front
    ''' panel. </remarks>
    ''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/26/2012" by="David" revision="1.0.4652"> Created. </history>
    Public Class Device
        Inherits DeviceBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary> Initializes a new instance of the <see cref="PowerSupply.SCPI.Device" /> class. </summary>
        Public Sub New()
            MyBase.New()
            Me.InitializeTimeout = TimeSpan.FromMilliseconds(5000)
            Me.ResourcesSearchPattern = Visa.InstrumentSearchPattern(VisaNS.HardwareInterfaceType.Gpib)
        End Sub

#Region " IDisposable Support "

        ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        ''' <remarks> Executes in two distinct scenarios as determined by its disposing parameter.  If True,
        ''' the method has been called directly or indirectly by a user's code--managed and unmanaged
        ''' resources can be disposed. If disposing equals False, the method has been called by the
        ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
        ''' resources can be disposed. </remarks>
        ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
        ''' False if this method releases only unmanaged resources. </param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Protected Overrides Sub Dispose(disposing As Boolean)
            Try
                If Not MyBase.IsDisposed Then
                    If disposing Then
                        ' dispose managed state (managed objects).
                        Me.OnClosing(New ComponentModel.CancelEventArgs)
                    End If
                End If
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, "Exception disposing device", "Exception details: {0}", ex)
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

#End Region

#End Region

#Region " I PRESETTABLE "

        ''' <summary> Clears the Device. Issues <see cref="StatusSubsystemBase.ClearActiveState">Selective
        ''' Device Clear</see>. </summary>
        Public Overrides Sub ClearActiveState()
            Me.StatusSubsystem.ClearActiveState()
        End Sub

#End Region

#Region " PUBLISHER "

        ''' <summary> Publishes all values by raising the property changed events. </summary>
        Public Overrides Sub Publish()
            Me.Subsystems.Publish()
            If Me.Publishable Then
                For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                    Me.AsyncNotifyPropertyChanged(p.Name)
                Next
            End If
        End Sub

#End Region

#Region " SESSION "

        Private _IsDeviceOpen As Boolean
        ''' <summary> Gets or sets a value indicating whether the device is open. This is
        '''           required when the device is used in emulation. </summary>
        ''' <value> <c>True</c> if the device has an open session; otherwise, <c>False</c>. </value>
        Public Overrides Property IsDeviceOpen As Boolean
            Get
                Return Me._IsDeviceOpen
            End Get
            Set(ByVal value As Boolean)
                If Not Me.IsDeviceOpen.Equals(value) Then
                    Me._IsDeviceOpen = value
                    Me.AsyncNotifyPropertyChanged("IsDeviceOpen")
                    If Me.StatusSubsystem IsNot Nothing Then
                        Me.StatusSubsystem.IsDeviceOpen = Me.IsDeviceOpen
                    End If
                End If
            End Set
        End Property

        ''' <summary> Allows the derived device to take actions after closing. </summary>
        Protected Overrides Sub OnClosed()
            MyBase.OnClosed()
        End Sub

        ''' <summary> Allows the derived device to take actions before closing. Removes subsystems and
        ''' event handlers. </summary>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        Protected Overrides Sub OnClosing(e As System.ComponentModel.CancelEventArgs)
            MyBase.OnClosing(e)
            If e Is Nothing OrElse Not e.Cancel Then
                If Me._SourceVoltageSubsystem IsNot Nothing Then
                    Me._SourceVoltageSubsystem.Dispose()
                    Me._SourceVoltageSubsystem = Nothing
                End If
                If Me._SourceCurrentSubsystem IsNot Nothing Then
                    Me._SourceCurrentSubsystem.Dispose()
                    Me._SourceCurrentSubsystem = Nothing
                End If
                If Me._OutputSubsystem IsNot Nothing Then
                    Me._OutputSubsystem.Dispose()
                    Me._OutputSubsystem = Nothing
                End If
                If Me._MeasureVoltageSubsystem IsNot Nothing Then
                    Me._MeasureVoltageSubsystem.Dispose()
                    Me._MeasureVoltageSubsystem = Nothing
                End If
                If Me._MeasureCurrentSubsystem IsNot Nothing Then
                    Me._MeasureCurrentSubsystem.Dispose()
                    Me._MeasureCurrentSubsystem = Nothing
                End If
                If Me._SystemSubsystem IsNot Nothing Then
                    'RemoveHandler Me.SystemSubsystem.PropertyChanged, AddressOf SystemSubsystemPropertyChanged
                    RemoveHandler Me.SystemSubsystem.TraceMessageAvailable, AddressOf Me.OnTraceMessageAvailable
                    Me._SystemSubsystem.Dispose()
                    Me._SystemSubsystem = Nothing
                End If
                If Me._StatusSubsystem IsNot Nothing Then
                    Me.StatusSubsystem.IsDeviceOpen = False
                    RemoveHandler Me.StatusSubsystem.PropertyChanged, AddressOf StatusSubsystemPropertyChanged
                    RemoveHandler Me.StatusSubsystem.TraceMessageAvailable, AddressOf Me.OnTraceMessageAvailable
                    Me._StatusSubsystem.Dispose()
                    Me._StatusSubsystem = Nothing
                End If
            End If
        End Sub

        ''' <summary> Allows the derived device to take actions after opening. Adds subsystems and event
        ''' handlers. </summary>
        Protected Overrides Sub OnOpened()

            ' prevent adding multiple times
            If MyBase.Subsystems IsNot Nothing AndAlso MyBase.Subsystems.Count <> 0 Then
                Debug.Assert(Not Debugger.IsAttached, "Unexpected attempt to add subsystems on top of subsystems")
            Else
                ' STATUS must be the first subsystem.
                Me._StatusSubsystem = New StatusSubsystem(MyBase.Session)
                MyBase.AddSubsystem(Me.StatusSubsystem)
                AddHandler Me.StatusSubsystem.PropertyChanged, AddressOf StatusSubsystemPropertyChanged
                AddHandler Me.StatusSubsystem.TraceMessageAvailable, AddressOf Me.OnTraceMessageAvailable

                Me._SystemSubsystem = New SystemSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.SystemSubsystem)
                'AddHandler Me.SystemSubsystem.PropertyChanged, AddressOf SystemSubsystemPropertyChanged
                AddHandler Me.SystemSubsystem.TraceMessageAvailable, AddressOf Me.OnTraceMessageAvailable

                Me._MeasureCurrentSubsystem = New MeasureCurrentSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.MeasureCurrentSubsystem)

                Me._MeasureVoltageSubsystem = New MeasureVoltageSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.MeasureVoltageSubsystem)

                Me._OutputSubsystem = New OutputSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.OutputSubsystem)

                Me._SourceCurrentSubsystem = New SourceCurrentSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.SourceCurrentSubsystem)

                Me._SourceVoltageSubsystem = New SourceVoltageSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.SourceVoltageSubsystem)
            End If

            Me.StatusSubsystem.EnableServiceRequest(ServiceRequests.All)
            MyBase.OnOpened()

        End Sub

        ''' <summary> Allows the derived device to take actions before opening. </summary>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        Protected Overrides Sub OnOpening(e As System.ComponentModel.CancelEventArgs)
            MyBase.OnOpening(e)
        End Sub

#End Region

#Region " SUBSYSTEMS "

        ''' <summary>
        ''' Gets or sets the Measure Current subsystem.
        ''' </summary>
        ''' <value>The Measure subsystem.</value>
        Public Property MeasureCurrentSubsystem As MeasureCurrentSubsystem

        ''' <summary>
        ''' Gets or sets the Measure Voltage subsystem.
        ''' </summary>
        ''' <value>The Measure subsystem.</value>
        Public Property MeasureVoltageSubsystem As MeasureVoltageSubsystem

        ''' <summary>
        ''' Gets or sets the Output subsystem.
        ''' </summary>
        ''' <value>The Output subsystem.</value>
        Public Property OutputSubsystem As OutputSubsystem

        ''' <summary>
        ''' Gets or sets the Source Current subsystem.
        ''' </summary>
        ''' <value>The Source subsystem.</value>
        Public Property SourceCurrentSubsystem As SourceCurrentSubsystem

        ''' <summary>
        ''' Gets or sets the Source Voltage subsystem.
        ''' </summary>
        ''' <value>The Source subsystem.</value>
        Public Property SourceVoltageSubsystem As SourceVoltageSubsystem

#Region " STATUS "

        ''' <summary> Gets or sets the Status Subsystem. </summary>
        ''' <value> The Status Subsystem. </value>
        Public Property StatusSubsystem As StatusSubsystem

        ''' <summary> Status subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub StatusSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso
                    e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
                    Dim subsystem As StatusSubsystem = CType(sender, StatusSubsystem)
                    Select Case e.PropertyName
                        Case "Identity"
                            If Not String.IsNullOrWhiteSpace(subsystem.Identity) Then
                                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "{0} identified as {1}.",
                                                           Me.ResourceName, subsystem.Identity)

                            End If
#If False Then
                        Case "DeviceErrors"
                        Case "ErrorAvailable"
                        Case "ErrorAvailableBits"
                        Case "MeasurementAvailable"
                        Case "MeasurementAvailableBits"
                        Case "MeasurementEventCondition"
                        Case "MeasurementEventEnableBitmask"
                        Case "MeasurementEventStatus"
                        Case "MessageAvailable"
                        Case "MessageAvailableBits"
                        Case "OperationEventCondition"
                        Case "OperationEventEnableBitmask"
                        Case "OperationEventStatus"
                        Case "OperationNegativeTransitionEventEnableBitmask"
                        Case "OperationPositiveTransitionEventEnableBitmask"
                        Case "QuestionableEventCondition"
                        Case "QuestionableEventEnableBitmask"
                        Case "QuestionableEventStatus"
                        Case "ServiceRequestEnableBitmask"
                        Case "ServiceRequestStatus"
                        Case "StandardDeviceErrorAvailable"
                        Case "StandardDeviceErrorAvailableBits"
                        Case "StandardEventEnableBitmask"
                        Case "StandardEventStatus"
                        Case "VersionInfo"
#End If
                    End Select
                End If
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling property '{0}'. Details: {1}.",
                             e.PropertyName, ex.Message)
            End Try
        End Sub

#End Region

#Region " SYSTEM "

        ''' <summary> Gets or sets the System Subsystem. </summary>
        ''' <value> The System Subsystem. </value>
        Public Property SystemSubsystem As SystemSubsystem

#If False Then
        ''' <summary> System subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub SystemSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
                    Select Case e.PropertyName
                        Case "LastError"
                        Case "LineFrequency"
                        Case "StationLineFrequency"
                    End Select
                End If
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling property '{0}'. Details: {1}.",
                             e.PropertyName, ex.Message)
            End Try
        End Sub
#End If

#End Region


#End Region

#Region " MIXED SUBSYSTEMS: OUTPUT / MEASURE "

        ''' <summary>
        ''' Sets the specified voltage and current limit and turns on the power supply.
        ''' </summary>
        ''' <param name="voltage">The voltage.</param>
        ''' <param name="currentLimit">The current limit.</param>
        ''' <param name="overvoltageLimit">The over voltage limit.</param>
        ''' <returns><c>True</c> if successful <see cref="VisaNS.VisaStatusCode">VISA Status Code</see>
        ''' was returned; <c>False</c> otherwise.</returns>
        ''' <remarks>OUTP ON</remarks>
        Public Function OutputOn(ByVal voltage As Double, ByVal currentLimit As Double, ByVal overvoltageLimit As Double) As Boolean

            ' Set the voltage
            Me.SourceVoltageSubsystem.ApplyLevel(voltage)

            ' Set the over voltage level
            Me.SourceVoltageSubsystem.ApplyProtectionLevel(overvoltageLimit)

            ' Turn on over current protection
            Me.SourceCurrentSubsystem.ApplyProtectionEnabled(True)

            ' Set the current level
            Me.SourceCurrentSubsystem.ApplyLevel(currentLimit)

            ' turn output on.
            Me.OutputSubsystem.ApplyOutputOnState(True)

            Me.StatusSubsystem.QueryOperationCompleted()

            ' operation completion is not sufficient to ensure that the Device has hit the correct voltage.

            Return Me.SystemSubsystem.LastStatus >= VisaNS.VisaStatusCode.Success

        End Function

        ''' <summary>
        ''' Turns the power supply on waiting for the voltage to reach the desired level.
        ''' </summary>
        ''' <param name="voltage">The voltage.</param>
        ''' <param name="currentLimit">The current limit.</param>
        ''' <param name="delta">The delta.</param>
        ''' <param name="voltageLimit">The over voltage limit.</param>
        ''' <param name="timeout">The timeout for awaiting for the voltage to reach the level.</param>
        ''' <returns><c>True</c> if successful <see cref="VisaNS.VisaStatusCode">VISA Status Code</see>
        ''' was returned; <c>False</c> otherwise.</returns>
        ''' <remarks>SCPI Command: OUTP ON</remarks>
        Public Function OutputOn(ByVal voltage As Double, ByVal currentLimit As Double,
                                 ByVal delta As Double, ByVal voltageLimit As Double, ByVal timeout As TimeSpan) As Boolean

            If Me.OutputOn(voltage, currentLimit, voltageLimit) Then
                Return Me.MeasureVoltageSubsystem.AwaitLevel(voltage, delta, timeout)
            Else
                Return False
            End If

        End Function

#End Region

#Region " SERVICE REQUEST "

        ''' <summary> Reads the event registers after receiving a service request. </summary>
        Protected Overrides Sub ProcessServiceRequest()
            Me.StatusSubsystem.ReadRegisters()
            If Me.StatusSubsystem.MessageAvailable Then
                ' if we have a message this needs to be processed by the subsystem requesting the message.
                ' Only thereafter the registers should be read.
            End If
            If Not Me.StatusSubsystem.MessageAvailable AndAlso Me.StatusSubsystem.ErrorAvailable Then
                Me.StatusSubsystem.QueryDeviceErrors()
            End If
            If Me.StatusSubsystem.MeasurementAvailable Then
            End If
        End Sub

#End Region

    End Class

End Namespace
