Imports System.ComponentModel
Imports isr.Core.Diagnosis

''' <summary> Provides a base user interface for a <see cref="isr.IO.Visa.DeviceBase">Visa Device</see>. </summary>
''' <remarks> Requires the use of the <see cref="ResourcePanelBaseWrapper">wrapper base form</see> with the designer.
''' <example> <code>
''' #Const designMode1 = True
''' #Region " BASE FROM WRAPPER "
'''     ' Designing requires changing the condition to True.
''' #If designMode Then
'''     Inherits ResourcePanelBaseWrapper
''' #Else
'''     Inherits ResourcePanelBase
''' #End If
''' #End Region
''' </code> </example> </remarks>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/04/2013" by="David" revision="3.0.4955"> created based on the legacy VISA
''' resource panel. </history>
<System.ComponentModel.Description("Resource Panel Base Control")>
<System.Drawing.ToolboxBitmap(GetType(ResourcePanelBase), "Panels.ResourcePanelBase.bmp"), ToolboxItem(True)>
Public MustInherit Class ResourcePanelBase
    #Const designMode1 = True
#Region " BASE FROM WRAPPER "
    ' Designing requires changing the condition to True.
#If designMode Then
    Inherits isr.Core.Diagnosis.TracePublisherControlBaseWrapper
#Else
    Inherits isr.Core.Diagnosis.TracePublisherControlBase
#End If
#End Region

    Implements ITraceMessageObserver

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    Protected Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions
        'onInitialize()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        Me.Connector.Connectible = True
        Me.Connector.Clearable = True
        Me.StatusRegisterToolStripStatusLabel.Visible = False
        Me.StandardRegisterToolStripStatusLabel.Visible = False
        Me.IdentityToolStripStatusLabel.Visible = False
        Me._StatusRegisterStatus = -1
        Me._StandardRegisterStatus = -1

    End Sub

    ''' <summary> Executes the dispose managed resources action. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub onDisposeManagedResources()

        If Me.Device IsNot Nothing Then
            Try
                If Me.IsDeviceOpen Then
                    Me.Device.DisableServiceRequestEventHandler()
                    RemoveHandler Me.Device.ServiceRequested, AddressOf Me.DeviceServiceRequested
                End If
                RemoveHandler Me.Device.PropertyChanged, AddressOf Me.DevicePropertyChanged
                RemoveHandler Me.Device.Opening, AddressOf Me.DeviceOpening
                RemoveHandler Me.Device.Opened, AddressOf Me.DeviceOpened
                RemoveHandler Me.Device.Closing, AddressOf Me.DeviceClosing
                RemoveHandler Me.Device.Closed, AddressOf Me.DeviceClosed
                Me._Device.Dispose()
                Me._Device = Nothing
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, "Exception occurred disposing panel resources", "Exception details: {0}", ex)
            End Try
        End If

    End Sub

#End Region

#Region " I TRACE MESSAGE OBSERVER "

    ''' <summary> Displays the <paramref name="value">synopsis</paramref>. </summary>
    ''' <param name="value"> The synopsis to display. </param>
    Protected Overrides Sub DisplaySynopsis(value As String)
        Me._StatusToolStripStatusLabel.Text = value
    End Sub

    ''' <summary> Observes the Trace event published by <see cref="ITraceMessagePublisher">trace publishers</see>. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="isr.Core.Diagnosis.TraceMessageEventArgs" /> instance containing the event data. </param>
    Protected Overloads Sub OnTraceMessageAvailable(ByVal sender As Object, ByVal e As TraceMessageEventArgs) Implements isr.Core.Diagnosis.ITraceMessageObserver.OnTraceMessageAvailable
        If sender IsNot Nothing AndAlso e IsNot Nothing Then
            Me.OnTraceMessageAvailable(e.TraceMessage)
        End If
    End Sub

    ''' <summary> Gets or sets the trace show level. The trace message is displayed if the trace level
    ''' is lower than this value. </summary>
    ''' <value> The trace show level. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected MustOverride ReadOnly Property TraceShowLevel As Diagnostics.TraceEventType Implements isr.Core.Diagnosis.ITraceMessageObserver.TraceShowLevel

    ''' <summary> Determines if the trace event type should be displayed. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <returns> <c>True</c> if the trace event type should be displayed. </returns>
    Protected Function ShouldShow(ByVal eventType As TraceEventType) As Boolean Implements ITraceMessageObserver.ShouldShow
        Return eventType <= Me.TraceShowLevel
    End Function


#End Region

#Region " REGISTER DISPLAY SETTERS "

    ''' <summary> Shows or hides the identity display. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub IdentityVisibleSetter(ByVal value As Boolean)
        isr.Core.Controls.ToolStripExtensions.SafeVisibleSetter(Me.IdentityToolStripStatusLabel, value)
    End Sub

    ''' <summary> Show or hide the status register display. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub StatusRegisterVisibleSetter(ByVal value As Boolean)
        isr.Core.Controls.ToolStripExtensions.SafeVisibleSetter(Me.StatusRegisterToolStripStatusLabel, value)
    End Sub

    ''' <summary> Displays the status register status using hex format. </summary>
    ''' <param name="value"> The register value. </param>
    Public Sub DisplayStatusRegisterStatus(ByVal value As Integer)
        Me.DisplayStatusRegisterStatus(value, "0x{0:X}")
    End Sub

    Private _StatusRegisterStatus As Integer
    ''' <summary> Displays the status register status. </summary>
    ''' <param name="value">  The register value. </param>
    ''' <param name="format"> The format. </param>
    Public Sub DisplayStatusRegisterStatus(ByVal value As Integer, ByVal format As String)
        If Not value.Equals(Me._StatusRegisterStatus) Then
            Me._StatusRegisterStatus = value
            isr.Core.Controls.ToolStripExtensions.SafeTextSetter(Me.StatusRegisterToolStripStatusLabel, value, format)
        End If
    End Sub

    ''' <summary> Shows or hides the Standard Register display. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub StandardRegisterVisibleSetter(ByVal value As Boolean)
        isr.Core.Controls.ToolStripExtensions.SafeVisibleSetter(Me.StandardRegisterToolStripStatusLabel, value)
    End Sub

    ''' <summary> Displays the standard register status. Uses Hex format. </summary>
    ''' <param name="value"> The register value. </param>
    Public Sub DisplayStandardRegisterStatus(ByVal value As Integer)
        Me.DisplayStandardRegisterStatus(value, "0x{0:X}")
    End Sub

    Private _StandardRegisterStatus As Integer
    ''' <summary> Displays the standard register status. </summary>
    ''' <param name="value">  The register value. </param>
    ''' <param name="format"> The format. </param>
    Public Sub DisplayStandardRegisterStatus(ByVal value As Integer?, ByVal format As String)
        If value.HasValue AndAlso Not value.Value.Equals(Me._StandardRegisterStatus) Then
            Me._StandardRegisterStatus = value.Value
            isr.Core.Controls.ToolStripExtensions.SafeTextSetter(Me.StandardRegisterToolStripStatusLabel, value.Value, format)
        End If
    End Sub

#End Region

#Region " RESOURCE NAME "

    Private _ResourceName As String

    ''' <summary> Gets or sets the name of the resource. </summary>
    ''' <value> The name of the resource. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ResourceName As String
        Get
            Return Me._ResourceName
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = ""
            If Not value.Equals(Me.ResourceName) Then
                Me._ResourceName = value
                isr.Core.Controls.ToolStripExtensions.SafeTextSetter(Me.IdentityToolStripStatusLabel, ResourceName)
                isr.Core.Controls.ToolStripExtensions.SafeToolTipTextSetter(Me.IdentityToolStripStatusLabel, ResourceName)
                MyBase.AsyncNotifyPropertyChanged("ResourceName")
            End If
            Me.Connector.SelectedResourceName = value
        End Set
    End Property

#End Region

#Region " DEVICE "

    Private _Device As IO.Visa.DeviceBase
    ''' <summary> Gets or sets reference to the VISA <see cref="IO.Visa.DeviceBase">device</see>
    ''' interfaces. </summary>
    ''' <value> The connectable resource. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected Property Device() As IO.Visa.DeviceBase
        Get
            Return Me._Device
        End Get
        Set(ByVal value As IO.Visa.DeviceBase)
            If value IsNot Nothing Then
                If Me._Device IsNot Nothing Then
                    RemoveHandler Me.Device.PropertyChanged, AddressOf Me.DevicePropertyChanged
                    RemoveHandler Me.Device.Opening, AddressOf Me.DeviceOpening
                    RemoveHandler Me.Device.Opened, AddressOf Me.DeviceOpened
                    RemoveHandler Me.Device.Closing, AddressOf Me.DeviceClosing
                    RemoveHandler Me.Device.Closed, AddressOf Me.DeviceClosed
                    RemoveHandler Me.Device.TraceMessageAvailable, AddressOf Me.OnTraceMessageAvailable
                End If
                Me._Device = value
                AddHandler Me.Device.TraceMessageAvailable, AddressOf Me.OnTraceMessageAvailable
                AddHandler Me.Device.PropertyChanged, AddressOf Me.DevicePropertyChanged
                AddHandler Me.Device.Opening, AddressOf Me.DeviceOpening
                AddHandler Me.Device.Opened, AddressOf Me.DeviceOpened
                AddHandler Me.Device.Closing, AddressOf Me.DeviceClosing
                AddHandler Me.Device.Closed, AddressOf Me.DeviceClosed
                If Me.Device.ResourcesSearchPattern IsNot Nothing Then
                    Me.Connector.ResourcesSearchPattern = Me.Device.ResourcesSearchPattern
                End If
            End If
        End Set
    End Property

    ''' <summary> Gets the is device open. </summary>
    ''' <value> The is device open. </value>
    Protected ReadOnly Property IsDeviceOpen As Boolean
        Get
            Return Me.Device IsNot Nothing AndAlso Me.Device.IsDeviceOpen
        End Get
    End Property

    ''' <summary> Gets the is session open. </summary>
    ''' <value> The is session open. </value>
    Protected ReadOnly Property IsSessionOpen As Boolean
        Get
            Return Me.Device IsNot Nothing AndAlso Me.Device.IsSessionOpen
        End Get
    End Property

    ''' <summary> Gets the connection status. </summary>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property IsConnected() As Boolean
        Get
            Return Me._Device IsNot Nothing AndAlso Me._Device.IsDeviceOpen
        End Get
    End Property

#End Region

#Region " DEVICE EVENT HANDLERS "

    ''' <summary> Event handler. Called when the device is requesting service. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Message based session event information. </param>
    Protected Overridable Sub DeviceServiceRequested(ByVal sender As Object, ByVal e As NationalInstruments.VisaNS.MessageBasedSessionEventArgs)
    End Sub

    ''' <summary> Event handler. Called for property changed events. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    Protected Overridable Sub DevicePropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
    End Sub

    ''' <summary> Event handler. Called upon device opening. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Overridable Sub DeviceOpening(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub

    ''' <summary> Enables service request event. </summary>
    Public Overridable Sub EnableServiceRequestEventHandler()
        If Me.IsDeviceOpen AndAlso Not Me.Device.ServiceRequestEventHandlerEnabled Then
            AddHandler Me.Device.ServiceRequested, AddressOf Me.DeviceServiceRequested
            Me.Device.EnableServiceRequestEventHandler()
        End If
    End Sub

    ''' <summary> Disables service request event. </summary>
    Public Overridable Sub DisableServiceRequestEventHandler()
        If Me.IsDeviceOpen AndAlso Me.Device.ServiceRequestEventHandlerEnabled Then
            RemoveHandler Me.Device.ServiceRequested, AddressOf Me.DeviceServiceRequested
            Me.Device.DisableServiceRequestEventHandler()
        End If
    End Sub

    ''' <summary> Event handler. Called when device opened. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Overridable Sub DeviceOpened(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Connector.IsConnected = True
        MyBase.SyncNotifyPropertyChanged("IsConnected")
        If Me.Device.Enabled Then
            If Me.IsSessionOpen Then
                Me.ResourceName = Me.Device.ResourceName
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Session is open;. ")
            Else
                Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Device is open but session is not;. ")
            End If
        End If
        Me.StatusRegisterToolStripStatusLabel.Text = "0x.."
        Me.StandardRegisterToolStripStatusLabel.Text = "0x.."
        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Connected;. ")
    End Sub

    ''' <summary> Event handler. Called when device is closing. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Overridable Sub DeviceClosing(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.DisableServiceRequestEventHandler()
    End Sub

    ''' <summary> Event handler. Called when device is closed. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Overridable Sub DeviceClosed(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Connector.IsConnected = False
        MyBase.SyncNotifyPropertyChanged("IsConnected")
        Me.StatusRegisterToolStripStatusLabel.Visible = False
        Me.StandardRegisterToolStripStatusLabel.Visible = False
        Me.IdentityToolStripStatusLabel.Visible = False
        Me.StandardRegisterVisibleSetter(False)
        Me.StatusRegisterVisibleSetter(False)
        If Me.Device IsNot Nothing Then
            Me.DisableServiceRequestEventHandler()
            If Me.Device.Enabled Then
                If Me.IsSessionOpen Then
                    Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Device closed but session still open;. ")
                Else
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Session close;. ")
                End If
            End If
        End If
        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Disconnected;. Device access closed.")
    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Occurs when the form is loaded.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.StatusToolStripStatusLabel.Text = "Find and select a resource."
        Me.IdentityToolStripStatusLabel.Text = String.Empty

    End Sub

#End Region

#Region " CONNECTOR EVENT HANDLERS "

    ''' <summary> Displays the resource names based on the device resource search pattern. </summary>
    Public Sub DisplayNames()
        ' get the list of available resources
        If Me.Device IsNot Nothing AndAlso Me.Device.ResourcesSearchPattern IsNot Nothing Then
            Me.Connector.ResourcesSearchPattern = Me.Device.ResourcesSearchPattern
        End If
        Me.Connector.DisplayResourceNames()
    End Sub

    ''' <summary> Clears the instrument by calling a propagating clear command. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Specifies the event arguments provided with the call. </param>
    Private Sub connector_Clear(ByVal sender As Object, ByVal e As System.EventArgs) Handles Connector.Clear
        Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Clearing resource;. Clearing the resource {0}", Me.Connector.SelectedResourceName)
        Me.Device.ResetAndClear()
        Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Resource cleared;. Resource {0} cleared", Me.Connector.SelectedResourceName)
    End Sub

    ''' <summary> Connects the instrument by calling a propagating connect command. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Specifies the event arguments provided with the call. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub connector_Connect(ByVal sender As Object, ByVal e As System.EventArgs) Handles Connector.Connect

        Try
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Opening {0};. ", Me.Connector.SelectedResourceName)
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
            Me.Device.OpenSession(Me.Connector.SelectedResourceName)
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Opened {0};. ", Me.Connector.SelectedResourceName)
        Catch ex As OperationFailedException
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Failed opening {0};. Details: {1}",
                                       Me.Connector.SelectedResourceName, ex)
        Catch ex As Exception
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception opening {0};. Details: {1}",
                                       Me.Connector.SelectedResourceName, ex)
        Finally
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary> Disconnects the instrument by calling a propagating disconnect command. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Specifies the event arguments provided with the call. </param>
    Private Sub connector_Disconnect(ByVal sender As Object, ByVal e As System.EventArgs) Handles Connector.Disconnect
        If Me.Device.IsDeviceOpen Then
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Releasing {0};. ", Me.Connector.SelectedResourceName)
        End If
        If Me.Device.TryCloseSession() Then
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Released {0};. ", Me.Connector.SelectedResourceName)
        Else
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Failed releasing {0};. ", Me.Connector.SelectedResourceName)
        End If
    End Sub

    ''' <summary> Displays available instrument names. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Specifies the event arguments provided with the call. </param>
    Private Sub connector_FindNames(ByVal sender As Object, ByVal e As System.EventArgs) Handles Connector.FindNames
        Me.DisplayNames()
    End Sub

    ''' <summary> Event handler. Called by _ResourceNameSelectorConnector for property changed
    ''' events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub connector_PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles Connector.PropertyChanged
        Try
            Select Case e.PropertyName
                Case "SelectedResourceName"
                    Me.ResourceName = Me.Connector.SelectedResourceName
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Selected {0};. ", Me.Connector.SelectedResourceName)
            End Select
        Catch ex As Exception
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception handling property changed Event;. Failed property {0}. Details: {1}",
                                       e.PropertyName, ex)
        End Try
    End Sub

    ''' <summary> Event handler. Called by Connector for trace message available events. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Trace message event information. </param>
    Private Sub Connector_TraceMessageAvailable(ByVal sender As Object, ByVal e As Core.Diagnosis.TraceMessageEventArgs) Handles Connector.TraceMessageAvailable
        Me.OnTraceMessageAvailable(e.TraceMessage)
    End Sub

#End Region

End Class

''' <summary> Resource Panel base wrapper. </summary>
''' <remarks> This class is required to permit the use of the
''' <see cref="ResourcePanelBase">base form</see> with the designer. </remarks>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="02/08/2014" by="David"> Created. </history>
Public Class ResourcePanelBaseWrapper
    Inherits ResourcePanelBase

    ''' <summary> Displays the <paramref name="value">message</paramref>. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Protected Overrides Sub DisplayMessage(value As TraceMessage)
    End Sub

    ''' <summary> Displays the <paramref name="value">synopsis</paramref>. </summary>
    ''' <param name="value"> The synopsis to display. </param>
    Protected Overrides Sub DisplaySynopsis(value As String)
    End Sub

    ''' <summary> Gets or sets the trace show level. The trace message is displayed if the trace level is lower
    ''' than this value. </summary>
    ''' <value> The trace show level. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected Overrides ReadOnly Property TraceShowLevel As Diagnostics.TraceEventType
        Get
            Return TraceEventType.Verbose
        End Get
    End Property

End Class
