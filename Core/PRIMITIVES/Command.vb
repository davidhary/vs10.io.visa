﻿Imports isr.Core.Primitives
''' <summary> Defines the commands for reading and/or writing instrument values. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="3/9/2014" by="David" revision=""> Created. </history>
Public Class Command

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.new()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="queryCommand">  The query command. </param>
    ''' <param name="writeCommand"> The Write command. </param>
    ''' <param name="maxValue">     The Max Value. </param>
    ''' <param name="minValue">     The Min Value. </param>
    ''' <param name="readFormat">   The read Format. Used for formatting a time span reading. </param>
    Public Sub New(ByVal queryCommand As String, ByVal writeCommand As String, ByVal maxValue As String, ByVal minValue As String, ByVal readFormat As String)
        MyBase.new()
        Me._QueryCommand = queryCommand
        Me._WriteCommand = writeCommand
        Me._MaxValue = maxValue
        Me._MinValue = minValue
        Me._ReadFormat = readFormat
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="baseCommand"> The base command. </param>
    ''' <param name="formats"> The formats. </param>
    ''' <remarks> Builds a complete command sets.<para>
    ''' For example, using the base command ":SENSE:RANGE"</para><para>
    ''' Sets the following:</para><para>
    ''' Read: ":SENSE:RANGE?"</para><para>
    ''' Write: ":SENSE:RANGE {0}"</para><para>
    ''' Max: "MAX"</para><para>
    ''' Min: "MIN"</para><para>
    ''' For TSP, using the base command "smua.voltage.range"</para><para>
    ''' Sets the following:</para><para>
    ''' Read: "_G.print(smua.voltage.range)"</para><para>
    ''' Write: "smua.voltage.range()"</para><para>
    ''' Max: "smua.voltage.range.max"</para><para>
    ''' Min: "smua.voltage.range.min"</para><para>
    ''' </para>
    '''           </remarks>
    Public Sub New(ByVal baseCommand As String, ByVal formats As CommandFormats)
        MyBase.new()
        If (formats And CommandFormats.Standard) <> 0 Then
            If (formats And CommandFormats.QuerySupported) <> 0 Then
                Me._QueryCommand = baseCommand & "?"
            End If
            If (formats And CommandFormats.WriteSupported) <> 0 Then
                If (formats And CommandFormats.WriteOneZero) <> 0 Then
                    Me._WriteCommand = baseCommand & " {0:'1';'1';'0'}"
                ElseIf (formats And CommandFormats.WriteOnOff) <> 0 Then
                    Me._WriteCommand = baseCommand & " {0:'ON';'ON';'OFF'}"
                Else
                    Me._WriteCommand = baseCommand & " {0}"
                End If
            End If
            If (formats And CommandFormats.MaxSupported) <> 0 Then
                Me._MaxValue = "MAX"
            End If
            If (formats And CommandFormats.MinSupported) <> 0 Then
                Me._MinValue = "MIN"
            End If
        ElseIf (formats And CommandFormats.TestScriptBuilder) <> 0 Then
            If (formats And CommandFormats.QuerySupported) <> 0 Then
                Me._QueryCommand = String.Format("_G.print({0}", baseCommand)
            End If
            If (formats And CommandFormats.WriteSupported) <> 0 Then
                If (formats And CommandFormats.WriteOneZero) <> 0 Then
                    Me._WriteCommand = baseCommand & " {0:'1';'1';'0'}"
                ElseIf (formats And CommandFormats.WriteOnOff) <> 0 Then
                    Me._WriteCommand = baseCommand & " {0:'ON';'ON';'OFF'}"
                Else
                    Me._WriteCommand = baseCommand & " {0}"
                End If
            End If
            If (formats And CommandFormats.MaxSupported) <> 0 Then
                Me._MaxValue = baseCommand & ".max"
            End If
            If (formats And CommandFormats.MinSupported) <> 0 Then
                Me._MaxValue = baseCommand & ".min"
            End If
        Else
            Me._QueryCommand = baseCommand
        End If
    End Sub

    Private _QueryCommand As String

    ''' <summary> Gets the query command. </summary>
    ''' <value> The query command. </value>
    Public ReadOnly Property QueryCommand As String
        Get
            Return _QueryCommand
        End Get
    End Property

    ''' <summary> Gets the query supported. </summary>
    ''' <value> The query supported sentinel. </value>
    Public ReadOnly Property QuerySupported As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.QueryCommand)
        End Get
    End Property

    Private _ReadFormat As String
    ''' <summary> Gets the read format. </summary>
    ''' <value> The read format. </value>
    Public ReadOnly Property ReadFormat As String
        Get
            Return _ReadFormat
        End Get
    End Property

    Private _WriteCommand As String
    ''' <summary> Gets the Write command. </summary>
    ''' <value> The Write command. </value>
    Public ReadOnly Property WriteCommand As String
        Get
            Return _WriteCommand
        End Get
    End Property

    ''' <summary> Gets the Write supported sentinel. </summary>
    ''' <value> The Write supported. </value>
    Public ReadOnly Property WriteSupported As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.WriteCommand)
        End Get
    End Property

    Private _MaxValue As String
    ''' <summary> Gets the Max Value. </summary>
    ''' <value> The Max Value. </value>
    Public ReadOnly Property MaxValue As String
        Get
            Return _MaxValue
        End Get
    End Property

    ''' <summary> Gets the Max Value supported sentinel. </summary>
    ''' <value> The Max value supported. </value>
    Public ReadOnly Property MaxSupported As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.MaxValue)
        End Get
    End Property

    Private _MinValue As String
    ''' <summary> Gets the Min Value. </summary>
    ''' <value> The Min Value. </value>
    Public ReadOnly Property MinValue As String
        Get
            Return _MinValue
        End Get
    End Property

    ''' <summary> Gets the Min Value supported sentinel. </summary>
    ''' <value> The Min value supported. </value>
    Public ReadOnly Property MinSupported As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.MinValue)
        End Get
    End Property

End Class

''' <summary> Enumerates the Command Formats. </summary>
<Flags()> Public Enum CommandFormats
    None
    <ComponentModel.Description("Standard SCPI format")> Standard = 1
    <ComponentModel.Description("Test Script Builder format")> TestScriptBuilder = 2
    <ComponentModel.Description("Query supported")> QuerySupported = 4
    <ComponentModel.Description("Write supported")> WriteSupported = 8
    <ComponentModel.Description("Max supported")> MaxSupported = 16
    <ComponentModel.Description("Min supported")> MinSupported = 32
    <ComponentModel.Description("One/Zero write format")> WriteOneZero = 64
    <ComponentModel.Description("On/Off write format")> WriteOnOff = 128
    <ComponentModel.Description("Standard All")> StandardAll = Standard + QuerySupported + WriteSupported + MaxSupported + MinSupported
    <ComponentModel.Description("Test Script Builder All")> TestScriptBuilderAll = TestScriptBuilder + QuerySupported + WriteSupported + MaxSupported + MinSupported
End Enum