﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class ServiceRequester
    Inherits isr.Core.WindowsForms.UserFormBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                Me.onDisposeManagedResources()
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._ReadingGroupBox = New System.Windows.Forms.GroupBox()
        Me._ServiceRequestByteTextBox = New System.Windows.Forms.TextBox()
        Me._ServiceRequestByteTextBoxLabel = New System.Windows.Forms.Label()
        Me._MessageStatusBitValueNumeric = New System.Windows.Forms.NumericUpDown()
        Me._MessageStatusBitsNumericLabel = New System.Windows.Forms.Label()
        Me._timingUnitsLabel = New System.Windows.Forms.Label()
        Me._timingTextBox = New System.Windows.Forms.TextBox()
        Me.clearButton = New System.Windows.Forms.Button()
        Me.readTextBox = New System.Windows.Forms.TextBox()
        Me.selectResourceLabel = New System.Windows.Forms.Label()
        Me._EnableServiceRequestButton = New System.Windows.Forms.Button()
        Me.writeTextBox = New System.Windows.Forms.TextBox()
        Me.commandTextBox = New System.Windows.Forms.TextBox()
        Me.commandLabel = New System.Windows.Forms.Label()
        Me.resetButton = New System.Windows.Forms.Button()
        Me.writeButton = New System.Windows.Forms.Button()
        Me.writingGroupBox = New System.Windows.Forms.GroupBox()
        Me.configuringGroupBox = New System.Windows.Forms.GroupBox()
        Me._TimeoutNumeric = New System.Windows.Forms.NumericUpDown()
        Me._TimeoutNumericLabel = New System.Windows.Forms.Label()
        Me._syncCallBacksCheckBox = New System.Windows.Forms.CheckBox()
        Me.deviceConfigureButton = New System.Windows.Forms.Button()
        Me.resourceNameLabel = New System.Windows.Forms.Label()
        Me.resourceNameTextBox = New System.Windows.Forms.TextBox()
        Me.toolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._ReadingGroupBox.SuspendLayout()
        CType(Me._MessageStatusBitValueNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.writingGroupBox.SuspendLayout()
        Me.configuringGroupBox.SuspendLayout()
        CType(Me._TimeoutNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_ReadingGroupBox
        '
        Me._ReadingGroupBox.Controls.Add(Me._ServiceRequestByteTextBox)
        Me._ReadingGroupBox.Controls.Add(Me._ServiceRequestByteTextBoxLabel)
        Me._ReadingGroupBox.Controls.Add(Me._MessageStatusBitValueNumeric)
        Me._ReadingGroupBox.Controls.Add(Me._MessageStatusBitsNumericLabel)
        Me._ReadingGroupBox.Controls.Add(Me._timingUnitsLabel)
        Me._ReadingGroupBox.Controls.Add(Me._timingTextBox)
        Me._ReadingGroupBox.Controls.Add(Me.clearButton)
        Me._ReadingGroupBox.Controls.Add(Me.readTextBox)
        Me._ReadingGroupBox.Location = New System.Drawing.Point(344, 132)
        Me._ReadingGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ReadingGroupBox.Name = "_ReadingGroupBox"
        Me._ReadingGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ReadingGroupBox.Size = New System.Drawing.Size(317, 207)
        Me._ReadingGroupBox.TabIndex = 3
        Me._ReadingGroupBox.TabStop = False
        Me._ReadingGroupBox.Text = "Reading"
        '
        '_ServiceRequestByteTextBox
        '
        Me._ServiceRequestByteTextBox.Font = New System.Drawing.Font(Me.Font, System.Drawing.FontStyle.Bold)
        Me._ServiceRequestByteTextBox.Location = New System.Drawing.Point(206, 132)
        Me._ServiceRequestByteTextBox.Name = "_ServiceRequestByteTextBox"
        Me._ServiceRequestByteTextBox.ReadOnly = True
        Me._ServiceRequestByteTextBox.Size = New System.Drawing.Size(72, 25)
        Me._ServiceRequestByteTextBox.TabIndex = 7
        Me._ServiceRequestByteTextBox.Text = "0x00"
        '
        '_ServiceRequestByteTextBoxLabel
        '
        Me._ServiceRequestByteTextBoxLabel.AutoSize = True
        Me._ServiceRequestByteTextBoxLabel.Location = New System.Drawing.Point(34, 136)
        Me._ServiceRequestByteTextBoxLabel.Name = "_ServiceRequestByteTextBoxLabel"
        Me._ServiceRequestByteTextBoxLabel.Size = New System.Drawing.Size(170, 17)
        Me._ServiceRequestByteTextBoxLabel.TabIndex = 6
        Me._ServiceRequestByteTextBoxLabel.Text = "Service Request Status Byte:"
        '
        '_MessageStatusBitValueNumeric
        '
        Me._MessageStatusBitValueNumeric.Hexadecimal = True
        Me._MessageStatusBitValueNumeric.Location = New System.Drawing.Point(256, 23)
        Me._MessageStatusBitValueNumeric.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me._MessageStatusBitValueNumeric.Name = "_MessageStatusBitValueNumeric"
        Me._MessageStatusBitValueNumeric.Size = New System.Drawing.Size(51, 25)
        Me._MessageStatusBitValueNumeric.TabIndex = 1
        Me._MessageStatusBitValueNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._MessageStatusBitValueNumeric.Value = New Decimal(New Integer() {16, 0, 0, 0})
        '
        '_MessageStatusBitsNumericLabel
        '
        Me._MessageStatusBitsNumericLabel.AutoSize = True
        Me._MessageStatusBitsNumericLabel.Location = New System.Drawing.Point(65, 27)
        Me._MessageStatusBitsNumericLabel.Name = "_MessageStatusBitsNumericLabel"
        Me._MessageStatusBitsNumericLabel.Size = New System.Drawing.Size(189, 17)
        Me._MessageStatusBitsNumericLabel.TabIndex = 0
        Me._MessageStatusBitsNumericLabel.Text = "Message Status Bit Value [hex]:"
        '
        '_timingUnitsLabel
        '
        Me._timingUnitsLabel.AutoSize = True
        Me._timingUnitsLabel.Location = New System.Drawing.Point(280, 172)
        Me._timingUnitsLabel.Name = "_timingUnitsLabel"
        Me._timingUnitsLabel.Size = New System.Drawing.Size(25, 17)
        Me._timingUnitsLabel.TabIndex = 5
        Me._timingUnitsLabel.Text = "ms"
        '
        '_timingTextBox
        '
        Me._timingTextBox.Font = New System.Drawing.Font(Me.Font, System.Drawing.FontStyle.Bold)
        Me._timingTextBox.Location = New System.Drawing.Point(205, 168)
        Me._timingTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._timingTextBox.Name = "_timingTextBox"
        Me._timingTextBox.Size = New System.Drawing.Size(73, 25)
        Me._timingTextBox.TabIndex = 4
        '
        'clearButton
        '
        Me.clearButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.clearButton.Font = New System.Drawing.Font(Me.Font, System.Drawing.FontStyle.Bold)
        Me.clearButton.Location = New System.Drawing.Point(9, 165)
        Me.clearButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.clearButton.Name = "clearButton"
        Me.clearButton.Size = New System.Drawing.Size(121, 31)
        Me.clearButton.TabIndex = 3
        Me.clearButton.Text = "Clear"
        '
        'readTextBox
        '
        Me.readTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.readTextBox.Location = New System.Drawing.Point(10, 55)
        Me.readTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.readTextBox.Multiline = True
        Me.readTextBox.Name = "readTextBox"
        Me.readTextBox.ReadOnly = True
        Me.readTextBox.Size = New System.Drawing.Size(298, 68)
        Me.readTextBox.TabIndex = 2
        Me.readTextBox.Text = "If not working, an SDC or *RST or *CLS are required line in the GPIB Test Panel. " & _
    " Run the GPIB Test panel and try again."
        '
        'selectResourceLabel
        '
        Me.selectResourceLabel.Location = New System.Drawing.Point(9, 13)
        Me.selectResourceLabel.Name = "selectResourceLabel"
        Me.selectResourceLabel.Size = New System.Drawing.Size(653, 44)
        Me.selectResourceLabel.TabIndex = 0
        Me.selectResourceLabel.Text = "Select the Resource Name associated with your device and press the Configure Devi" & _
    "ce button. Then enter the command string that enables SRQ and click the Enable S" & _
    "RQ button."
        '
        '_EnableServiceRequestButton
        '
        Me._EnableServiceRequestButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._EnableServiceRequestButton.Font = New System.Drawing.Font(Me.Font, System.Drawing.FontStyle.Bold)
        Me._EnableServiceRequestButton.Location = New System.Drawing.Point(187, 240)
        Me._EnableServiceRequestButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._EnableServiceRequestButton.Name = "_EnableServiceRequestButton"
        Me._EnableServiceRequestButton.Size = New System.Drawing.Size(121, 31)
        Me._EnableServiceRequestButton.TabIndex = 7
        Me._EnableServiceRequestButton.Text = "Enable SRQ"
        '
        'writeTextBox
        '
        Me.writeTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.writeTextBox.Font = New System.Drawing.Font(Me.Font, System.Drawing.FontStyle.Bold)
        Me.writeTextBox.Location = New System.Drawing.Point(9, 31)
        Me.writeTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.writeTextBox.Name = "writeTextBox"
        Me.writeTextBox.Size = New System.Drawing.Size(177, 25)
        Me.writeTextBox.TabIndex = 0
        Me.writeTextBox.Text = "*IDN?\n"
        '
        'commandTextBox
        '
        Me.commandTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.commandTextBox.Font = New System.Drawing.Font(Me.Font, System.Drawing.FontStyle.Bold)
        Me.commandTextBox.Location = New System.Drawing.Point(9, 243)
        Me.commandTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.commandTextBox.Name = "commandTextBox"
        Me.commandTextBox.Size = New System.Drawing.Size(177, 25)
        Me.commandTextBox.TabIndex = 6
        Me.commandTextBox.Text = "*SRE 16"
        '
        'commandLabel
        '
        Me.commandLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.commandLabel.Location = New System.Drawing.Point(9, 153)
        Me.commandLabel.Name = "commandLabel"
        Me.commandLabel.Size = New System.Drawing.Size(299, 86)
        Me.commandLabel.TabIndex = 5
        Me.commandLabel.Text = "Type the command to enable the instrument's SRQ event on MAV or clear this field " & _
    "if the instrument service request is already enabled and no command is required " & _
    "or allowed then click Enable SRQ:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'resetButton
        '
        Me.resetButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.resetButton.Font = New System.Drawing.Font(Me.Font, System.Drawing.FontStyle.Bold)
        Me.resetButton.Location = New System.Drawing.Point(186, 110)
        Me.resetButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.resetButton.Name = "resetButton"
        Me.resetButton.Size = New System.Drawing.Size(121, 31)
        Me.resetButton.TabIndex = 4
        Me.resetButton.Text = "Reset"
        '
        'writeButton
        '
        Me.writeButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.writeButton.Font = New System.Drawing.Font(Me.Font, System.Drawing.FontStyle.Bold)
        Me.writeButton.Location = New System.Drawing.Point(187, 28)
        Me.writeButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.writeButton.Name = "writeButton"
        Me.writeButton.Size = New System.Drawing.Size(121, 30)
        Me.writeButton.TabIndex = 1
        Me.writeButton.Text = "Write"
        '
        'writingGroupBox
        '
        Me.writingGroupBox.Controls.Add(Me.writeButton)
        Me.writingGroupBox.Controls.Add(Me.writeTextBox)
        Me.writingGroupBox.Location = New System.Drawing.Point(344, 56)
        Me.writingGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.writingGroupBox.Name = "writingGroupBox"
        Me.writingGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.writingGroupBox.Size = New System.Drawing.Size(317, 67)
        Me.writingGroupBox.TabIndex = 2
        Me.writingGroupBox.TabStop = False
        Me.writingGroupBox.Text = "Writing"
        '
        'configuringGroupBox
        '
        Me.configuringGroupBox.Controls.Add(Me._TimeoutNumeric)
        Me.configuringGroupBox.Controls.Add(Me._TimeoutNumericLabel)
        Me.configuringGroupBox.Controls.Add(Me._syncCallBacksCheckBox)
        Me.configuringGroupBox.Controls.Add(Me._EnableServiceRequestButton)
        Me.configuringGroupBox.Controls.Add(Me.commandTextBox)
        Me.configuringGroupBox.Controls.Add(Me.commandLabel)
        Me.configuringGroupBox.Controls.Add(Me.resetButton)
        Me.configuringGroupBox.Controls.Add(Me.deviceConfigureButton)
        Me.configuringGroupBox.Controls.Add(Me.resourceNameLabel)
        Me.configuringGroupBox.Controls.Add(Me.resourceNameTextBox)
        Me.configuringGroupBox.Location = New System.Drawing.Point(9, 56)
        Me.configuringGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.configuringGroupBox.Name = "configuringGroupBox"
        Me.configuringGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.configuringGroupBox.Size = New System.Drawing.Size(317, 284)
        Me.configuringGroupBox.TabIndex = 1
        Me.configuringGroupBox.TabStop = False
        Me.configuringGroupBox.Text = "Configuring"
        '
        '_TimeoutNumeric
        '
        Me._TimeoutNumeric.Location = New System.Drawing.Point(239, 76)
        Me._TimeoutNumeric.Maximum = New Decimal(New Integer() {30000, 0, 0, 0})
        Me._TimeoutNumeric.Name = "_TimeoutNumeric"
        Me._TimeoutNumeric.Size = New System.Drawing.Size(68, 25)
        Me._TimeoutNumeric.TabIndex = 9
        Me._TimeoutNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._TimeoutNumeric.Value = New Decimal(New Integer() {3000, 0, 0, 0})
        '
        '_TimeoutNumericLabel
        '
        Me._TimeoutNumericLabel.AutoSize = True
        Me._TimeoutNumericLabel.Location = New System.Drawing.Point(150, 80)
        Me._TimeoutNumericLabel.Name = "_TimeoutNumericLabel"
        Me._TimeoutNumericLabel.Size = New System.Drawing.Size(87, 17)
        Me._TimeoutNumericLabel.TabIndex = 8
        Me._TimeoutNumericLabel.Text = "Timeout [ms]:"
        '
        '_syncCallBacksCheckBox
        '
        Me._syncCallBacksCheckBox.AutoSize = True
        Me._syncCallBacksCheckBox.Location = New System.Drawing.Point(184, 16)
        Me._syncCallBacksCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._syncCallBacksCheckBox.Name = "_syncCallBacksCheckBox"
        Me._syncCallBacksCheckBox.Size = New System.Drawing.Size(114, 21)
        Me._syncCallBacksCheckBox.TabIndex = 2
        Me._syncCallBacksCheckBox.Text = "Sync Call Backs"
        Me.toolTip.SetToolTip(Me._syncCallBacksCheckBox, "Defaults to true with a new session.")
        Me._syncCallBacksCheckBox.UseVisualStyleBackColor = True
        '
        'deviceConfigureButton
        '
        Me.deviceConfigureButton.Font = New System.Drawing.Font(Me.Font, System.Drawing.FontStyle.Bold)
        Me.deviceConfigureButton.Location = New System.Drawing.Point(8, 110)
        Me.deviceConfigureButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.deviceConfigureButton.Name = "deviceConfigureButton"
        Me.deviceConfigureButton.Size = New System.Drawing.Size(121, 31)
        Me.deviceConfigureButton.TabIndex = 3
        Me.deviceConfigureButton.Text = "Configure Device"
        '
        'resourceNameLabel
        '
        Me.resourceNameLabel.Location = New System.Drawing.Point(9, 21)
        Me.resourceNameLabel.Name = "resourceNameLabel"
        Me.resourceNameLabel.Size = New System.Drawing.Size(131, 21)
        Me.resourceNameLabel.TabIndex = 0
        Me.resourceNameLabel.Text = "Resource Name:"
        '
        'resourceNameTextBox
        '
        Me.resourceNameTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.resourceNameTextBox.Font = New System.Drawing.Font(Me.Font, System.Drawing.FontStyle.Bold)
        Me.resourceNameTextBox.Location = New System.Drawing.Point(9, 42)
        Me.resourceNameTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.resourceNameTextBox.Name = "resourceNameTextBox"
        Me.resourceNameTextBox.Size = New System.Drawing.Size(298, 25)
        Me.resourceNameTextBox.TabIndex = 1
        Me.resourceNameTextBox.Text = "GPIB0::26::INSTR"
        '
        'ServiceRequester
        '
        Me.ClientSize = New System.Drawing.Size(674, 353)
        Me.Controls.Add(Me._ReadingGroupBox)
        Me.Controls.Add(Me.selectResourceLabel)
        Me.Controls.Add(Me.writingGroupBox)
        Me.Controls.Add(Me.configuringGroupBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.Name = "ServiceRequester"
        Me.Text = "Service Request Tester"
        Me._ReadingGroupBox.ResumeLayout(False)
        Me._ReadingGroupBox.PerformLayout()
        CType(Me._MessageStatusBitValueNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.writingGroupBox.ResumeLayout(False)
        Me.writingGroupBox.PerformLayout()
        Me.configuringGroupBox.ResumeLayout(False)
        Me.configuringGroupBox.PerformLayout()
        CType(Me._TimeoutNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _ReadingGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents clearButton As System.Windows.Forms.Button
    Private WithEvents readTextBox As System.Windows.Forms.TextBox
    Private WithEvents selectResourceLabel As System.Windows.Forms.Label
    Private WithEvents _EnableServiceRequestButton As System.Windows.Forms.Button
    Private WithEvents writeTextBox As System.Windows.Forms.TextBox
    Private WithEvents commandTextBox As System.Windows.Forms.TextBox
    Private WithEvents commandLabel As System.Windows.Forms.Label
    Private WithEvents resetButton As System.Windows.Forms.Button
    Private WithEvents writeButton As System.Windows.Forms.Button
    Private WithEvents writingGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents configuringGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents deviceConfigureButton As System.Windows.Forms.Button
    Private WithEvents resourceNameLabel As System.Windows.Forms.Label
    Private WithEvents resourceNameTextBox As System.Windows.Forms.TextBox
    Private WithEvents toolTip As System.Windows.Forms.ToolTip
    Private WithEvents _syncCallBacksCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _timingUnitsLabel As System.Windows.Forms.Label
    Private WithEvents _timingTextBox As System.Windows.Forms.TextBox
    Private WithEvents _MessageStatusBitValueNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _MessageStatusBitsNumericLabel As System.Windows.Forms.Label
    Private WithEvents _TimeoutNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _TimeoutNumericLabel As System.Windows.Forms.Label
    Private WithEvents _ServiceRequestByteTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ServiceRequestByteTextBoxLabel As System.Windows.Forms.Label
End Class
