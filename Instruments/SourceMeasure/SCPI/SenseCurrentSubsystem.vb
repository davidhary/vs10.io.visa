Imports NationalInstruments
Namespace Scpi

    ''' <summary> Defines a SCPI Sense Current Subsystem for a generic Source Measure instrument . </summary>
    ''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/26/2012" by="David" revision="1.0.4652"> Created. </history>
    Public Class SenseCurrentSubsystem
        Inherits Visa.Scpi.SenseCurrentSubsystemBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary> Initializes a new instance of the <see cref="SenseCurrentSubsystem" /> class. </summary>
        ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">message based
        ''' session</see>. </param>
        Public Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
            MyBase.New(statusSubsystem)
        End Sub

#End Region

#Region " I PRESETTABLE "

        ''' <summary> Sets subsystem values to their known execution clear state. </summary>
        Public Overrides Sub ClearExecutionState()
        End Sub

#End Region

#Region " PUBLISHER "

        ''' <summary> Publishes all values by raising the property changed events. </summary>
        Public Overrides Sub Publish()
            If Me.Publishable Then
                For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                    Me.AsyncNotifyPropertyChanged(p.Name)
                Next
            End If
        End Sub

#End Region

#Region " COMMAND SYNTAX "

        ''' <summary> Gets the preset command. </summary>
        ''' <value> The preset command. </value>
        Protected Overrides ReadOnly Property PresetCommand As String
            Get
                Return ""
            End Get
        End Property

#Region " AUTO RANGE "

        ''' <summary> Gets the automatic Range enabled command Format. </summary>
        ''' <value> The automatic Range enabled query command. </value>
        Protected Overrides ReadOnly Property AutoRangeEnabledCommandFormat As String
            Get
                Return ":SENS:CURR:RANG:AUTO {0:'ON';'ON';'OFF'}"
            End Get
        End Property

        ''' <summary> Gets the automatic Range enabled query command. </summary>
        ''' <value> The automatic Range enabled query command. </value>
        Protected Overrides ReadOnly Property AutoRangeEnabledQueryCommand As String
            Get
                Return ":SENS:CURR:RANG:AUTO?"
            End Get
        End Property

#End Region

#Region " POWER LINE CYCLES "

        ''' <summary> Gets The Power Line Cycles command format. </summary>
        ''' <value> The Power Line Cycles command format. </value>
        Protected Overrides ReadOnly Property PowerLineCyclesCommandFormat As String
            Get
                Return ":SENS:CURR:NPLC {0}"
            End Get
        End Property

        ''' <summary> Gets The Power Line Cycles query command. </summary>
        ''' <value> The Power Line Cycles query command. </value>
        Protected Overrides ReadOnly Property PowerLineCyclesQueryCommand As String
            Get
                Return ":SENS:CURR:NPLC?"
            End Get
        End Property

#End Region

#Region " PROTECTION LEVEL "

        ''' <summary> Gets the protection level command format. </summary>
        ''' <value> the protection level command format. </value>
        Protected Overrides ReadOnly Property ProtectionLevelCommandFormat As String
            Get
                Return ":SENS:CURR:PROT {0}"
            End Get
        End Property

        ''' <summary> Gets the protection level query command. </summary>
        ''' <value> the protection level query command. </value>
        Protected Overrides ReadOnly Property ProtectionLevelQueryCommand As String
            Get
                Return ":SENS:CURR:PROT?"
            End Get
        End Property

#End Region

#Region " RANGE "

        ''' <summary> Gets the range command format. </summary>
        ''' <value> The range command format. </value>
        Protected Overrides ReadOnly Property RangeCommandFormat As String
            Get
                Return ":SENS:CURR:RANG {0}"
            End Get
        End Property

        ''' <summary> Gets the range query command. </summary>
        ''' <value> The range query command. </value>
        Protected Overrides ReadOnly Property RangeQueryCommand As String
            Get
                Return ":SENS:CURR:RANG?"
            End Get
        End Property

#End Region

#End Region

    End Class

End Namespace
