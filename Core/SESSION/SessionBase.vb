﻿Imports System.Threading
Imports System.ComponentModel
Imports NationalInstruments
Imports isr.Core.Primitives.EnumExtensions

''' <summary> A Message-Based Session class implementing the
''' <see cref="Visa.Session">message based session interface</see>. </summary>
''' <remarks> Message-based sessions access VISA resources in an interface-independent manner. </remarks>
''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/2005" by="David" revision="1.0.1841.x"> Created. </history>
Public Class Session
    Inherits VisaNS.MessageBasedSession
    Implements INotifyPropertyChanged

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Initializes a new instance of the MessageBasedSession object from the specified
    ''' resource name. </summary>
    ''' <param name="resourceName"> String that describes a unique VISA resource. </param>
    Public Sub New(ByVal resourceName As String)
        Me.New(resourceName, TimeSpan.FromMilliseconds(10000))
    End Sub

    ''' <summary> Initializes a new instance of the MessageBasedSession object from the specified
    ''' resource name, access mode, and timeout. </summary>
    ''' <param name="resourceName"> String that describes a unique VISA resource. </param>
    ''' <param name="accessMode">   Mode by which the specified VISA resource is accessed. </param>
    ''' <param name="openTimeout">  Maximum time that this method waits to open a VISA session with
    ''' the specified resource name. This parameter does not set the Timeout property. </param>
    Public Sub New(ByVal resourceName As String, ByVal accessMode As VisaNS.AccessModes, ByVal openTimeout As TimeSpan)
        MyBase.New(resourceName, accessMode, CInt(openTimeout.TotalMilliseconds))
        Me._Timeouts = New Collections.Generic.Stack(Of Integer)
        Me._MessageAvailableBits = ServiceRequests.MessageAvailable
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="Session" /> class. </summary>
    ''' <remarks> This method does not lock the resource. Rev 4.1 and 5.0 of VISA did not support this
    ''' call and could not verify the resource.  </remarks>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <param name="openTimeout">  The open timeout. </param>
    Public Sub New(ByVal resourceName As String, ByVal openTimeout As TimeSpan)
        Me.New(resourceName, VisaNS.AccessModes.NoLock, openTimeout)
    End Sub

#Region " Disposable Support"

    ''' <summary> Gets the disposed value. </summary>
    ''' <value> The disposed value. </value>
    Public Property Disposed As Boolean ' To detect redundant calls disposable

    ''' <summary> Releases the unmanaged resources used by Session and optionally releases the managed
    ''' resources associated with this object. </summary>
    ''' <param name="disposing"> <c>True</c> if this method releases managed and unmanaged resources; false
    ''' if this method releases only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.Disposed Then
                If disposing Then

                    Try
                        MyBase.DiscardEvent(VisaNS.MessageBasedSessionEventType.AllEnabledEvents)
                    Catch ex As Exception
                        Debug.Assert(Not Debugger.IsAttached, "Failed discarding enabled events.", "Failed discarding enabled events. Details: {0}", ex)
                    End Try

                    If Me.PropertyChangedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.PropertyChangedEvent.GetInvocationList
                            Try
                                RemoveHandler Me.PropertyChanged, CType(d, System.ComponentModel.PropertyChangedEventHandler)
                            Catch ex As Exception
                                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                            End Try
                        Next
                    End If

                    If Not Me.Timeouts Is Nothing Then
                        Me.Timeouts.Clear()
                        Me._Timeouts = Nothing
                    End If
                End If
            End If
        Finally
            Me.Disposed = True
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#End Region

#Region " VISA STATUS "

    ''' <summary> Builds a VISA warning or error message. </summary>
    ''' <returns> The visa status details. </returns>
    Public Function BuildVisaStatusDetails() As String
        Return VisaException.BuildVisaStatusDetails(MyBase.LastStatus)
    End Function

#End Region

#Region " VISA RESOURCE "

    ''' <summary> Gets the name of the interface base. </summary>
    ''' <value> The name of the interface base. </value>
    Public ReadOnly Property InterfaceBaseName As String
        Get
            Return ResourceManagerExtensions.InterfaceResourceBaseName(Me.HardwareInterfaceType)
        End Get
    End Property

    ''' <summary> Gets the name of the interface resource. </summary>
    ''' <returns> The name of the interface resource. </returns>
    Public Function InterfaceResourceName() As String
        If Me.HardwareInterfaceType = NationalInstruments.VisaNS.HardwareInterfaceType.Gpib Then
            Return ResourceManagerExtensions.InterfaceResourceName(Me.InterfaceBaseName, Me.HardwareInterfaceNumber)
        Else
            Throw New NotImplementedException("Interface resource name is available only for GPIB interfaces at this time")
        End If
    End Function

#End Region

#Region " QUERY "

    ''' <summary> Performs a synchronous write of ASCII-encoded string data, followed by a synchronous
    ''' read. </summary>
    ''' <param name="dataToWrite"> The data to write. </param>
    ''' <returns> The  <see cref="LastMessageReceived">last received data</see>. </returns>
    Public Overloads Function Query(ByVal dataToWrite As String) As String
        If String.IsNullOrWhiteSpace(dataToWrite) Then
            Me.LastMessageReceived = Nothing
        Else
            Me.WriteLine(dataToWrite)
            If Me.LastStatus >= VisaNS.VisaStatusCode.Success Then
                Return Me.ReadLine()
            Else
                Me.LastMessageReceived = Nothing
            End If
        End If
        Return Me.LastMessageReceived
    End Function

    ''' <summary> Performs a synchronous write of ASCII-encoded string data, followed by a synchronous
    ''' read. </summary>
    ''' <param name="format"> The format of the data to write. </param>
    ''' <param name="args">   The format arguments. </param>
    ''' <returns> The  <see cref="LastMessageReceived">last received data</see>. </returns>
    Public Overloads Function Query(ByVal format As String, ByVal ParamArray args() As Object) As String
        If String.IsNullOrWhiteSpace(format) Then
            Me.LastMessageReceived = Nothing
        Else
            Me.WriteLine(format, args)
            If Me.LastStatus >= VisaNS.VisaStatusCode.Success Then
                Return Me.ReadLine()
            Else
                Me.LastMessageReceived = Nothing
            End If
        End If
        Return Me.LastMessageReceived
    End Function

    ''' <summary> Performs a synchronous write of ASCII-encoded string data, followed by a synchronous
    ''' read. </summary>
    ''' <param name="dataToWrite"> The data to write. </param>
    ''' <returns> The <see cref="LastMessageReceived">last received data</see> without the
    ''' <see cref="TerminationCharacter">termination character</see>. </returns>
    Public Overloads Function QueryTrimEnd(ByVal dataToWrite As String) As String
        Return Me.Query(dataToWrite).TrimEnd(Convert.ToChar(Me.TerminationCharacter))
    End Function

    ''' <summary> Queries the device and returns a string save the termination character.
    '''           Expects terminated query command. </summary>
    ''' <param name="format"> The format of the data to write. </param>
    ''' <param name="args">   The format arguments. </param>
    ''' <returns> The <see cref="LastMessageReceived">last received data</see> without the
    ''' <see cref="TerminationCharacter">termination character</see>. </returns>
    Public Overloads Function QueryTrimEnd(ByVal format As String, ByVal ParamArray args() As Object) As String
        Return Me.Query(format, args).TrimEnd(Convert.ToChar(Me.TerminationCharacter))
    End Function

#End Region

#Region " READ "

    ''' <summary> Synchronously reads ASCII-encoded string data. Reads up to
    ''' <see cref="DefaultBufferSize">characters</see> or up to the
    ''' <see cref="TerminationCharacter">termination character</see>. </summary>
    ''' <returns> The received message. </returns>
    Public Function ReadLine() As String
        Return Me.ReadString()
    End Function

    ''' <summary> Synchronously reads ASCII-encoded string data. Reads up to
    ''' <see cref="DefaultBufferSize">characters</see> or up to the
    ''' <see cref="TerminationCharacter">termination character</see>. </summary>
    ''' <returns> The received message without the <see cref="TerminationCharacter">termination
    ''' character</see>. </returns>
    Public Overloads Function ReadLineTrimEnd() As String
        Return Me.ReadLine().TrimEnd(Convert.ToChar(Me.TerminationCharacter))
    End Function

    ''' <summary> Synchronously reads ASCII-encoded string data. Reads up to
    ''' <see cref="DefaultBufferSize">characters</see> or up to the
    ''' <see cref="TerminationCharacter">termination character</see>. </summary>
    ''' <returns> The received message without the carriage return (13) and line feed (10) characters. </returns>
    Public Overloads Function ReadLineTrimNewLine() As String
        Return Me.ReadLine().TrimEnd(New Char() {Convert.ToChar(13), Convert.ToChar(10)})
    End Function

    ''' <summary> Synchronously reads ASCII-encoded string data. Reads up to
    ''' <see cref="DefaultBufferSize">characters</see> or up to the
    ''' <see cref="TerminationCharacter">termination character</see>. </summary>
    ''' <returns> The received message without the line feed (10) characters. </returns>
    Public Overloads Function ReadLineTrimLinefeed() As String
        Return Me.ReadLine().TrimEnd(Convert.ToChar(10))
    End Function


    ''' <summary> Synchronously reads ASCII-encoded string data. Reads up to
    ''' <see cref="DefaultBufferSize">characters</see> or up to the
    ''' <see cref="TerminationCharacter">termination character</see>. </summary>
    ''' <returns> The received message. </returns>
    Public Shadows Function ReadString() As String
        Me.LastMessageReceived = MyBase.ReadString()
        Return Me.LastMessageReceived
    End Function

    ''' <summary> Reads a potentially long string from the instrument until reading an end of line. </summary>
    ''' <param name="messageAvailableBits"> The message available bits. </param>
    ''' <param name="pollDelay">            Time to wait between service requests. </param>
    ''' <param name="timeout">              Specifies the time to wait for message available. </param>
    ''' <returns> The string. </returns>
    Public Shadows Function ReadString(ByVal messageAvailableBits As Integer, ByVal pollDelay As TimeSpan, ByVal timeout As TimeSpan) As String

        Dim listBuilder As New System.Text.StringBuilder
        Dim endDetected As Boolean = False
        Dim timedOut As Boolean = False
        Do Until endDetected OrElse timedOut OrElse Me.LastStatus < VisaNS.VisaStatusCode.Success

            timedOut = False
            Dim endTime As Date = DateTime.Now.Add(timeout)

            ' allow message available time to materialize
            Do Until timedOut OrElse Me.IsMessageAvailable(messageAvailableBits)
                timedOut = DateTime.Now > endTime
                Threading.Thread.Sleep(pollDelay)
            Loop

            If Me.IsMessageAvailable(messageAvailableBits) Then
                Dim value As String = MyBase.ReadString()
                listBuilder.Append(value)
                endDetected = value.EndsWith(Convert.ToChar(Me.TerminationCharacter), StringComparison.OrdinalIgnoreCase)
            End If

        Loop
        Me.LastMessageReceived = listBuilder.ToString
        Return Me.LastMessageReceived

    End Function

#End Region

#Region " READ LINES "

    ''' <summary> Reads multiple lines from the instrument until timeout. </summary>
    ''' <param name="pollDelay"> Time to wait between service requests. </param>
    ''' <param name="timeout">   Specifies the time to wait for message available. </param>
    ''' <param name="trimEnd">   Specifies a directive to trim the end character from each line. </param>
    ''' <returns> Data. </returns>
    Public Function ReadLines(ByVal pollDelay As TimeSpan, ByVal timeout As TimeSpan, ByVal trimEnd As Boolean) As String
        Return Me.ReadLines(Me.MessageAvailableBits, pollDelay, timeout, trimEnd)
    End Function

    ''' <summary> Reads multiple lines from the instrument until data is no longer available. </summary>
    ''' <param name="messageAvailableBits"> The message available bits. </param>
    ''' <param name="pollDelay">            Time to wait between service requests. </param>
    ''' <param name="timeout">              Specifies the time to wait for message available. </param>
    ''' <param name="trimEnd">              Specifies a directive to trim the end character from each
    ''' line. </param>
    ''' <returns> Data. </returns>
    Public Function ReadLines(ByVal messageAvailableBits As Integer, ByVal pollDelay As TimeSpan, ByVal timeout As TimeSpan, ByVal trimEnd As Boolean) As String

        Return ReadLines(messageAvailableBits, pollDelay, timeout, False, trimEnd)

    End Function

    ''' <summary> Reads multiple lines from the instrument until data is no longer available. </summary>
    ''' <param name="pollDelay">  Time to wait between service requests. </param>
    ''' <param name="timeout">    Specifies the time to wait for message available. </param>
    ''' <param name="trimSpaces"> Specifies a directive to trim leading and trailing spaces from each
    ''' line. This also trims the end character. </param>
    ''' <param name="trimEnd">    Specifies a directive to trim the end character from each line. </param>
    ''' <returns> Data. </returns>
    Public Function ReadLines(ByVal pollDelay As TimeSpan, ByVal timeout As TimeSpan,
                              ByVal trimSpaces As Boolean, ByVal trimEnd As Boolean) As String
        Return Me.ReadLines(Me.MessageAvailableBits, pollDelay, timeout, trimSpaces, trimEnd)
    End Function

    ''' <summary> Reads multiple lines from the instrument until data is no longer available. </summary>
    ''' <param name="messageAvailableBits"> The message available bits. </param>
    ''' <param name="pollDelay">            Time to wait between service requests. </param>
    ''' <param name="timeout">              Specifies the time to wait for message available. </param>
    ''' <param name="trimSpaces">           Specifies a directive to trim leading and trailing spaces
    ''' from each line. This also trims the end character. </param>
    ''' <param name="trimEnd">              Specifies a directive to trim the end character from each
    ''' line. </param>
    ''' <returns> Data. </returns>
    Public Function ReadLines(ByVal messageAvailableBits As Integer, ByVal pollDelay As TimeSpan, ByVal timeout As TimeSpan,
                              ByVal trimSpaces As Boolean, ByVal trimEnd As Boolean) As String

        Dim listBuilder As New System.Text.StringBuilder

        Dim endTime As Date = DateTime.Now.Add(timeout)
        Dim timedOut As Boolean = False
        Do While (Not timedOut) AndAlso Me.LastStatus >= VisaNS.VisaStatusCode.Success

            ' allow message available time to materialize
            Do Until Me.IsMessageAvailable(messageAvailableBits) OrElse timedOut
                timedOut = DateTime.Now > endTime
                Dim t1 As DateTime = DateTime.Now.Add(pollDelay)
                Do Until DateTime.Now > t1
                    Windows.Forms.Application.DoEvents()
                    Threading.Thread.Sleep(2)
                    Windows.Forms.Application.DoEvents()
                Loop
            Loop

            If Me.IsMessageAvailable(messageAvailableBits) Then
                timedOut = False
                endTime = DateTime.Now.Add(timeout)
                If trimSpaces Then
                    listBuilder.AppendLine(Me.ReadLine().Trim())
                ElseIf trimEnd Then
                    listBuilder.AppendLine(Me.ReadLineTrimEnd())
                Else
                    listBuilder.AppendLine(Me.ReadLine())
                End If
            End If

        Loop
        Return listBuilder.ToString

    End Function

    ''' <summary> Reads multiple lines from the instrument until timeout. </summary>
    ''' <param name="timeout">        Specifies the time in millisecond to wait for message available. </param>
    ''' <param name="trimSpaces">     Specifies a directive to trim leading and trailing spaces from
    ''' each line. </param>
    ''' <param name="expectedLength"> Specifies the amount of data expected without trimming. </param>
    ''' <returns> Data. </returns>
    Public Function ReadLines(ByVal timeout As TimeSpan, ByVal trimSpaces As Boolean, ByVal expectedLength As Integer) As String

        Try

            Dim listBuilder As New System.Text.StringBuilder
            Me.StoreTimeout(timeout)

            Dim timedOut As Boolean = False
            Dim currentLength As Integer = 0
            Do While currentLength < expectedLength AndAlso
                Me.LastStatus >= VisaNS.VisaStatusCode.Success AndAlso Not timedOut

                Try

                    Dim buffer As String = Me.ReadLine()
                    expectedLength += buffer.Length
                    If trimSpaces Then
                        listBuilder.AppendLine(buffer.Trim)
                    Else
                        listBuilder.AppendLine(buffer)
                    End If

                Catch ex As NationalInstruments.VisaNS.VisaException When ex.ErrorCode = VisaNS.VisaStatusCode.ErrorTimeout

                    timedOut = True

                End Try

            Loop

            Return listBuilder.ToString

        Catch

            Throw

        Finally

            Me.RestoreTimeout()

        End Try

    End Function

#End Region

#Region " WRITE "

    ''' <summary> Synchronously writes and ASCII-encoded string data. Terminates the data with the 
    '''           <see cref="TerminationCharacter">termination character</see>. </summary>
    ''' <param name="format"> The format of the data to write. </param>
    ''' <param name="args">   The format arguments. </param>
    Public Sub WriteLine(ByVal format As String, ByVal ParamArray args() As Object)
        Me.WriteLine(String.Format(Globalization.CultureInfo.InvariantCulture, format, args))
    End Sub

    ''' <summary> Synchronously writes and ASCII-encoded string data. Terminates the data with the 
    '''           <see cref="TerminationCharacter">termination character</see>. </summary>
    ''' <param name="dataToWrite"> The data to write. </param>
    Public Sub WriteLine(ByVal dataToWrite As String)
        If String.IsNullOrWhiteSpace(dataToWrite) Then
            Me.LastMessageSent = Nothing
        Else
            Me.Write(dataToWrite)
        End If
    End Sub

    ''' <summary> Synchronously writes ASCII-encoded string data to the device or interface. Terminates
    ''' the data with the <see cref="TerminationCharacter">termination character</see>. </summary>
    ''' <param name="dataToWrite"> The data to write. </param>
    Public Shadows Sub Write(ByVal dataToWrite As String)
        If String.IsNullOrWhiteSpace(dataToWrite) Then
            Me.LastMessageSent = Nothing
        Else
            Me.LastMessageSent = dataToWrite
            MyBase.Write(dataToWrite)
        End If
    End Sub

#End Region

#Region " DISCARD UNREAD DATA "

    ''' <summary> Reads and discards all data from the VISA session until the END indicator is read. </summary>
    ''' <remarks> Uses 10ms poll delay, 100ms timeout and default 
    '''           <see cref="MessageAvailableBits">message available bits</see>. </remarks>
    Public Sub DiscardUnreadData()
        Me.DiscardUnreadData(Me.MessageAvailableBits, TimeSpan.FromMilliseconds(10), TimeSpan.FromMilliseconds(100))
    End Sub

    ''' <summary> Reads and discards all data from the VISA session until the END indicator is read. </summary>
    ''' <remarks> This does not work on the 6221 if the only data is an End Line because this device
    ''' does not have the message available flag set.</remarks>
    ''' <param name="messageAvailableBits"> The message available bits. </param>
    Public Sub DiscardUnreadData(ByVal messageAvailableBits As Integer)
        Me._DiscardedData = New System.Text.StringBuilder
        Do While (Me.ReadStatusByte() And messageAvailableBits) <> 0
            Me._DiscardedData.AppendLine(Me.ReadLine())
        Loop
        If Not String.IsNullOrWhiteSpace(Me.DiscardedData) Then
            Me.AsyncNotifyPropertyChanged("DiscardedData")
        End If
    End Sub

    Private _DiscardedData As System.Text.StringBuilder

    ''' <summary> Gets the information describing the discarded. </summary>
    ''' <value> Information describing the discarded. </value>
    Public ReadOnly Property DiscardedData As String
        Get
            If Me._DiscardedData Is Nothing Then
                Return ""
            Else
                Return Me._DiscardedData.ToString
            End If
        End Get
    End Property

    ''' <summary> Reads and discards all data from the VISA session until the END indicator is read and no data are 
    '''           added to the output buffer. </summary>
    ''' <param name="messageAvailableBits"> The message available bits. </param>
    ''' <param name="pollDelay">            Time to wait between service requests. </param>
    ''' <param name="timeout">              Specifies the time to wait for message available. </param>
    Public Sub DiscardUnreadData(ByVal messageAvailableBits As Integer, ByVal pollDelay As TimeSpan, ByVal timeout As TimeSpan)

        Me._DiscardedData = New System.Text.StringBuilder
        Do

            Dim endTime As Date = DateTime.Now.Add(timeout)

            ' allow message available time to materialize
            Do Until Me.IsMessageAvailable(messageAvailableBits) OrElse DateTime.Now > endTime
                Threading.Thread.Sleep(pollDelay)
            Loop

            If Me.MessageAvailable Then
                Me._DiscardedData.AppendLine(Me.ReadLine())
            End If

        Loop While Me.MessageAvailable AndAlso Me.LastStatus >= VisaNS.VisaStatusCode.Success

    End Sub

#End Region

#Region " TIMEOUT MANAGEMENT "

    ''' <summary> The timeouts. </summary>
    Private _Timeouts As System.Collections.Generic.Stack(Of Integer)

    ''' <summary> Gets the reference to the stack of timeouts. </summary>
    ''' <value> The timeouts. </value>
    Protected ReadOnly Property Timeouts As System.Collections.Generic.Stack(Of Integer)
        Get
            Return Me._Timeouts
        End Get
    End Property

    ''' <summary> Restores the last timeout from the stack. </summary>
    Public Sub RestoreTimeout()
        MyBase.Timeout = Me.Timeouts.Pop
    End Sub

    ''' <summary> Saves the current timeout and sets a new setting timeout. </summary>
    ''' <param name="timeout"> Specifies the new timeout. </param>
    Public Sub StoreTimeout(ByVal timeout As TimeSpan)
        Me.Timeouts.Push(MyBase.Timeout)
        MyBase.Timeout = CType(Math.Max(timeout.TotalMilliseconds, Integer.MaxValue), Integer)
    End Sub

#End Region

#Region " MESSAGE EVENTS "

    Private _SynchronousLastMessageReceivedEnabled As Boolean

    ''' <summary> Gets or sets the synchronous last message received enabled sentinel. </summary>
    ''' <remarks> Enable synchronous reporting of last received when messages are needed to be parsed
    ''' to determine the instrument state such as with TSP. This ensures that the parser works on the
    ''' relevant message that was received from the instrument. </remarks>
    ''' <value> The synchronous last message received enabled. </value>
    Public Property SynchronousLastMessageReceivedEnabled As Boolean
        Get
            Return Me._SynchronousLastMessageReceivedEnabled
        End Get
        Set(value As Boolean)
            If value <> Me.SynchronousLastMessageReceivedEnabled Then
                Me._SynchronousLastMessageReceivedEnabled = value
                Me.AsyncNotifyPropertyChanged("SynchronousLastMessageReceivedEnabled")
            End If
        End Set
    End Property

    Private _LastMessageReceived As String
    ''' <summary> Gets or sets the last message Received. </summary>
    ''' <remarks> The last message sent is posted asynchronously. This may not be processed fast enough
    ''' with TSP devices to determine the state of the instrument. </remarks>
    ''' <value> The last message Received. </value>
    Public Property LastMessageReceived As String
        Get
            Return Me._LastMessageReceived
        End Get
        Set(ByVal value As String)
            Me._LastMessageReceived = value
            If Me.SynchronousLastMessageReceivedEnabled Then
                Me.SyncNotifyPropertyChanged("LastMessageReceived")
            Else
                Me.AsyncNotifyPropertyChanged("LastMessageReceived")
            End If
        End Set
    End Property

    Private _LastMessageSent As String
    ''' <summary> Gets or sets the last message Sent. </summary>
    ''' <remarks> The last message sent is posted asynchronously. </remarks>
    ''' <value> The last message Sent. </value>
    Public Property LastMessageSent As String
        Get
            Return Me._LastMessageSent
        End Get
        Set(ByVal value As String)
            Me._LastMessageSent = value
            Me.AsyncNotifyPropertyChanged("LastMessageSent")
        End Set
    End Property

#End Region

#Region " PROPERTY CHANGED EVENT IMPLEMENTATION "

    ''' <summary> Propagates a sync context. </summary>
    ''' <value> The synchronization context. </value>
    ''' <remarks> The current thread may not have a synchronization context object or
    ''' a synchronization context has not been set for this thread. Creating an instance of a
    ''' synchronization context can yield unexpected results if the Current property of the
    ''' synchronization context is checked from a thread other than the thread on which the UI
    ''' is running, in which case the context will be null. Rather, the current
    ''' context is checked whenever the sync context is called. </remarks>
    Protected Shared ReadOnly Property CurrentSyncContext As SynchronizationContext
        Get
            Return SynchronizationContext.Current
        End Get
    End Property

    ''' <summary> Event that is raised when a property value changes. </summary>
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

#Region " INVOKE "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub InvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        If evt IsNot Nothing Then evt.Invoke(Me, e)
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub InvokePropertyChanged(ByVal obj As Object)
        Me.InvokePropertyChanged(CType(obj, System.ComponentModel.PropertyChangedEventArgs))
    End Sub

#End Region

#Region " NOTIFICATIONS "

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property value. </summary>
    ''' <param name="name"> The property name. </param>
    Protected Sub SyncNotifyPropertyChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then
            If Session.CurrentSyncContext Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
                ' posting is handled inside the method to gain the time 
                ' (~.2ms on the super fast development machine) it takes to call 
                ' the method.
                If evt IsNot Nothing Then
                    Dim e As New System.ComponentModel.PropertyChangedEventArgs(name)
                    For Each d As [Delegate] In evt.GetInvocationList
                        If d.Target Is Nothing Then
                            d.DynamicInvoke(New Object() {Me, e})
                        Else
                            Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                            If target Is Nothing Then
                                d.DynamicInvoke(New Object() {Me, e})
                            Else
                                target.Invoke(d, New Object() {Me, e})
                            End If
                        End If
                    Next
                End If
            Else
                Session.CurrentSyncContext.Send(New SendOrPostCallback(AddressOf InvokePropertyChanged),
                                                New System.ComponentModel.PropertyChangedEventArgs(name))
            End If
        End If
    End Sub

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
    ''' <param name="name"> The property name. </param>
    Protected Sub AsyncNotifyPropertyChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then
            If Session.CurrentSyncContext Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
                ' posting is handled inside the method to gain the time 
                ' (~.2ms on the super fast development machine) it takes to call 
                ' the method.
                If evt IsNot Nothing Then
                    Dim e As New System.ComponentModel.PropertyChangedEventArgs(name)
                    For Each d As [Delegate] In evt.GetInvocationList
                        If d.Target Is Nothing Then
                            d.DynamicInvoke(New Object() {Me, e})
                        Else
                            Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                            If target Is Nothing Then
                                d.DynamicInvoke(New Object() {Me, e})
                            Else
                                target.BeginInvoke(d, New Object() {Me, e})
                            End If
                        End If
                    Next
                End If
            Else
                Session.CurrentSyncContext.Post(New SendOrPostCallback(AddressOf InvokePropertyChanged),
                                                New System.ComponentModel.PropertyChangedEventArgs(name))
            End If
        End If
    End Sub

#End Region

#End Region

End Class
