﻿Imports NationalInstruments
Namespace Scpi

    ''' <summary> Defines a SCPI Trigger Subsystem for a generic Source Measure instrument. </summary>
    ''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/26/2012" by="David" revision="1.0.4652"> Created. </history>
    Public Class TriggerSubsystem
        Inherits Visa.SCPI.TriggerSubsystemBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary> Initializes a new instance of the <see cref="TriggerSubsystem" /> class. </summary>
        ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">message based
        ''' session</see>. </param>
        Public Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
            MyBase.New(statusSubsystem)
        End Sub

#End Region

#Region " I PRESETTABLE "

        ''' <summary> Sets subsystem values to their known execution clear state. </summary>
        Public Overrides Sub ClearExecutionState()
        End Sub

#End Region

#Region " PUBLISHER "

        ''' <summary> Publishes all values by raising the property changed events. </summary>
        Public Overrides Sub Publish()
            If Me.Publishable Then
                For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                    Me.AsyncNotifyPropertyChanged(p.Name)
                Next
            End If
        End Sub

#End Region

#Region " COMMAND SYNTAX "

        ''' <summary> Gets the preset command. </summary>
        ''' <value> The preset command. </value>
        Protected Overrides ReadOnly Property PresetCommand As String
            Get
                Return ""
            End Get
        End Property

#Region " ABORT / INIT COMMANDS "

        ''' <summary> Gets the Abort command. </summary>
        ''' <value> The Abort command. </value>
        Protected Overrides ReadOnly Property AbortCommand As String
            Get
                Return ":ABOR"
            End Get
        End Property

        ''' <summary> Gets the initiate command. </summary>
        ''' <value> The initiate command. </value>
        Protected Overrides ReadOnly Property InitiateCommand As String
            Get
                Return ":INIT"
            End Get
        End Property

#End Region

#Region " ARM "

        ''' <summary> Gets the Arm source command format. </summary>
        ''' <value> The write Arm source command format. </value>
        Protected Overrides ReadOnly Property ArmSourceCommandFormat As String
            Get
                Return ":TRAC:ARM {0}"
            End Get
        End Property

        ''' <summary> Gets the Arm source query command. </summary>
        ''' <value> The Arm source query command. </value>
        Protected Overrides ReadOnly Property ArmSourceQueryCommand As String
            Get
                Return ":TRAC:ARM?"
            End Get
        End Property

#End Region

#Region " AUTO DELAY "

        ''' <summary> Gets the automatic delay enabled command Format. </summary>
        ''' <value> The automatic delay enabled query command. </value>
        Protected Overrides ReadOnly Property AutoDelayEnabledCommandFormat As String
            Get
                Return ":TRIG:DEL:AUTO {0:'ON';'ON';'OFF'}"
            End Get
        End Property

        ''' <summary> Gets the automatic delay enabled query command. </summary>
        ''' <value> The automatic delay enabled query command. </value>
        Protected Overrides ReadOnly Property AutoDelayEnabledQueryCommand As String
            Get
                Return ":TRIG:DEL:AUTO?"
            End Get
        End Property

#End Region

#Region " COUNT "

        ''' <summary> Gets trigger count query command. </summary>
        ''' <value> The trigger count query command. </value>
        Protected Overrides ReadOnly Property CountQueryCommand As String
            Get
                Return ":TRIG:COUN?"
            End Get
        End Property

        ''' <summary> Gets trigger count command format. </summary>
        ''' <value> The trigger count command format. </value>
        Protected Overrides ReadOnly Property CountCommandFormat As String
            Get
                Return ":TRIG:COUN {0}"
            End Get
        End Property

#End Region

#Region " DELAY "

        ''' <summary> Gets the delay command format. </summary>
        ''' <value> The delay command format. </value>
        Protected Overrides ReadOnly Property DelayCommandFormat As String
            Get
                Return ":TRIG:DEL {0:s\.fff}"
            End Get
        End Property

        ''' <summary> Gets the Delay format for converting the query to time span. </summary>
        ''' <value> The Delay query command. </value>
        Protected Overrides ReadOnly Property DelayFormat As String
            Get
                Return "s\.fff"
            End Get
        End Property

        ''' <summary> Gets the delay query command. </summary>
        ''' <value> The delay query command. </value>
        Protected Overrides ReadOnly Property DelayQueryCommand As String
            Get
                Return ":TRIG:DEL?"
            End Get
        End Property

#End Region

#Region " DIRECTION (BYPASS) "

        ''' <summary> Gets the Direction command. </summary>
        ''' <value> The Direction command. </value>
        Protected Overrides ReadOnly Property DirectionCommand As Command
            Get
                Static cmd As Command
                If cmd Is Nothing Then
                    cmd = New Command(":TRIG:DIR", CommandFormats.Standard And CommandFormats.QuerySupported And CommandFormats.WriteSupported)
                End If
                Return cmd
            End Get
        End Property

#End Region

#Region " INPUT LINE NUMBER "

        ''' <summary> Gets the Input Line Number command format. </summary>
        ''' <value> The Input Line Number command format. </value>
        Protected Overrides ReadOnly Property InputLineNumberCommandFormat As String
            Get
                Return ":TRIG:ILIN {0}"
            End Get
        End Property

        ''' <summary> Gets the Input Line Number query command. </summary>
        ''' <value> The Input Line Number query command. </value>
        Protected Overrides ReadOnly Property InputLineNumberQueryCommand As String
            Get
                Return ":TRIG:ILIN?"
            End Get
        End Property

#End Region

#Region " OUTPUT LINE NUMBER "

        ''' <summary> Gets the Output Line Number command format. </summary>
        ''' <value> The Output Line Number command format. </value>
        Protected Overrides ReadOnly Property OutputLineNumberCommandFormat As String
            Get
                Return ":TRIG:OLIN {0}"
            End Get
        End Property

        ''' <summary> Gets the Output Line Number query command. </summary>
        ''' <value> The Output Line Number query command. </value>
        Protected Overrides ReadOnly Property OutputLineNumberQueryCommand As String
            Get
                Return ":TRIG:OLIN?"
            End Get
        End Property

#End Region

#Region " TIMER TIME SPAN "

        ''' <summary> Gets the Timer Interval command format. </summary>
        ''' <value> The query command format. </value>
        Protected Overrides ReadOnly Property TimerIntervalCommandFormat As String
            Get
                Return ":TRIG:TIM {0:s\.fff}"
            End Get
        End Property

        ''' <summary> Gets the Timer Interval format for converting the query to time span. </summary>
        ''' <value> The Timer Interval query command. </value>
        Protected Overrides ReadOnly Property TimerIntervalFormat As String
            Get
                Return "s\.fff"
            End Get
        End Property

        ''' <summary> Gets the Timer Interval query command. </summary>
        ''' <value> The Timer Interval query command. </value>
        Protected Overrides ReadOnly Property TimerIntervalQueryCommand As String
            Get
                Return ":TRIG:TIM?"
            End Get
        End Property
#End Region

#End Region

    End Class

End Namespace
