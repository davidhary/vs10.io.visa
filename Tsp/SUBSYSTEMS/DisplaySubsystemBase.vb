Imports NationalInstruments
Imports isr.Core.Primitives
''' <summary> Defines a System Subsystem for a TSP System. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="10/7/2013" by="David" revision=""> Created. </history>
Public MustInherit Class DisplaySubsystemBase
    Inherits Visa.SCPI.DisplaySubsystemBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Initializes a new instance of the <see cref="DisplaySubsystemBase" /> class. </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="statusSubsystemBase">status
    ''' Subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        If statusSubsystem IsNot Nothing Then
            Me.TspSession = statusSubsystem.TspSession
        End If
        Me.RestoreMainScreenWaitCompleteCommand = TspSyntax.Display.RestoreMainWaitCompleteCommand
    End Sub

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    ''' <c>False</c> if this method releases only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me.TspSession = Nothing

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' dispose the base class.
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets subsystem values to their known execution clear state. </summary>
    Public Overrides Sub ClearExecutionState()
    End Sub

    ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
    Public Overrides Sub ResetKnownState()
    End Sub

#End Region

#Region " SESSION / STATUS SUBSYSTEM"

    ''' <summary> Gets or sets the session. </summary>
    Private TspSession As TspSession

#End Region

#Region " EXISTS "

    Private _isDisplayExists As Boolean?

    ''' <summary> Gets or sets (Protected) the display existence indicator.
    '''           Some TSP instruments (e.g., 3706) may have no display. </summary>
    ''' <value> The is display exists. </value>
    Public Property IsDisplayExists() As Boolean?
        Get
            Return Me._isDisplayExists
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(value, Me.IsDisplayExists) Then
                Me._isDisplayExists = value
                MyBase.AsyncNotifyPropertyChanged("IsDisplayExists")
            End If
        End Set
    End Property

    ''' <summary> Reads  the display existence indicator.
    '''           Some TSP instruments (e.g., 3706) may have no display. </summary>
    ''' <returns> <c>True</c> if the display exists; otherwise, <c>False</c>. </returns>
    Public Function ReadDisplayExists() As Boolean?
        If Me.IsSessionOpen AndAlso Not Me.IsDisplayExists.HasValue Then
            ' detect the display
            Me.IsDisplayExists = Not Me.TspSession.IsNil(TspSyntax.Display.SubsystemName)
        End If
        Return Me.IsDisplayExists
    End Function

#End Region

#Region " CLEAR "

    ''' <summary> Clears the display. </summary>
    ''' <remarks> Sets the display to the user mode. </remarks>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function ClearDisplay() As Boolean

        Me.DisplayScreen = DisplayScreens.User
        If Me.ReadDisplayExists.GetValueOrDefault(False) Then
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(TspSyntax.Display.ClearCommand)
                Return Me.ReportVisaDeviceOperationOkay(False, "clearing display;. ")
            End If
        Else
            Return True
        End If

    End Function

    ''' <summary> Clears the display if not in measurement mode and set measurement mode. </summary>
    ''' <remarks> Sets the display to the measurement. </remarks>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function ClearDisplayMeasurement() As Boolean

        If Me.IsDisplayExists Then
            If (Me.DisplayScreen And DisplayScreens.Measurement) = 0 Then
                Me.ClearDisplay()
            End If
        End If
        Me.DisplayScreen = DisplayScreens.Measurement
        Return Not Me.IsSessionOpen OrElse Me.LastStatus >= VisaNS.VisaStatusCode.Success

    End Function

#End Region

#Region " DISPLAY COMMANDS "

    ''' <summary> Displays the character. </summary>
    ''' <param name="lineNumber">      The line number. </param>
    ''' <param name="position">        The position. </param>
    ''' <param name="characterNumber"> The character number. </param>
    Public Sub DisplayCharacter(ByVal lineNumber As Integer, ByVal position As Integer,
                                ByVal characterNumber As Integer)
        Me.DisplayScreen = DisplayScreens.User Or DisplayScreens.Custom
        If Not Me.ReadDisplayExists.GetValueOrDefault(False) Then
            Return
        End If
        ' ignore empty character.
        If characterNumber <= 0 OrElse characterNumber > TspSyntax.Display.MaximumCharacterNumber Then
            Return
        End If
        Me.Session.WriteLine(TspSyntax.Display.SetCursorCommandFormat, lineNumber, position)
        If Me.LastStatus >= VisaNS.VisaStatusCode.Success Then
            Me.Session.WriteLine(TspSyntax.Display.SetCharacterCommandFormat, characterNumber)
        End If
    End Sub

    ''' <summary> Displays message on line one of the display. </summary>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <param name="format">     Describes the format to use. </param>
    ''' <param name="args">       A variable-length parameters list containing arguments. </param>
    Public Sub DisplayLine(ByVal lineNumber As Integer, ByVal format As String, ByVal ParamArray args() As Object)
        Me.DisplayLine(lineNumber, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

    ''' <summary> Displays a message on the display. </summary>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <param name="value">      The value. </param>
    Public Sub DisplayLine(ByVal lineNumber As Integer, ByVal value As String)

        Me.DisplayScreen = DisplayScreens.User Or DisplayScreens.Custom
        If Not Me.ReadDisplayExists.GetValueOrDefault(False) Then
            Return
        End If

        ' ignore empty strings.
        If String.IsNullOrWhiteSpace(value) Then
            Return
        End If

        Dim length As Integer = TspSyntax.Display.FirstLineLength

        If lineNumber < 1 Then
            lineNumber = 1
        ElseIf lineNumber > 2 Then
            lineNumber = 2
        End If
        If lineNumber = 2 Then
            length = TspSyntax.Display.SecondLineLength
        End If

        If Me.IsSessionOpen Then
            Me.Session.WriteLine(TspSyntax.Display.SetCursorLineCommandFormat, lineNumber)
            If Me.LastStatus >= VisaNS.VisaStatusCode.Success Then
                If value.Length < length Then
                    value = value.PadRight(length)
                End If
                Me.Session.WriteLine(TspSyntax.Display.SetTextCommandFormat, value)
            End If
        End If

    End Sub

    ''' <summary> Displays the program title. </summary>
    ''' <param name="title">    Top row data. </param>
    ''' <param name="subtitle"> Bottom row data. </param>
    Public Sub DisplayTitle(ByVal title As String, ByVal subtitle As String)
        Me.DisplayLine(0, title)
        If Me.LastStatus >= VisaNS.VisaStatusCode.Success Then
            Me.DisplayLine(2, subtitle)
        End If
    End Sub

    ''' <summary> Displays the program title. </summary>
    Public Overridable Sub DisplayTitle()
        Me.DisplayTitle("title", "sub title")
    End Sub

#End Region

#Region " SCREEN "

    Private _DisplayScreen As DisplayScreens

    ''' <summary> Gets or sets (Protected) the <see cref="DisplayScreens">display screen and
    ''' status</see>. </summary>
    ''' <value> The display screen. </value>
    Public Property DisplayScreen() As DisplayScreens
        Get
            Return Me._DisplayScreen
        End Get
        Protected Set(ByVal value As DisplayScreens)
            If Not value.Equals(Me.DisplayScreen) Then
                Me._DisplayScreen = value
                MyBase.AsyncNotifyPropertyChanged("DisplayScreen")
            End If
        End Set
    End Property

#End Region

#Region " RESTORE "

    ''' <summary> Gets or sets the restore display command. </summary>
    ''' <value> The restore display command. </value>
    Public Property RestoreMainScreenWaitCompleteCommand As String

    ''' <summary> Restores the instrument display. </summary>
    ''' <param name="timeout"> The timeout. </param>
    Public Sub RestoreDisplay(ByVal timeout As TimeSpan)
        Me.DisplayScreen = DisplayScreens.Default
        If Me.ReadDisplayExists.GetValueOrDefault(False) AndAlso Me.IsSessionOpen AndAlso
            Not String.IsNullOrWhiteSpace(Me.RestoreMainScreenWaitCompleteCommand) Then
            Me.StatusSubsystem.EnableWaitComplete()
            ' Documentation error: Display Main equals 1, not 0. This code should work on other instruments.
            Me.Session.WriteLine(Me.RestoreMainScreenWaitCompleteCommand)
            Me.StatusSubsystem.AwaitOperationCompleted(timeout)
        End If
    End Sub

    ''' <summary> Restores the instrument display. </summary>
    Public Sub RestoreDisplay()
        Me.DisplayScreen = DisplayScreens.Default
        If Me.ReadDisplayExists.GetValueOrDefault(False) AndAlso Me.IsSessionOpen AndAlso
            Not String.IsNullOrWhiteSpace(Me.RestoreMainScreenWaitCompleteCommand) Then
            ' Documentation error: Display Main equals 1, not 0. This code should work on other instruments.
            Me.Session.WriteLine(Me.RestoreMainScreenWaitCompleteCommand)
            Me.StatusSubsystem.QueryOperationCompleted()
        End If
    End Sub

#End Region

End Class
