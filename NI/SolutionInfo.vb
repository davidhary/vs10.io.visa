﻿Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Security.Permissions

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

<Assembly: AssemblyCompany("National Instruments")> 
<Assembly: AssemblyCopyright("(c) 2010 National Instruments. All rights reserved.")>  
<Assembly: AssemblyTrademark("Licensed under The MIT License.")> 
<Assembly: Resources.NeutralResourcesLanguage("en-US", Resources.UltimateResourceFallbackLocation.MainAssembly)> 

' Version information for an assembly consists of the following four values:

'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
<Assembly: AssemblyVersion("3.0.*")> 

' The file version specifies the version that is deployed.
' Commenting this out makes these versions the same as the Assembly Version.
' <Assembly: AssemblyFileVersion("3.0.0.0")> 
' <assembly: AssemblyInformationalVersion("3.0.0.0")> 
