'================================================================================================== 
' 
' Title      : ResourceList.cs 
' Purpose    : This application shows the user how to use ResourceManager to 
'				find all of the available resources on their system. In the example, 
'				they can select between several filters to narrow the list. Public
'				property ResourceName contains the resource name selected in resourceTreeView
' 
'================================================================================================== 

Imports NationalInstruments.VisaNS

Public Class ResourceListForm
    Inherits TopDialogBase

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        ndGpib = New TreeNode("GPIB")
        ndVxi = New TreeNode("VXI")
        ndGpibVxi = New TreeNode("GPIB VXI")
        ndSerial = New TreeNode("Serial")
        ndPxi = New TreeNode("PXI")
        ndTcpip = New TreeNode("TCP/IP")
        ndUSB = New TreeNode("USB")
        ndFireWire = New TreeNode("FireWire")
        CleanResourceNodes()

        PopulateFilterList()
        findResourcesButton_Click(Nothing, Nothing) ' Automatically call FindResources on "?*" when the program starts

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Private WithEvents FilterStringLabel As System.Windows.Forms.Label
    Private WithEvents doubleClickLabel As System.Windows.Forms.Label
    Private WithEvents filterStringsListBox As System.Windows.Forms.ListBox
    Private WithEvents resourceTreeView As System.Windows.Forms.TreeView
    Private WithEvents findResourcesButton As System.Windows.Forms.Button
    Private WithEvents findTcpipResourcesButton As System.Windows.Forms.Button
    Private WithEvents clearButton As System.Windows.Forms.Button
    Private WithEvents useCustomStringButton As System.Windows.Forms.Button
    Private WithEvents alternativeLabel As System.Windows.Forms.Label
    Private WithEvents resourceNameTextBox As System.Windows.Forms.TextBox
    Private WithEvents openButton As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(ResourceListForm))
        Me.FilterStringLabel = New System.Windows.Forms.Label
        Me.doubleClickLabel = New System.Windows.Forms.Label
        Me.filterStringsListBox = New System.Windows.Forms.ListBox
        Me.resourceTreeView = New System.Windows.Forms.TreeView
        Me.findResourcesButton = New System.Windows.Forms.Button
        Me.findTcpipResourcesButton = New System.Windows.Forms.Button
        Me.clearButton = New System.Windows.Forms.Button
        Me.useCustomStringButton = New System.Windows.Forms.Button
        Me.alternativeLabel = New System.Windows.Forms.Label
        Me.resourceNameTextBox = New System.Windows.Forms.TextBox
        Me.openButton = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'FilterStringLabel
        '
        Me.FilterStringLabel.Location = New System.Drawing.Point(16, 24)
        Me.FilterStringLabel.Name = "FilterStringLabel"
        Me.FilterStringLabel.Size = New System.Drawing.Size(64, 16)
        Me.FilterStringLabel.TabIndex = 0
        Me.FilterStringLabel.Text = "Filter String "
        '
        'doubleClickLabel
        '
        Me.doubleClickLabel.Location = New System.Drawing.Point(16, 216)
        Me.doubleClickLabel.Name = "doubleClickLabel"
        Me.doubleClickLabel.Size = New System.Drawing.Size(160, 16)
        Me.doubleClickLabel.TabIndex = 1
        Me.doubleClickLabel.Text = "Double-Click on the Resource:"
        '
        'filterStringsListBox
        '
        Me.filterStringsListBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                                System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.filterStringsListBox.Location = New System.Drawing.Point(16, 40)
        Me.filterStringsListBox.Name = "filterStringsListBox"
        Me.filterStringsListBox.Size = New System.Drawing.Size(248, 121)
        Me.filterStringsListBox.TabIndex = 2
        '
        'resourceTreeView
        '
        Me.resourceTreeView.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or
                                             System.Windows.Forms.AnchorStyles.Left) Or
                                         System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.resourceTreeView.ImageIndex = -1
        Me.resourceTreeView.Location = New System.Drawing.Point(16, 232)
        Me.resourceTreeView.Name = "resourceTreeView"
        Me.resourceTreeView.SelectedImageIndex = -1
        Me.resourceTreeView.Size = New System.Drawing.Size(248, 136)
        Me.resourceTreeView.TabIndex = 3
        '
        'findResourcesButton
        '
        Me.findResourcesButton.Location = New System.Drawing.Point(16, 168)
        Me.findResourcesButton.Name = "findResourcesButton"
        Me.findResourcesButton.Size = New System.Drawing.Size(104, 24)
        Me.findResourcesButton.TabIndex = 4
        Me.findResourcesButton.Text = "Find Resources"
        '
        'findTcpipResourcesButton
        '
        Me.findTcpipResourcesButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.findTcpipResourcesButton.Location = New System.Drawing.Point(128, 168)
        Me.findTcpipResourcesButton.Name = "findTcpipResourcesButton"
        Me.findTcpipResourcesButton.Size = New System.Drawing.Size(136, 23)
        Me.findTcpipResourcesButton.TabIndex = 5
        Me.findTcpipResourcesButton.Text = "Find TCP/IP Resources"
        '
        'clearButton
        '
        Me.clearButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.clearButton.Location = New System.Drawing.Point(192, 200)
        Me.clearButton.Name = "clearButton"
        Me.clearButton.Size = New System.Drawing.Size(72, 23)
        Me.clearButton.TabIndex = 6
        Me.clearButton.Text = "Clear"
        '
        'useCustomStringButton
        '
        Me.useCustomStringButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.useCustomStringButton.Location = New System.Drawing.Point(152, 8)
        Me.useCustomStringButton.Name = "useCustomStringButton"
        Me.useCustomStringButton.Size = New System.Drawing.Size(112, 23)
        Me.useCustomStringButton.TabIndex = 7
        Me.useCustomStringButton.Text = "Use Custom String"
        '
        'alternativeLabel
        '
        Me.alternativeLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.alternativeLabel.Location = New System.Drawing.Point(16, 387)
        Me.alternativeLabel.Name = "alternativeLabel"
        Me.alternativeLabel.Size = New System.Drawing.Size(256, 32)
        Me.alternativeLabel.TabIndex = 8
        Me.alternativeLabel.Text = "Or Type in the Resource or Its Alias and Click on Open Button:"
        '
        'resourceNameTextBox
        '
        Me.resourceNameTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) Or
                                               System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.resourceNameTextBox.Location = New System.Drawing.Point(16, 416)
        Me.resourceNameTextBox.Name = "resourceNameTextBox"
        Me.resourceNameTextBox.Size = New System.Drawing.Size(152, 20)
        Me.resourceNameTextBox.TabIndex = 9
        Me.resourceNameTextBox.Text = ""
        '
        'openButton
        '
        Me.openButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.openButton.Location = New System.Drawing.Point(192, 416)
        Me.openButton.Name = "openButton"
        Me.openButton.TabIndex = 10
        Me.openButton.Text = "Open"
        '
        'ResourceListForm
        '
        Me.ClientSize = New System.Drawing.Size(280, 446)
        Me.Controls.Add(Me.openButton)
        Me.Controls.Add(Me.resourceNameTextBox)
        Me.Controls.Add(Me.alternativeLabel)
        Me.Controls.Add(Me.useCustomStringButton)
        Me.Controls.Add(Me.clearButton)
        Me.Controls.Add(Me.findTcpipResourcesButton)
        Me.Controls.Add(Me.findResourcesButton)
        Me.Controls.Add(Me.resourceTreeView)
        Me.Controls.Add(Me.filterStringsListBox)
        Me.Controls.Add(Me.doubleClickLabel)
        Me.Controls.Add(Me.FilterStringLabel)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimumSize = New System.Drawing.Size(288, 384)
        Me.Name = "ResourceListForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Available Resources List"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private visaSession As session
    Private ndGpib As TreeNode
    Private ndVxi As TreeNode
    Private ndGpibVxi As TreeNode
    Private ndSerial As TreeNode
    Private ndPxi As TreeNode
    Private ndTcpip As TreeNode
    Private ndUSB As TreeNode
    Private ndFireWire As TreeNode

    Private filter As String
    Private ndTcpipAdded As Boolean = False

    <STAThread()> 
    Public Shared Sub Main()
        Application.Run(New ResourceListForm())
    End Sub

    Private Sub PopulateFilterList()
        filterStringsListBox.Items.Clear()
        filterStringsListBox.Items.Add("?*")
        filterStringsListBox.Items.Add("GPIB?*")
        filterStringsListBox.Items.Add("GPIB?*INSTR")
        filterStringsListBox.Items.Add("GPIB?*INTFC")
        filterStringsListBox.Items.Add("GPIB?*SERVANT")
        filterStringsListBox.Items.Add("GPIB-VXI?*")
        filterStringsListBox.Items.Add("GPIB-VXI?*INSTR")
        filterStringsListBox.Items.Add("GPIB-VXI?*MEMACC")
        filterStringsListBox.Items.Add("GPIB-VXI?*BACKPLANE")
        filterStringsListBox.Items.Add("PXI?*INSTR")
        filterStringsListBox.Items.Add("ASRL?*INSTR")
        filterStringsListBox.Items.Add("VXI?*")
        filterStringsListBox.Items.Add("VXI?*INSTR")
        filterStringsListBox.Items.Add("VXI?*MEMACC")
        filterStringsListBox.Items.Add("VXI?*BACKPLANE")
        filterStringsListBox.Items.Add("VXI?*SERVANT")
        filterStringsListBox.Items.Add("USB?*")
        filterStringsListBox.Items.Add("FIREWIRE?*")
        filterStringsListBox.SelectedIndex = 0
    End Sub

    Private Sub AddToResourceTree()
        If ndGpib.Nodes.Count <> 0 Then
            resourceTreeView.Nodes.Add(ndGpib)
        End If
        If ndVxi.Nodes.Count <> 0 Then
            resourceTreeView.Nodes.Add(ndVxi)
        End If
        If ndGpibVxi.Nodes.Count <> 0 Then
            resourceTreeView.Nodes.Add(ndGpibVxi)
        End If
        If ndSerial.Nodes.Count <> 0 Then
            resourceTreeView.Nodes.Add(ndSerial)
        End If
        If ndPxi.Nodes.Count <> 0 Then
            resourceTreeView.Nodes.Add(ndPxi)
        End If
        If ndUSB.Nodes.Count <> 0 Then
            resourceTreeView.Nodes.Add(ndUSB)
        End If
        If ndFireWire.Nodes.Count <> 0 Then
            resourceTreeView.Nodes.Add(ndFireWire)
        End If
    End Sub

    Private Sub AddToResourceNode(ByVal resourceName As String, ByVal intType As HardwareInterfaceType)
        Select Case intType
            Case HardwareInterfaceType.Gpib
                ndGpib.Nodes.Add(New TreeNode(resourceName))
            Case HardwareInterfaceType.Vxi
                ndVxi.Nodes.Add(New TreeNode(resourceName))
            Case HardwareInterfaceType.GpibVxi
                ndGpibVxi.Nodes.Add(New TreeNode(resourceName))
            Case HardwareInterfaceType.Serial
                ndSerial.Nodes.Add(New TreeNode(resourceName))
            Case HardwareInterfaceType.Pxi
                ndPxi.Nodes.Add(New TreeNode(resourceName))
            Case HardwareInterfaceType.Tcpip
                ndTcpip.Nodes.Add(New TreeNode(resourceName))
            Case HardwareInterfaceType.Usb
                ndUSB.Nodes.Add(New TreeNode(resourceName))
            Case HardwareInterfaceType.Firewire
                ndFireWire.Nodes.Add(New TreeNode(resourceName))
        End Select
    End Sub

    Private Sub findTcpipResourcesButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles findTcpipResourcesButton.Click
        Dim trf As TcpipResourceVerifiicationUtilityForm = New TcpipResourceVerifiicationUtilityForm
        trf.ShowDialog()

        If trf.TcpipResourceNames.Length <> 0 And (Not ndTcpipAdded Or resourceTreeView.Nodes.Count = 0) Then
            resourceTreeView.Nodes.Add(ndTcpip)
            ndTcpipAdded = True
        End If

        Dim s As String
        For Each s In trf.TcpipResourceNames
            If (Not InResourceTree(s)) Then
                AddToResourceNode(s, HardwareInterfaceType.Tcpip)
            End If
        Next

        ndTcpip.ExpandAll()
    End Sub

    Private Function InResourceTree(ByVal resource As String) As Boolean
        Dim nd As TreeNode
        For Each nd In ndTcpip.Nodes
            If nd.Text = resource Then
                Return True
            End If
        Next

        Return False
    End Function

    Private Sub FindResources()
        Try
            Dim resources As String() = ResourceManager.GetLocalManager().FindResources(filter)

            If resources.Length = 0 Then
                MessageBox.Show("There was no resource found on your system.")
            End If

            Dim s As String
            For Each s In resources
                Dim intType As HardwareInterfaceType
                Dim intNum As Short
                ResourceManager.GetLocalManager().ParseResource(s, intType, intNum)
                AddToResourceNode(s, intType)
            Next

            AddToResourceTree()
        Catch ex As VisaException
            ' Don't do anything
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CleanResourceNodes()
        ndGpib.Nodes.Clear()
        ndVxi.Nodes.Clear()
        ndGpibVxi.Nodes.Clear()
        ndSerial.Nodes.Clear()
        ndPxi.Nodes.Clear()
        ndTcpip.Nodes.Clear()
        ndUSB.Nodes.Clear()
        ndFireWire.Nodes.Clear()
    End Sub

    Private Sub findResourcesButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles findResourcesButton.Click
        filter = filterStringsListBox.Text
        DisplayResources()
    End Sub

    Private Function getCustomFilter() As String
        Dim cff As CustomFilterForm = New CustomFilterForm
        cff.ShowDialog()
        Return cff.CustomFilter
    End Function

    Private Sub clearButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clearButton.Click
        resourceTreeView.Nodes.Clear()
        CleanResourceNodes()
    End Sub

    Private Sub useCustomStringButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles useCustomStringButton.Click
        filter = getCustomFilter()
        DisplayResources()
    End Sub

    Private Sub DisplayResources()
        resourceTreeView.Nodes.Clear()
        ndTcpipAdded = False
        CleanResourceNodes()
        FindResources()
        resourceTreeView.ExpandAll()
    End Sub

    Private Sub OnResourceNameChosen(ByVal sender As Object, ByVal e As System.EventArgs) Handles resourceTreeView.DoubleClick
        Try
            visaSession = ResourceManager.GetLocalManager().Open(resourceTreeView.SelectedNode.Text)
            Close()
        Catch ex As Exception
            MessageBox.Show("Resource was not opened successfully.")
        End Try
    End Sub

    Public ReadOnly Property OpenedSession() As Session
        Get
            Return visaSession
        End Get
    End Property

    Private Sub openButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openButton.Click
        Try
            visaSession = ResourceManager.GetLocalManager().Open(resourceNameTextBox.Text)
            Close()
        Catch ex As Exception
            MessageBox.Show("Resource was not opened successfully.")
        End Try
    End Sub
End Class
