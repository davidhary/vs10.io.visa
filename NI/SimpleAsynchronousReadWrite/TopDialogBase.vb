﻿Imports System.Drawing
''' <summary> Keeps dialog form on top. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/31/13" by="David" revision="6.1.4779.x"> created. </history>
Public Class TopDialogBase
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    Protected Sub New()


        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    Private WithEvents _onTopTimer As System.Windows.Forms.Timer
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._onTopTimer = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        '_onTopTimer
        '
        Me._onTopTimer.Interval = 1000
        '
        'TopDialogBase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(331, 341)
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "TopDialogBase"
        Me.Text = "Top Dialog Base"
        Me.ResumeLayout(False)

    End Sub

#End Region

    ''' <summary> Gets or sets the sentinel indicating that the form loaded without an exception.
    '''           Should be set only if load did not fail. </summary>
    ''' <value> The is loaded. </value>
    Protected Property IsLoaded As Boolean

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Load" /> event . </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As System.EventArgs)
        Try
            Me.IsLoaded = True
        Catch
            Throw
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event after enabling the
    ''' top most timer. </summary>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnShown(e As System.EventArgs)
        Me._onTopTimer.Enabled = True
        MyBase.OnShown(e)
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event after Disabling
    ''' the top most timer if not canceled. </summary>
    ''' <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
    ''' event data. </param>
    Protected Overrides Sub OnClosing(e As System.ComponentModel.CancelEventArgs)
        If e Is Nothing OrElse Not e.Cancel Then
            Me._onTopTimer.Enabled = False
        End If
        MyBase.OnClosing(e)
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Closed" /> event after disabling
    ''' the top most timer. </summary>
    ''' <param name="e"> The <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnClosed(e As System.EventArgs)
        If Me._onTopTimer IsNot Nothing Then
            Me._onTopTimer.Enabled = False
        End If
        MyBase.OnClosed(e)
    End Sub

    ''' <summary> Handles the Tick event of the _onTopTimer control. Sets <see cref="TopMost"></see>
    ''' true if not already true. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub _onTopTimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _onTopTimer.Tick
        If sender IsNot Nothing Then
            If Not Me.TopMost = True Then
                Me.TopMost = True
            End If
        End If
    End Sub

End Class