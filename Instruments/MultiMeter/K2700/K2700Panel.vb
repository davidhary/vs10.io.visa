#Region " IMPORTS "
Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Core.Controls.ControlExtensions
Imports isr.Core.Controls.ComboBoxExtensions
Imports isr.Core.Controls.CheckBoxExtensions
Imports isr.Core.Controls.NumericUpDownExtensions
Imports isr.Core.Controls.ToolStripExtensions
Imports isr.Core.Primitives
Imports isr.Core.Primitives.EnumExtensions
Imports isr.Core.Primitives.StringEscapeSequencesExtensions
#End Region
Namespace K2700

    ''' <summary> Provides a user interface for the Keithley 27XX Device. </summary>
    ''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/2008" by="David" revision="2.0.2936.x"> Create based on the 24xx
    ''' system classes. </history>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")>
    <System.ComponentModel.DisplayName("K2700 Panel"),
      System.ComponentModel.Description("Keithley 2700 Device Panel"),
      System.Drawing.ToolboxBitmap(GetType(K2700.K2700Panel))>
    Public Class K2700Panel
        #Const designMode1 = True
#Region " BASE FROM WRAPPER "
        ' Designing requires changing the condition to True.
#If designMode Then
        Inherits Visa.Instrument.ResourcePanelBaseWrapper
#Else
        Inherits Visa.Instrument.ResourcePanelBase
#End If
#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary> Default constructor. </summary>
        Public Sub New()
            MyBase.New()

            ' This call is required by the Windows Form Designer.
            InitializeComponent()

            ' instantiate the reference to the Device
            Me.Device = New K2700.Device()

            Me.onDisplayDeviceOpenChanged(Me.IsDeviceOpen)
            Me.initializeUserInterface()
            Me.onMeasurementAvailable(Nothing)
        End Sub

        ''' <summary> Disposes managed resources. </summary>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub onDisposeManagedResources()
            If Me.Device IsNot Nothing Then
                Try
                    ' remove event handlers.
                    Me.DeviceClosing(Me, System.EventArgs.Empty)
                    Me._Device.Dispose()
                    Me._Device = Nothing
                Catch ex As Exception
                    Debug.Assert(Not Debugger.IsAttached,
                                 "Exception occurred disposing Prober panel resources", "Exception details: {0}", ex)
                End Try
            End If
        End Sub

        ''' <summary> Enables or disables controls based on the device open state. </summary>
        ''' <param name="deviceIsOpen"> true if device is open. </param>
        Private Sub onDisplayDeviceOpenChanged(ByVal deviceIsOpen As Boolean)
            Me._Tabs.Enabled = True
            For Each t As Windows.Forms.TabPage In Me._Tabs.TabPages
                If t IsNot Me._MessagesTabPage Then
                    For Each c As Windows.Forms.Control In t.Controls : c.RecursivelyEnable(deviceIsOpen) : Next
                End If
            Next
        End Sub

        ''' <summary> Initialize User interface. </summary>
        Private Sub initializeUserInterface()
            Me.StatusRegisterToolStripStatusLabel.Visible = True
            Me.StandardRegisterToolStripStatusLabel.Visible = False
            Me.IdentityToolStripStatusLabel.Visible = False
        End Sub

#End Region

#Region " DEVICE "

        Private _Device As K2700.Device

        ''' <summary> Gets or sets a reference to the Keithley 2700 Device. </summary>
        ''' <value> The device. </value>
        <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
        Public Overloads Property Device() As K2700.Device
            Get
                Return Me._Device
            End Get
            Friend Set(ByVal value As K2700.Device)
                If value IsNot Nothing Then
                    Me._Device = value
                    MyBase.Device = value
                End If
            End Set
        End Property

#End Region

#Region " DEVICE EVENT HANDLERS "

        ''' <summary> Handle the device property changed event. </summary>
        ''' <param name="device">    The device. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnDevicePropertyChanged(ByVal device As Device, ByVal propertyName As String)
            If device Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "Enabled"
                        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Hardware {0};. ",
                                                   IIf(device.Enabled, "Enabled", "Disabled"))
                    Case "ResourcesSearchPattern"
                        MyBase.Connector.ResourcesSearchPattern = device.ResourcesSearchPattern
                    Case "ServiceRequestFailureMessage"
                        If Not String.IsNullOrWhiteSpace(device.ServiceRequestFailureMessage) Then
                            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, device.ServiceRequestFailureMessage)
                        End If
                    Case "SessionPropertyChangeHandlerEnabled"
                        Me._HandleServiceRequestsCheckBox.Checked = device.SessionPropertyChangeHandlerEnabled
#If False Then
                    Case "IsSessionOpen"
                    Case "IsDeviceOpen"
#End If
                End Select
            End If
        End Sub

        ''' <summary> Device property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Protected Overrides Sub DevicePropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnDevicePropertyChanged(TryCast(sender, Device), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception handling property changed Event;. Failed property {0}. Details: {1}",
                                           e.PropertyName, ex)
            End Try
        End Sub

        ''' <summary> Device service requested. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Message based session event information. </param>
        Protected Overrides Sub DeviceServiceRequested(ByVal sender As Object, ByVal e As NationalInstruments.VisaNS.MessageBasedSessionEventArgs)
            MyBase.DeviceServiceRequested(sender, e)
        End Sub

        ''' <summary> Event handler. Called upon device opening. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        Protected Overrides Sub DeviceOpening(ByVal sender As Object, ByVal e As System.EventArgs)
            MyBase.DeviceOpening(sender, e)
        End Sub

        ''' <summary> Event handler. Called when device opened. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        Protected Overrides Sub DeviceOpened(ByVal sender As Object, ByVal e As System.EventArgs)
            AddHandler Me.Device.FormatSubsystem.PropertyChanged, AddressOf Me.FormatSubsystemPropertyChanged
            AddHandler Me.Device.MeasureSubsystem.PropertyChanged, AddressOf Me.MeasureSubsystemPropertyChanged
            'AddHandler Me.Device.RouteSubsystem.PropertyChanged, AddressOf Me.RouteSubsystemPropertyChanged
            AddHandler Me.Device.SenseSubsystem.PropertyChanged, AddressOf Me.SenseSubsystemPropertyChanged
            AddHandler Me.Device.SenseVoltageSubsystem.PropertyChanged, AddressOf Me.SenseVoltageSubsystemPropertyChanged
            AddHandler Me.Device.SenseCurrentSubsystem.PropertyChanged, AddressOf Me.SenseCurrentSubsystemPropertyChanged
            AddHandler Me.Device.SenseFourWireResistanceSubsystem.PropertyChanged, AddressOf Me.SenseFourWireResistanceSubsystemPropertyChanged
            'AddHandler Me.Device.TraceSubsystem.PropertyChanged, AddressOf Me.TraceSubsystemPropertyChanged
            'AddHandler Me.Device.TriggerSubsystem.PropertyChanged, AddressOf Me.TriggerSubsystemPropertyChanged
            AddHandler Me.Device.StatusSubsystem.PropertyChanged, AddressOf Me.StatusSubsystemPropertyChanged
            AddHandler Me.Device.SystemSubsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged

            MyBase.DeviceOpened(sender, e)
            Me.onDisplayDeviceOpenChanged(Me.IsDeviceOpen)
            ' this defines the readings.
            Me.Device.InitializeKnownState()
            ' TO DO: Enable? should this be the first call of this function. 
            ' Me.EnableServiceRequestEventHandler()
        End Sub

        ''' <summary> Event handler. Called when device is closing. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        Protected Overrides Sub DeviceClosing(ByVal sender As Object, ByVal e As System.EventArgs)
            MyBase.DeviceClosing(sender, e)
            If Me.IsDeviceOpen Then
                RemoveHandler Me.Device.FormatSubsystem.PropertyChanged, AddressOf Me.FormatSubsystemPropertyChanged
                RemoveHandler Me.Device.MeasureSubsystem.PropertyChanged, AddressOf Me.MeasureSubsystemPropertyChanged
                'RemoveHandler Me.Device.RouteSubsystem.PropertyChanged, AddressOf Me.RouteSubsystemPropertyChanged
                RemoveHandler Me.Device.SenseCurrentSubsystem.PropertyChanged, AddressOf Me.SenseCurrentSubsystemPropertyChanged
                RemoveHandler Me.Device.SenseFourWireResistanceSubsystem.PropertyChanged, AddressOf Me.SenseFourWireResistanceSubsystemPropertyChanged
                RemoveHandler Me.Device.SenseVoltageSubsystem.PropertyChanged, AddressOf Me.SenseVoltageSubsystemPropertyChanged
                RemoveHandler Me.Device.SenseSubsystem.PropertyChanged, AddressOf Me.SenseSubsystemPropertyChanged
                'RemoveHandler Me.Device.TraceSubsystem.PropertyChanged, AddressOf Me.TraceSubsystemPropertyChanged
                'RemoveHandler Me.Device.TriggerSubsystem.PropertyChanged, AddressOf Me.TriggerSubsystemPropertyChanged
                RemoveHandler Me.Device.StatusSubsystem.PropertyChanged, AddressOf Me.StatusSubsystemPropertyChanged
                RemoveHandler Me.Device.SystemSubsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
            End If
        End Sub

        ''' <summary> Event handler. Called when device is closed. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        Protected Overrides Sub DeviceClosed(ByVal sender As Object, ByVal e As System.EventArgs)
            Me.onDisplayDeviceOpenChanged(Me.IsDeviceOpen)
            MyBase.DeviceClosed(sender, e)
        End Sub

#End Region

#Region " SUBSYSTEMS "

#Region " FORMAT "

        ''' <summary> Handle the format subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As FormatSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "Elements"
                        If Me.Device IsNot Nothing AndAlso subsystem.Elements <> ReadingElements.None Then
                            Dim selectedIndex As Integer = Me._ReadingComboBox.SelectedIndex
                            With Me._ReadingComboBox
                                .DataSource = Nothing
                                .Items.Clear()
                                .DataSource = GetType(Visa.ReadingElements).ValueDescriptionPairs(subsystem.Elements And Not ReadingElements.Units)
                                .DisplayMember = "Value"
                                .ValueMember = "Key"
                                If .Items.Count > 0 Then
                                    .SelectedIndex = Math.Max(selectedIndex, 0)
                                End If
                            End With
                        End If
#If False Then
                    Case "IntegrationPeriod"
                    Case "LastStatus"
#End If
                End Select
            End If
        End Sub

        ''' <summary> Format subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub FormatSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, FormatSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception handling Format Subsystem property changed Event;. Failed property {0}. Details: {1}",
                                           e.PropertyName, ex)
            End Try
        End Sub

#End Region

#Region " MEASURE "

        ''' <summary> Executes the measurement available action. </summary>
        ''' <param name="readings"> The readings. </param>
        Private Sub onMeasurementAvailable(ByVal readings As K2700.Readings)

            Const clear As String = "    "
            Const compliance As String = "..C.."
            Const rangeCompliance As String = ".RC."
            Const levelCompliance As String = ".LC."

            If readings Is Nothing Then
                Me._ReadingToolStripStatusLabel.Text = "-.------- V"
                Me._ComplianceToolStripStatusLabel.Text = clear
                Me._TbdToolStripStatusLabel.Text = clear
            Else
                Me._ReadingToolStripStatusLabel.SafeTextSetter(readings.ToString(Me.selectedReading))
                If readings.Reading.MetaStatus.HitCompliance Then

                    Me._ComplianceToolStripStatusLabel.Text = compliance
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "Real Compliance.  Instrument sensed an output overflow of the measured value.")

                ElseIf readings.Reading.MetaStatus.HitRangeCompliance Then

                    Me._ComplianceToolStripStatusLabel.Text = rangeCompliance
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "Range Compliance.  Instrument sensed an output overflow of the measured value.")

                ElseIf readings.Reading.MetaStatus.HitLevelCompliance Then

                    Me._ComplianceToolStripStatusLabel.Text = levelCompliance
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "Level Compliance.  Instrument sensed an output overflow of the measured value.")

                Else

                    Me._ComplianceToolStripStatusLabel.Text = "  "
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "Instruments parsed reading elements.")

                End If

            End If


        End Sub

        ''' <summary> Handles the visa status available action. </summary>
        ''' <param name="subsystem"> The subsystem. </param>
        Private Sub onVisaStatusAvailable(ByVal subsystem As MeasureSubsystem)
            If subsystem.LastStatus > NationalInstruments.VisaNS.VisaStatusCode.Success Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                           "Visa operation warning;. Visa Status: {0}",
                                           VisaException.BuildVisaStatusDetails(subsystem.LastStatus))
            ElseIf subsystem.LastStatus < NationalInstruments.VisaNS.VisaStatusCode.Success Then
                Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                           "Visa operation failed;. Visa Status: {0}",
                                           VisaException.BuildVisaStatusDetails(subsystem.LastStatus))
            End If
        End Sub

        ''' <summary> Handles the Measure subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As MeasureSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "LastReading"
                        Me._LastReadingTextBox.SafeTextSetter(subsystem.LastReading)
                        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                                   "Measure message: {0}.", subsystem.LastReading.InsertCommonEscapeSequences)
                    Case "LastStatus"
                        Me.onVisaStatusAvailable(subsystem)
                    Case "MeasurementAvailable"
                        Me.onMeasurementAvailable(subsystem.Readings)
                    Case "Reading"
                End Select
            End If
        End Sub

        ''' <summary> Measure subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub MeasureSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, MeasureSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception handling Measure Subsystem property changed Event;. Failed property {0}. Details: {1}",
                                           e.PropertyName, ex)
            End Try
        End Sub

#End Region

#Region " ROUTE "

#If False Then

        ''' <summary> Handle the Route subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As RouteSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "ScanList"
                    Case "TerminalMode"
                End Select
            End If
        End Sub

        ''' <summary> Route subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub RouteSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, RouteSubsystem), e.PropertyName)
                End if
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceId, 
                                           "Exception handling Route Subsystem property changed Event;. Failed property {0}. Details: {1}",
                                           e.PropertyName, ex)
            End Try
        End Sub
#End If
#End Region

#Region " SENSE "

        ''' <summary> Handles the supported function modes changed action. </summary>
        ''' <param name="subsystem"> The subsystem. </param>
        Private Sub onSupportedFunctionModesChanged(ByVal subsystem As SenseSubsystem)
            If subsystem IsNot Nothing AndAlso subsystem.SupportedFunctionModes <> Visa.Scpi.SenseFunctionModes.None Then
                With Me._SenseFunctionComboBox
                    .DataSource = Nothing
                    .Items.Clear()
                    .DataSource = GetType(Visa.Scpi.SenseFunctionModes).ValueDescriptionPairs(subsystem.SupportedFunctionModes)
                    .DisplayMember = "Value"
                    .ValueMember = "Key"
                    If .Items.Count > 0 Then
                        .SelectedItem = Visa.Scpi.SenseFunctionModes.VoltageDC.ValueDescriptionPair()
                    End If
                End With
            End If
        End Sub

        ''' <summary> Handles the function modes changed action. </summary>
        ''' <param name="subsystem"> The subsystem. </param>
        Private Sub onFunctionModesChanged(ByVal subsystem As SenseSubsystem)
            If subsystem IsNot Nothing AndAlso subsystem.FunctionMode.HasValue Then
                Dim value As Visa.Scpi.SenseFunctionModes = subsystem.FunctionMode.GetValueOrDefault(Visa.Scpi.SenseFunctionModes.None)
                If value <> Visa.Scpi.SenseFunctionModes.None Then
                    If Not Visa.Scpi.SenseSubsystemBase.TryParse(value, Me.Device.MeasureSubsystem.Readings.Reading.Unit) Then
                        Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                                   "Failed parsing function mode '{0}' to a standard unit.",
                                                   subsystem.FunctionMode.Value)
                    End If
                    Me._SenseFunctionComboBox.SafeSelectItem(value, value.Description)
                    Select Case value
                        Case Scpi.SenseFunctionModes.CurrentDC, Scpi.SenseFunctionModes.Current, Scpi.SenseFunctionModes.CurrentAC
                            With Me._SenseRangeNumeric
                                .Minimum = 0
                                .Maximum = 10D
                                .DecimalPlaces = 3
                            End With
                        Case Scpi.SenseFunctionModes.VoltageDC, Scpi.SenseFunctionModes.Voltage, Scpi.SenseFunctionModes.VoltageAC
                            With Me._SenseRangeNumeric
                                .Minimum = 0
                                .Maximum = 1000D
                                .DecimalPlaces = 3
                            End With
                        Case Scpi.SenseFunctionModes.FourWireResistance, Scpi.SenseFunctionModes.Resistance
                            With Me._SenseRangeNumeric
                                .Minimum = 0
                                .Maximum = 1000000000D
                                .DecimalPlaces = 0
                            End With
                    End Select
                End If
            End If
        End Sub

        ''' <summary> Handle the Sense subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As SenseSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                ' Me._senseRangeTextBox.SafeTextSetter(Me.Device.SenseRange(Visa.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
                ' Me._integrationPeriodTextBox.SafeTextSetter(Me.Device.SenseIntegrationPeriodCaption)
                Select Case propertyName
#If False Then
                    Case "Reading"
                    Case "ConcurrentSenseEnabled"
                    Case "ProtectionLevel"
                    Case "SupportsMultiFunctions"
#End If
                    Case "MeasurementAvailable"
                        Me.onMeasurementAvailable(subsystem.Readings)
                    Case "SupportedFunctionModes"
                        Me.onSupportedFunctionModesChanged(subsystem)
                    Case "FunctionMode"
                        Me.onFunctionModesChanged(subsystem)
                        Me._SenseRangeNumericLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                        "Range [{0}]:", subsystem.Readings.Reading.Unit.Symbol)
                End Select
            End If
        End Sub

        ''' <summary> Sense subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub SenseSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, SenseSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception handling Sense Subsystem property changed Event;. Failed property {0}. Details: {1}",
                                           e.PropertyName, ex)
            End Try
        End Sub

#End Region

#Region " SENSE VOLTAGE "

        ''' <summary> Handle the Sense subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As SenseVoltageSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                ' Me._senseRangeTextBox.SafeTextSetter(Me.Device.SenseRange(Visa.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
                ' Me._integrationPeriodTextBox.SafeTextSetter(Me.Device.SenseIntegrationPeriodCaption)
                Select Case propertyName
#If False Then
                    Case "Reading"
                    Case "ConcurrentSenseEnabled"
                    Case "ProtectionLevel"
                    Case "SupportsMultiFunctions"
#End If
                    Case "AutoRangeEnabled"
                        If Me.Device IsNot Nothing AndAlso subsystem.AutoRangeEnabled.HasValue Then
                            Me._SenseAutoRangeToggle.SafeCheckedSetter(subsystem.AutoRangeEnabled.Value)
                        End If
                    Case "PowerLineCycles"
                        If Me.Device IsNot Nothing AndAlso subsystem.PowerLineCycles.HasValue Then
                            Dim nplc As Double = subsystem.PowerLineCycles.Value
                            Me._IntegrationPeriodNumeric.SafeValueSetter(Visa.SystemSubsystemBase.IntegrationPeriod(nplc).TotalMilliseconds)
                        End If
                    Case "Range"
                        If Me.Device IsNot Nothing AndAlso subsystem.Range.HasValue Then
                            Me._SenseRangeNumeric.SafeValueSetter(subsystem.Range.Value)
                        End If
                End Select
            End If
        End Sub

        ''' <summary> Sense voltage subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub SenseVoltageSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, SenseVoltageSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception handling Sense Voltage Subsystem property changed Event;. Failed property {0}. Details: {1}",
                                           e.PropertyName, ex)
            End Try
        End Sub

#End Region

#Region " SENSE CURRENT "

        ''' <summary> Handle the Sense subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As SenseCurrentSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                ' Me._senseRangeTextBox.SafeTextSetter(Me.Device.SenseRange(Visa.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
                ' Me._integrationPeriodTextBox.SafeTextSetter(Me.Device.SenseIntegrationPeriodCaption)
                Select Case propertyName
#If False Then
                    Case "Reading"
                    Case "ConcurrentSenseEnabled"
                    Case "ProtectionLevel"
                    Case "SupportsMultiFunctions"
#End If
                    Case "AutoRangeEnabled"
                        If Me.Device IsNot Nothing AndAlso subsystem.AutoRangeEnabled.HasValue Then
                            Me._SenseAutoRangeToggle.SafeCheckedSetter(subsystem.AutoRangeEnabled.Value)
                        End If
                    Case "PowerLineCycles"
                        If Me.Device IsNot Nothing AndAlso subsystem.PowerLineCycles.HasValue Then
                            Dim nplc As Double = subsystem.PowerLineCycles.Value
                            Me._IntegrationPeriodNumeric.SafeValueSetter(Visa.SystemSubsystemBase.IntegrationPeriod(nplc).TotalMilliseconds)
                        End If
                    Case "Range"
                        If Me.Device IsNot Nothing AndAlso subsystem.Range.HasValue Then
                            Me._SenseRangeNumeric.SafeValueSetter(subsystem.Range.Value)
                        End If
                End Select
            End If
        End Sub

        ''' <summary> Sense Current subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub SenseCurrentSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, SenseCurrentSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception handling Sense Current Subsystem property changed Event;. Failed property {0}. Details: {1}",
                                           e.PropertyName, ex)
            End Try
        End Sub

#End Region

#Region " SENSE RESISTANCE "

        ''' <summary> Handle the Sense subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As SenseFourWireResistanceSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                ' Me._senseRangeTextBox.SafeTextSetter(Me.Device.SenseRange(Visa.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
                ' Me._integrationPeriodTextBox.SafeTextSetter(Me.Device.SenseIntegrationPeriodCaption)
                Select Case propertyName
#If False Then
                    Case "Reading"
                    Case "ConcurrentSenseEnabled"
                    Case "ProtectionLevel"
                    Case "SupportsMultiFunctions"
#End If
                    Case "AutoRangeEnabled"
                        If Me.Device IsNot Nothing AndAlso subsystem.AutoRangeEnabled.HasValue Then
                            Me._SenseAutoRangeToggle.SafeCheckedSetter(subsystem.AutoRangeEnabled.Value)
                        End If
                    Case "PowerLineCycles"
                        If Me.Device IsNot Nothing AndAlso subsystem.PowerLineCycles.HasValue Then
                            Dim nplc As Double = subsystem.PowerLineCycles.Value
                            Me._IntegrationPeriodNumeric.SafeValueSetter(Visa.SystemSubsystemBase.IntegrationPeriod(nplc).TotalMilliseconds)
                        End If
                    Case "Range"
                        If Me.Device IsNot Nothing AndAlso subsystem.Range.HasValue Then
                            Me._SenseRangeNumeric.SafeValueSetter(subsystem.Range.Value)
                        End If
                End Select
            End If
        End Sub

        ''' <summary> Sense Four Wire Resistance subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub SenseFourWireResistanceSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, SenseFourWireResistanceSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception handling Sense Resistance Subsystem property changed Event;. Failed property {0}. Details: {1}",
                                           e.PropertyName, ex)
            End Try
        End Sub

#End Region

#Region " TRACE "
#If False Then

        ''' <summary> Handle the Trace subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As TraceSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "FeedSource"
                    Case "NextFeedEnabled"
                    Case "PointsCount"
                End Select
            End If
        End Sub


        ''' <summary> Trace subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub TraceSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, TraceSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceId, 
                                           "Exception handling Trace Subsystem property changed Event;. Failed property {0}. Details: {1}",
                                           e.PropertyName, ex)
            End Try
        End Sub
#End If
#End Region

#Region " TRIGGER "
#If False Then

        ''' <summary> Handle the Trigger subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As TriggerSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                ' Me._triggerDelayTextBox.SafeTextSetter(Me.Device.TriggerDelay(Visa.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
                Select Case propertyName
                    Case "ArmSource"
                    Case "AutoDelayEnabled"
                    Case "Count"
                    Case "Delay"
                    Case "TimerTimeSpan"
                End Select
            End If
        End Sub

        ''' <summary> Trigger subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub TriggerSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, TriggerSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceId, 
                                           "Exception handling Trigger Subsystem property changed Event;. Failed property {0}. Details: {1}",
                                           e.PropertyName, ex)
            End Try
        End Sub
#End If
#End Region

#Region " STATUS "

        ''' <summary> Handle the Status subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As StatusSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "Identity"
                        If Not String.IsNullOrWhiteSpace(subsystem.Identity) Then
                            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                                       "{0} identified as {1}.", Me.ResourceName, subsystem.Identity)

                        End If
                    Case "DeviceErrors"
                        If Not String.IsNullOrWhiteSpace(subsystem.DeviceErrors) Then
                            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                                       "{0} Device errors: {1}", Me.ResourceName, subsystem.DeviceErrors)
                        End If
                    Case "ErrorAvailable"
                        If Me.IsDeviceOpen AndAlso Me.Device.StatusSubsystem.ErrorAvailable Then
                            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Error available;. ")
                            Me.Device.SystemSubsystem.QueryLastError()
                        End If
                    Case "ServiceRequestStatus"
                        Me.DisplayStatusRegisterStatus(Me.Device.StatusSubsystem.ServiceRequestStatus)
#If False Then
                    Case "ErrorAvailable"
                    Case "ErrorAvailableBits"
                    Case "MeasurementAvailable"
                    Case "MeasurementAvailableBits"
                    Case "MeasurementEventCondition"
                    Case "MeasurementEventEnableBitmask"
                    Case "MeasurementEventStatus"
                    Case "MessageAvailable"
                    Case "MessageAvailableBits"
                    Case "OperationEventCondition"
                    Case "OperationEventEnableBitmask"
                    Case "OperationEventStatus"
                    Case "OperationNegativeTransitionEventEnableBitmask"
                    Case "OperationPositiveTransitionEventEnableBitmask"
                    Case "QuestionableEventCondition"
                    Case "QuestionableEventEnableBitmask"
                    Case "QuestionableEventStatus"
                    Case "ServiceRequestEnableBitmask"
                    Case "ServiceRequestStatus"
                    Case "StandardDeviceErrorAvailable"
                    Case "StandardDeviceErrorAvailableBits"
                    Case "StandardEventEnableBitmask"
                    Case "StandardEventStatus"
                    Case "VersionInfo"
#End If
                End Select
            End If
        End Sub


        ''' <summary> Status subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub StatusSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, StatusSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception handling Status Subsystem property changed Event;. Failed property {0}. Details: {1}",
                                           e.PropertyName, ex)
            End Try
        End Sub
#End Region

#Region " SYSTEM "

        ''' <summary> Reports the last error. </summary>
        ''' <param name="subsystem"> The subsystem. </param>
        Private Sub onLastError(ByVal subsystem As SystemSubsystem)
            If subsystem Is Nothing Then
                Throw New ArgumentNullException("subsystem")
            End If
            If Me.IsDeviceOpen AndAlso subsystem.LastError IsNot Nothing Then
                If subsystem.LastError.ErrorNumber = 0 Then
                    Me._LastErrorTextBox.ForeColor = Drawing.Color.Aquamarine
                Else
                    Me._LastErrorTextBox.ForeColor = Drawing.Color.OrangeRed
                    Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Error;. Last error: {0}",
                                               subsystem.LastError.CompoundErrorMessage)
                End If
                Me._LastErrorTextBox.Text = subsystem.LastError.CompoundErrorMessage
            Else
                Me._LastErrorTextBox.ForeColor = Drawing.Color.Aquamarine
                Me._LastErrorTextBox.Text = ""
            End If

        End Sub

        ''' <summary> Handle the System subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As SystemSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "LastError"
                        Me.onLastError(subsystem)
                    Case "FrontSwitched"
                        Me._TerminalsToggle.SafeSilentCheckStateSetter((Not subsystem.FrontSwitched).ToCheckState)
                        Windows.Forms.Application.DoEvents()
#If False Then
                    Case "AutoZeroEnabled"
                    Case "BeeperEnabled"
                    Case "FourWireSenseEnabled"
                    Case "LineFrequency"
                    Case "ScpiRevision"
                    Case "StationLineFrequency"
#End If
                End Select
            End If
        End Sub

        ''' <summary> System subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub SystemSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, SystemSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception handling System Subsystem property changed Event;. Failed property {0}. Details: {1}",
                                           e.PropertyName, ex)
            End Try
        End Sub
#End Region

#End Region

#Region " TRACE MESSAGES HANDLERS "

        ''' <summary> Displays the <paramref name="value">message</paramref>. </summary>
        ''' <param name="value"> The <see cref="Core.Diagnosis.TraceMessage">message</see> to display and log. </param>
        Protected Overrides Sub DisplayMessage(value As Core.Diagnosis.TraceMessage)
            If value IsNot Nothing AndAlso MyBase.ShouldShow(value.EventType) Then
                Me._MessagesBox.AddMessage(value)
            End If
        End Sub

        ''' <summary> Gets the trace display level. </summary>
        ''' <value> The trace display level. </value>
        Public Property TraceDisplayLevel As TraceEventType = TraceEventType.Verbose

        Protected Overrides ReadOnly Property TraceShowLevel As System.Diagnostics.TraceEventType
            Get
                Return Me.TraceDisplayLevel
            End Get
        End Property

#End Region

#Region " DISPLAY: TITLE "

        ''' <summary> Gets or sets the title. </summary>
        ''' <value> The title. </value>
        <Category("Appearance"), Description("The title of this panel"),
            Browsable(True),
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
            DefaultValue("K2700")>
        Public Property Title As String
            Get
                Return Me._TitleLabel.Text
            End Get
            Set(value As String)
                Me._TitleLabel.Text = value
                Me._TitleLabel.Visible = Not String.IsNullOrWhiteSpace(value)
            End Set
        End Property

#End Region

#Region " DISPLAY SETTINGS: READING "

        ''' <summary> Selects a new reading to display. </summary>
        ''' <param name="value"> The <see cref="Core.Diagnosis.TraceMessage">message</see> to display and
        ''' log. </param>
        ''' <returns> The Visa.ReadingElements. </returns>
        Friend Function SelectReading(ByVal value As Visa.ReadingElements) As Visa.ReadingElements
            If Me.IsDeviceOpen AndAlso
                (value <> Visa.ReadingElements.None) AndAlso (value <> Me.selectedReading) Then
                Me._ReadingComboBox.SafeSelectItem(value.ValueDescriptionPair)
            End If
            Return Me.selectedReading
        End Function

        ''' <summary> Gets the selected reading. </summary>
        ''' <value> The selected reading. </value>
        <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
        Private ReadOnly Property selectedReading() As Visa.ReadingElements
            Get
                Return CType(CType(Me._ReadingComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(
                                            Of [Enum], String)).Key, Visa.ReadingElements)
            End Get
        End Property

#End Region

#Region " DEVICE SETTINGS: FUNCTION MODE "

        Private _actualFunctionMode As Visa.Scpi.SenseFunctionModes
        ''' <summary>
        ''' Return the last Sense function mode.  This is set only after the 
        ''' actual value is applies whereas the <see cref="SelectedFunctionMode">selected mode</see>
        ''' reflects the status of the combo box.
        ''' </summary>
        Friend ReadOnly Property ActualFunctionMode() As Visa.Scpi.SenseFunctionModes
            Get
                Return Me._actualFunctionMode
            End Get
        End Property

        ''' <summary>
        ''' Selects a new sense mode.
        ''' </summary>
        Friend Sub ApplyFunctionMode(ByVal value As Visa.Scpi.SenseFunctionModes)
            If Me.IsDeviceOpen AndAlso
                ((Me._Device.SenseSubsystem.SupportedFunctionModes And value) <> 0) AndAlso
                (value <> Me._actualFunctionMode) Then
                Me._Device.SenseSubsystem.ApplyFunctionMode(value)
            End If
        End Sub

        ''' <summary>
        ''' Gets or sets the selected function mode.
        ''' </summary>
        <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
        Private ReadOnly Property selectedFunctionMode() As Visa.Scpi.SenseFunctionModes
            Get
                Return CType(CType(Me._SenseFunctionComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(
                  Of [Enum], String)).Key, Visa.Scpi.SenseFunctionModes)
            End Get
        End Property

#End Region

#Region " CHANNELS "

        ''' <summary> Gets or sets the scan list of closed channels. </summary>
        ''' <value> The closed channels. </value>
        Public Property ClosedChannels() As String
            Get
                Return Me._ClosedChannelsTextBox.Text
            End Get
            Set(ByVal value As String)
                Me._ClosedChannelsTextBox.SafeTextSetter(value)
            End Set
        End Property

        ''' <summary> Adds new items to the combo box. </summary>
        Friend Sub updateChannelListComboBox()
            If MyBase.Visible Then
                ' check if we are asking for a new channel list
                If Me._ChannelListComboBox IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(Me._ChannelListComboBox.Text) AndAlso
                    Me._ChannelListComboBox.FindString(Me._ChannelListComboBox.Text) < 0 Then
                    ' if we have a new string, add it to the channel list
                    Me._ChannelListComboBox.Items.Add(Me._ChannelListComboBox.Text)
                End If
            End If
        End Sub

        ''' <summary> Event handler. Called by _closeChannelsButton for click events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _closeChannelsButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _CloseChannelsButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                updateChannelListComboBox()
                Me.Device.ClearExecutionState()
                Me.Device.RouteSubsystem.CloseChannels(Me._ChannelListComboBox.Text)
            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred initiating a measurement;. Details: {0}",
                                           ex)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Event handler. Called by channelOpenButton for click events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub channelOpenButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _OpenChannelsButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                updateChannelListComboBox()
                Me.Device.ClearExecutionState()
                Me.Device.RouteSubsystem.OpenChannels(Me._ChannelListComboBox.Text)
            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred initiating a measurement;. Details: {0}",
                                           ex)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Event handler. Called by CloseOnlyButton for click events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub CloseOnlyButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _CloseOnlyButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                updateChannelListComboBox()
                Me.Device.ClearExecutionState()
                Me.Device.RouteSubsystem.OpenAll()
                Me.Device.RouteSubsystem.CloseChannels(Me._ChannelListComboBox.Text)
                ' this works only if a single channel:
                ' Visa.RouteSubsystem.CloseChannels(Me.Device, Me._channelListComboBox.Text)
            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred initiating a measurement;. Details: {0}",
                                           ex)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Event handler. Called by OpenAllButton for click events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub OpenAllButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _OpenAllButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                Me.Device.ClearExecutionState()
                Me.Device.RouteSubsystem.OpenAll()
            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred initiating a measurement;. Details: {0}",
                                           ex)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: RESET "

        ''' <summary> Event handler. Called by _SessionTraceEnableCheckBox for checked changed events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _SessionTraceEnableCheckBox_CheckedChanged(ByVal sender As Object, e As System.EventArgs) Handles _SessionTraceEnableCheckBox.CheckedChanged
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If Not Me.DesignMode AndAlso sender IsNot Nothing Then
                    Dim checkBox As Windows.Forms.CheckBox = CType(sender, Windows.Forms.CheckBox)
                    If checkBox.Enabled Then
                        Me.Device.SessionPropertyChangeHandlerEnabled = checkBox.Checked
                        If Me.Device.SessionPropertyChangeHandlerEnabled = checkBox.Checked Then
                            Me.Device.SessionMessagesTraceEnabled = checkBox.Checked
                        Else
                            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                                       "Failed to toggle the session property handler")
                        End If
                    End If
                End If
            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred initiating a measurement;. Details: {0}",
                                           ex)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Event handler. Called by interfaceClearButton for click events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _InterfaceClearButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _InterfaceClearButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If Me.IsDeviceOpen AndAlso Me.Device.IsSessionOpen Then
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "Clearing interface '{0}';. ", Me.Device.Session.InterfaceResourceName)
                    Me.Device.SystemSubsystem.ClearInterface()
                End If
            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred clearing interface;. Details: {0}",
                                          ex)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Event handler. Called by _SelectiveDeviceClearButton for click events. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _SelectiveDeviceClearButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SelectiveDeviceClearButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If Me.IsDeviceOpen AndAlso Me.Device.IsSessionOpen Then
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "'{0}' Clearing Device '{1}';. ",
                                                Me.Device.Session.InterfaceResourceName, Me.ResourceName)
                    Me.Device.SystemSubsystem.ClearDevice()
                End If
            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred sending SDC;. Details: {0}",
                                          ex)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Issue RST. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _ResetButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ResetButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If Me.IsDeviceOpen Then
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "Resetting known state for '{0}';. ", Me.ResourceName)
                    Me.Device.ResetKnownState()
                End If
            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred resetting known state;. Details: {0}",
                                           ex)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Event handler. Called by _InitializeKnownStateButton for click events. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _InitializeKnownStateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _InitializeKnownStateButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If Me.IsDeviceOpen Then
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "Initializing known state for '{0}';. ", Me.ResourceName)
                    Me.Device.InitializeKnownState()
                End If
            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred initializing known state;. Details: {0}",
                                           ex)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: READING "

        ''' <summary> Event handler. Called by InitButton for click events. Initiates a reading for
        ''' retrieval by way of the service request event. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub InitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _InitiateButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()

                ' clear execution state before enabling events
                Me.Device.ClearExecutionState()

                ' set the service request
                Me.Device.StatusSubsystem.ApplyMeasurementEventEnableBitmask(MeasurementEvents.All)
                Me.Device.StatusSubsystem.EnableServiceRequest(Visa.ServiceRequests.All And Not Visa.ServiceRequests.MessageAvailable)

                ' trigger the initiation of the measurement letting the service request do the rest.
                Me.Device.ClearExecutionState()
                Me.Device.TriggerSubsystem.Initiate()

            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred initiating a measurement;. Details: {0}",
                                           ex)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try

        End Sub

        ''' <summary> Event handler. Called by _ReadingComboBox for selected index changed events. Selects
        ''' a new reading to display. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _ReadingComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ReadingComboBox.SelectedIndexChanged
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                If Me._ReadingComboBox.Enabled AndAlso Me._ReadingComboBox.SelectedIndex >= 0 AndAlso
                    Not String.IsNullOrWhiteSpace(Me._ReadingComboBox.Text) Then
                    Me.onMeasurementAvailable(Me.Device.MeasureSubsystem.Readings)
                End If
            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred initiating a measurement;. Details: {0}",
                                           ex)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try

        End Sub

        ''' <summary> Event handler. Called by _ReadButton for click events. Query the Device for a
        ''' reading. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _ReadButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ReadButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()

                ' update the terminal display.
                Me.Device.SystemSubsystem.QueryFrontSwitched()

                ' update display modalities if changed.
                Me.Device.MeasureSubsystem.Read()

            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred initiating a measurement;. Details: {0}",
                                           ex)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try

        End Sub

        ''' <summary> Event handler. Called by _terminalsToggle for click events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        Private Sub _terminalstoggle_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _TerminalsToggle.CheckStateChanged

            Dim checkBox As Windows.Forms.CheckBox = TryCast(sender, Windows.Forms.CheckBox)
            If checkBox IsNot Nothing Then
                If checkBox.CheckState = Windows.Forms.CheckState.Indeterminate Then
                    checkBox.Text = "R/F?"
                Else
                    checkBox.Text = CStr(IIf(checkBox.Checked, "Rear", "Front"))
                End If
            End If

        End Sub

        ''' <summary> Event handler. Called by _HandleServiceRequestsCheckBox for check state changed
        ''' events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        Private Sub _HandleServiceRequestsCheckBox_CheckStateChanged(sender As Object, e As System.EventArgs) Handles _HandleServiceRequestsCheckBox.CheckStateChanged
            If sender IsNot Nothing AndAlso Me.IsDeviceOpen Then
                Dim checkBox As CheckBox = TryCast(sender, CheckBox)
                If checkBox IsNot Nothing AndAlso Not checkBox.Checked = Me.Device.ServiceRequestEventHandlerEnabled Then
                    If checkBox IsNot Nothing AndAlso checkBox.Checked Then
                        Me.EnableServiceRequestEventHandler()
                        Me.Device.StatusSubsystem.EnableServiceRequest(ServiceRequests.All)
                    Else
                        Me.Device.StatusSubsystem.EnableServiceRequest(ServiceRequests.None)
                        Me.DisableServiceRequestEventHandler()
                    End If
                    Me.Device.StatusSubsystem.ReadRegisters()
                End If
            End If

        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: SENSE "

        ''' <summary> Event handler. Called by _SenseFunctionComboBox for selected index changed
        ''' events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="degC")>
        Private Sub _SenseFunctionComboBox_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles _SenseFunctionComboBox.SelectedIndexChanged
            Dim format As String = "Range [{0}]:"
            Dim control As Windows.Forms.Control = TryCast(sender, Windows.Forms.Control)
            If control IsNot Nothing AndAlso control.Enabled Then
                Select Case Me.selectedFunctionMode
                    Case Scpi.SenseFunctionModes.Current, Scpi.SenseFunctionModes.CurrentAC, Scpi.SenseFunctionModes.CurrentDC
                        Me._SenseRangeNumericLabel.Text = String.Format(format, "A")
                    Case Scpi.SenseFunctionModes.FourWireResistance, Scpi.SenseFunctionModes.Resistance
                        Me._SenseRangeNumericLabel.Text = String.Format(format, "Ohm")
                    Case Scpi.SenseFunctionModes.Frequency
                        Me._SenseRangeNumericLabel.Text = String.Format(format, "Hz")
                    Case Scpi.SenseFunctionModes.Period
                        Me._SenseRangeNumericLabel.Text = String.Format(format, "S")
                    Case Scpi.SenseFunctionModes.Temperature
                        Me._SenseRangeNumericLabel.Text = String.Format(format, "degC")
                    Case Scpi.SenseFunctionModes.Voltage, Scpi.SenseFunctionModes.VoltageAC, Scpi.SenseFunctionModes.VoltageDC
                        Me._SenseRangeNumericLabel.Text = String.Format(format, "V")
                End Select
                Me._SenseRangeNumericLabel.Left = Me._SenseRangeNumeric.Left - Me._SenseRangeNumericLabel.Width
            End If
        End Sub

        ''' <summary>
        ''' Applies the selected measurements settings.
        ''' </summary>
        Private Sub applySenseSettings()

            Me.Device.ClearExecutionState()

            Me.Device.SenseSubsystem.ApplyFunctionMode(Me.selectedFunctionMode)

            If Me.Device.SenseSubsystem.FunctionMode = Scpi.SenseFunctionModes.CurrentDC Then

                Me.Device.MeasureSubsystem.Readings.Reading.Unit = Arebis.StandardUnits.ElectricUnits.Ampere

                With Me.Device.SenseCurrentSubsystem
                    ' set the Integration Period
                    .ApplyPowerLineCycles(Visa.SystemSubsystemBase.PowerLineCycles(TimeSpan.FromMilliseconds(Me._IntegrationPeriodNumeric.Value)))

                    ' set the range
                    .ApplyAutoRangeEnabled(Me._SenseAutoRangeToggle.Checked)

                    If Not Me._SenseAutoRangeToggle.Checked Then
                        .ApplyRange(Me._SenseRangeNumeric.Value)
                    End If
                End With

            ElseIf Me.Device.SenseSubsystem.FunctionMode = Scpi.SenseFunctionModes.FourWireResistance Then

                Me.Device.MeasureSubsystem.Readings.Reading.Unit = Arebis.StandardUnits.ElectricUnits.Ohm

                With Me.Device.SenseFourWireResistanceSubsystem
                    ' set the Integration Period
                    .ApplyPowerLineCycles(Visa.SystemSubsystemBase.PowerLineCycles(TimeSpan.FromMilliseconds(Me._IntegrationPeriodNumeric.Value)))

                    ' set the range
                    .ApplyAutoRangeEnabled(Me._SenseAutoRangeToggle.Checked)

                    If Not Me._SenseAutoRangeToggle.Checked Then
                        .ApplyRange(Me._SenseRangeNumeric.Value)
                    End If
                End With

            ElseIf Me.Device.SenseSubsystem.FunctionMode = Scpi.SenseFunctionModes.VoltageDC Then

                Me.Device.MeasureSubsystem.Readings.Reading.Unit = Arebis.StandardUnits.ElectricUnits.Volt

                With Me.Device.SenseVoltageSubsystem
                    ' set the Integration Period
                    .ApplyPowerLineCycles(Visa.SystemSubsystemBase.PowerLineCycles(TimeSpan.FromMilliseconds(Me._IntegrationPeriodNumeric.Value)))

                    ' set the range
                    .ApplyAutoRangeEnabled(Me._SenseAutoRangeToggle.Checked)

                    If Not Me._SenseAutoRangeToggle.Checked Then
                        .ApplyRange(Me._SenseRangeNumeric.Value)
                    End If
                End With

            End If

            ' get the delay time
            If Me._TriggerDelayNumeric.Value >= 0 Then
                Me.Device.TriggerSubsystem.ApplyDelay(TimeSpan.FromSeconds(Me._TriggerDelayNumeric.Value))
            Else
                Me.Device.TriggerSubsystem.ApplyAutoDelayEnabled(True)
            End If

        End Sub

        ''' <summary> Event handler. Called by ApplySenseSettingsButton for click events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub ApplySenseSettingsButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ApplySenseSettingsButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                Me.applySenseSettings()
            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred initiating a measurement;. Details: {0}",
                                           ex)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

#End Region

    End Class

End Namespace
