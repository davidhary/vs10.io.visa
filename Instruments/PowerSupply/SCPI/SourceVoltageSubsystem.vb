Imports NationalInstruments
Namespace SCPI

    ''' <summary> Defines a SCPI Source Voltage Subsystem for power supplies. </summary>
    ''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/26/2012" by="David" revision="1.0.4652"> Created. </history>
    Public Class SourceVoltageSubsystem
        Inherits Visa.SCPI.SourceVoltageSubsystemBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary> Initializes a new instance of the <see cref="SourceVoltageSubsystem" /> class. </summary>
        ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">message based
        ''' session</see>. </param>
        Public Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
            MyBase.New(statusSubsystem)
        End Sub

#End Region

#Region " I PRESETTABLE "

        ''' <summary> Sets subsystem values to their known execution clear state. </summary>
        Public Overrides Sub ClearExecutionState()
        End Sub

        ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
        Public Overrides Sub ResetKnownState()
            MyBase.ResetKnownState()
            Me.Level = 0
            Me.ProtectionLevel = New Double?
        End Sub

#End Region

#Region " PUBLISHER "

        ''' <summary> Publishes all values by raising the property changed events. </summary>
        Public Overrides Sub Publish()
            If Me.Publishable Then
                For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                    Me.AsyncNotifyPropertyChanged(p.Name)
                Next
            End If
        End Sub

#End Region

#Region " COMMAND SYNTAX "

        ''' <summary> Gets the preset command. </summary>
        ''' <value> The preset command. </value>
        Protected Overrides ReadOnly Property PresetCommand As String
            Get
                Return ""
            End Get
        End Property

#Region " PROTECTION LEVEL "

        ''' <summary> Gets the protection level command format. </summary>
        ''' <value> the protection level command format. </value>
        Protected Overrides ReadOnly Property ProtectionLevelCommandFormat As String
            Get
                Return ":SOUR:VOLT:PROT:LEV {0}"
            End Get
        End Property

        ''' <summary> Gets the protection level query command. </summary>
        ''' <value> the protection level query command. </value>
        Protected Overrides ReadOnly Property ProtectionLevelQueryCommand As String
            Get
                Return ":SOUR:VOLT:PROT:LEV?"
            End Get
        End Property

#End Region

#Region " RANGE "

        ''' <summary> Gets the range command format. </summary>
        ''' <value> The range command format. </value>
        Protected Overrides ReadOnly Property RangeCommandFormat As String
            Get
                Return ":SOUR:VOLT:RANG {0}"
            End Get
        End Property

        ''' <summary> Gets the range query command. </summary>
        ''' <value> The range query command. </value>
        Protected Overrides ReadOnly Property RangeQueryCommand As String
            Get
                Return ":SOUR:VOLT:RANG?"
            End Get
        End Property

#End Region

#End Region

    End Class

End Namespace
