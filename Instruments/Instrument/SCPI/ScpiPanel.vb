Imports System.ComponentModel
Imports isr.Core.Controls.ControlExtensions

Namespace SCPI

    ''' <summary> Provides a user interface for a generic SCPI Device. </summary>
    ''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/2008" by="David" revision="2.0.2936.x"> Create based on the 24xx
    ''' system classes. </history>
    <System.ComponentModel.DisplayName("SCPI Panel"),
      System.ComponentModel.Description("SCPI Device Panel"),
      System.Drawing.ToolboxBitmap(GetType(SCPI.ScpiPanel))>
    Public Class ScpiPanel
        #Const designMode1 = True
#Region " BASE FROM WRAPPER "
        ' Designing requires changing the condition to True.
#If designMode Then
        Inherits ResourcePanelBaseWrapper
#Else
        Inherits ResourcePanelBase
#End If
#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary> Default constructor. </summary>
        Public Sub New()
            MyBase.New()

            ' This call is required by the Windows Form Designer.
            InitializeComponent()

            ' instantiate the reference to the Device
            Me.Device = New SCPI.Device()

            Me.onDisplayDeviceOpenChanged(Me.IsDeviceOpen)
            Me.initializeUserInterface()

        End Sub

        ''' <summary> Disposes managed resources. </summary>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub onDisposeManagedResources()
            If Me.Device IsNot Nothing Then
                Try
                    ' remove event handlers.
                    Me.DeviceClosing(Me, System.EventArgs.Empty)
                    Me._Device.Dispose()
                    Me._Device = Nothing
                Catch ex As Exception
                    Debug.Assert(Not Debugger.IsAttached, "Exception occurred disposing Prober panel resources", "Exception details: {0}", ex)
                End Try
            End If
        End Sub

        ''' <summary> Enables or disables controls based on the device open state. </summary>
        ''' <param name="deviceIsOpen"> true if device is open. </param>
        Private Sub onDisplayDeviceOpenChanged(ByVal deviceIsOpen As Boolean)
            Me._Tabs.Enabled = True
            For Each t As Windows.Forms.TabPage In Me._Tabs.TabPages
                If t IsNot Me._MessagesTabPage Then
                    For Each c As Windows.Forms.Control In t.Controls : c.RecursivelyEnable(deviceIsOpen) : Next
                End If
            Next
        End Sub

        ''' <summary> Initialize User interface. </summary>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")>
        Private Sub initializeUserInterface()
        End Sub

#End Region

#Region " DEVICE "

        Private _Device As SCPI.Device

        ''' <summary> Gets or sets a reference to the Device. </summary>
        ''' <value> The device. </value>
        <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
        Public Overloads Property Device() As SCPI.Device
            Get
                Return Me._Device
            End Get
            Friend Set(ByVal value As SCPI.Device)
                If value IsNot Nothing Then
                    Me._Device = value
                    MyBase.Device = value
                End If
            End Set
        End Property

        ''' <summary> Handle the device property changed event. </summary>
        ''' <param name="device">    The device. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnDevicePropertyChanged(ByVal device As Device, ByVal propertyName As String)
            If device Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "Enabled"
                        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                                   "Hardware {0}.", IIf(device.Enabled, "Enabled", "Disabled"))
                    Case "ResourcesSearchPattern"
                        MyBase.Connector.ResourcesSearchPattern = device.ResourcesSearchPattern
                    Case "ServiceRequestFailureMessage"
                        If Not String.IsNullOrWhiteSpace(device.ServiceRequestFailureMessage) Then
                            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, device.ServiceRequestFailureMessage)
                        End If
#If False Then
                    Case "IsSessionOpen"
                    Case "IsDeviceOpen"
#End If
                End Select
            End If
        End Sub

        ''' <summary> Device property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Protected Overrides Sub DevicePropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnDevicePropertyChanged(TryCast(sender, Device), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception handling property changed Event;. Failed property {0}. Details: {1}",
                                           e.PropertyName, ex)
            End Try
        End Sub

#End Region

#Region " DEVICE EVENT HANDLERS "

        ''' <summary> Device service requested. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Message based session event information. </param>
        Protected Overrides Sub DeviceServiceRequested(ByVal sender As Object, ByVal e As NationalInstruments.VisaNS.MessageBasedSessionEventArgs)
            MyBase.DeviceServiceRequested(sender, e)
        End Sub

        ''' <summary> Event handler. Called upon device opening. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        Protected Overrides Sub DeviceOpening(ByVal sender As Object, ByVal e As System.EventArgs)
            MyBase.DeviceOpening(sender, e)
        End Sub

        ''' <summary> Event handler. Called when device opened. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        Protected Overrides Sub DeviceOpened(ByVal sender As Object, ByVal e As System.EventArgs)
            Me.EnableServiceRequestEventHandler()
            AddHandler Me.Device.StatusSubsystem.PropertyChanged, AddressOf Me.StatusSubsystemPropertyChanged
            AddHandler Me.Device.SystemSubsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
            MyBase.DeviceOpened(sender, e)
            Me.onDisplayDeviceOpenChanged(Me.IsDeviceOpen)
        End Sub

        ''' <summary> Event handler. Called when device is closing. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        Protected Overrides Sub DeviceClosing(ByVal sender As Object, ByVal e As System.EventArgs)
            MyBase.DeviceClosing(sender, e)
            If Me.IsDeviceOpen Then
                RemoveHandler Me.Device.StatusSubsystem.PropertyChanged, AddressOf Me.StatusSubsystemPropertyChanged
                RemoveHandler Me.Device.SystemSubsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
            End If
        End Sub

        ''' <summary> Event handler. Called when device is closed. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        Protected Overrides Sub DeviceClosed(ByVal sender As Object, ByVal e As System.EventArgs)
            Me.onDisplayDeviceOpenChanged(Me.IsDeviceOpen)
            MyBase.DeviceClosed(sender, e)
        End Sub

#End Region

#Region " SUBSYSTEMS "

#Region " STATUS "

        ''' <summary> Handle the Status subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As StatusSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "Identity"
                        If Not String.IsNullOrWhiteSpace(subsystem.Identity) Then
                            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                                       "{0} identified as {1}.", Me.ResourceName, subsystem.Identity)

                        End If
                    Case "DeviceErrors"
                        If Not String.IsNullOrWhiteSpace(subsystem.DeviceErrors) Then
                            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                                       "{0} Device errors: {1}", Me.ResourceName, subsystem.DeviceErrors)
                        End If
#If False Then
                    Case "ErrorAvailable"
                    Case "ErrorAvailableBits"
                    Case "MeasurementAvailable"
                    Case "MeasurementAvailableBits"
                    Case "MeasurementEventCondition"
                    Case "MeasurementEventEnableBitmask"
                    Case "MeasurementEventStatus"
                    Case "MessageAvailable"
                    Case "MessageAvailableBits"
                    Case "OperationEventCondition"
                    Case "OperationEventEnableBitmask"
                    Case "OperationEventStatus"
                    Case "OperationNegativeTransitionEventEnableBitmask"
                    Case "OperationPositiveTransitionEventEnableBitmask"
                    Case "QuestionableEventCondition"
                    Case "QuestionableEventEnableBitmask"
                    Case "QuestionableEventStatus"
                    Case "ServiceRequestEnableBitmask"
                    Case "ServiceRequestStatus"
                    Case "StandardDeviceErrorAvailable"
                    Case "StandardDeviceErrorAvailableBits"
                    Case "StandardEventEnableBitmask"
                    Case "StandardEventStatus"
                    Case "VersionInfo"
#End If
                End Select
            End If
        End Sub

        ''' <summary> Status subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub StatusSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, StatusSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception handling property changed Event;. Failed property {0}. Details: {1}",
                                           e.PropertyName, ex)
            End Try
        End Sub
#End Region

#Region " SYSTEM "

        ''' <summary> Handle the System subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As SystemSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "LastError"
                        Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                                   "Last error: {0}", subsystem.LastError.CompoundErrorMessage)
#If False Then
                    Case "AutoZeroEnabled"
                    Case "BeeperEnabled"
                    Case "FourWireSenseEnabled"
                    Case "LineFrequency"
                    Case "ScpiRevision"
                    Case "StationLineFrequency"
#End If
                End Select
            End If
        End Sub

        ''' <summary> System subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub SystemSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, SystemSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception handling property changed Event;. Failed property {0}. Details: {1}",
                                           e.PropertyName, ex)
            End Try
        End Sub
#End Region

#End Region

#Region " TRACE MESSAGES HANDLERS "

        ''' <summary> Displays the <paramref name="value">message</paramref>. </summary>
        ''' <param name="value"> The <see cref="Core.Diagnosis.TraceMessage">message</see> to display and log. </param>
        Protected Overrides Sub DisplayMessage(value As Core.Diagnosis.TraceMessage)
            If value IsNot Nothing AndAlso MyBase.ShouldShow(value.EventType) Then
                Me._MessagesBox.AddMessage(value)
            End If
        End Sub

        ''' <summary> Gets the trace display level. </summary>
        ''' <value> The trace display level. </value>
        Public Property TraceDisplayLevel As TraceEventType = TraceEventType.Verbose

        ''' <summary> Gets or sets the trace show level. The trace message is displayed if the trace level is lower
        ''' than this value. </summary>
        ''' <value> The trace show level. </value>
        <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
        Protected Overrides ReadOnly Property TraceShowLevel As Diagnostics.TraceEventType
            Get
                Return Me.TraceDisplayLevel
            End Get
        End Property

#End Region

#Region " CONTROL EVENT HANDLERS: INTERFACE "

        ''' <summary> Event handler. Called by interfaceClearButton for click events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        Private Sub interfaceClearButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _InterfaceClearButton.Click
            ' Me.Device.GpibInterface.SendInterfaceClear()
        End Sub

        ''' <summary> Issue RST. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        Private Sub ResetButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ResetButton.Click
            Me.Device.ResetKnownState()
        End Sub

#End Region

    End Class

End Namespace
