Namespace My

    Public Module MyLibraryProperty
        ''' <summary>
        ''' Returns a singleton instance of the <see cref="Library">library manager</see>.
        ''' </summary>
        Public ReadOnly Property [Library]() As My.MyLibrary
            Get
                Return My.MyLibrary.[Get]
            End Get
        End Property
    End Module

    ''' <summary> Defines a singleton class to provide project management for this project.
    ''' This class has the Public Shared Not Creatable instancing property. </summary>
    ''' <license> (c) 2006 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="06/13/2006" by="David" revision="1.0.2355.x"> Created. </history>
    Public NotInheritable Class MyLibrary

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

#End Region

#Region " SINGLETON "

        ''' <summary> Creates a new default instance of this class.</summary>
        Public Overloads Shared Sub NewDefault()
            SyncLock MyLibrary.syncLocker
                MyLibrary.instance = New MyLibrary
            End SyncLock
        End Sub

        ''' <summary> The locking object to enforce thread safety when creating the singleton instance. </summary>
        Private Shared ReadOnly syncLocker As Object = New Object

        ''' <summary> The singleton instance </summary>
        Private Shared instance As MyLibrary

        ''' <summary> Instantiates the class. </summary>
        ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
        ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        ''' <returns> A new or existing instance of the class. </returns>
        Public Shared Function [Get]() As MyLibrary
            If Not MyLibrary.Instantiated Then
                MyLibrary.NewDefault()
            End If
            Return MyLibrary.instance
        End Function

        ''' <summary> Gets or sets True if the singleton instance was instantiated. </summary>
        ''' <value> The instantiated. </value>
        Public Shared ReadOnly Property Instantiated() As Boolean
            Get
                SyncLock MyLibrary.syncLocker
                    Return MyLibrary.instance IsNot Nothing
                End SyncLock
            End Get
        End Property

#End Region

#Region " TRACE ID "

        ''' <summary> Identifier for the trace event. </summary>
        Public Const TraceEventId As Integer = isr.Core.Diagnosis.TraceEventIds.IsrIOVisaMultimeterLibrary

#End Region

    End Class

End Namespace

