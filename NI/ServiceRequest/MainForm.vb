'================================================================================================== 
' 
' Title      : MainForm.vb 
' Purpose    : This application illustrates how to use the service request event and
'				the service request status byte to determine when generated data is ready 
'				and how to read it.
' 
'================================================================================================== 

Imports NationalInstruments.VisaNS

Public Class MainForm
    Inherits FormBase

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()
        toolTip.SetToolTip(enableSRQButton, "Enable the instrument's SRQ event on MAV by sending the following command (varies by instrument)")
        toolTip.SetToolTip(deviceConfigureButton, "The resource name of the device is set and the control attempts to connect to the device")
        toolTip.SetToolTip(writeButton, "Send string to device")
        toolTip.SetToolTip(resetButton, "Causes the control to release its handle to the device")
        writeTextBox.Enabled = False
        commandTextBox.Enabled = False

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Private WithEvents toolTip As System.Windows.Forms.ToolTip
    Private WithEvents selectResourceLabel As System.Windows.Forms.Label
    Private WithEvents configuringGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents writingGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents readingGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents resourceNameTextBox As System.Windows.Forms.TextBox
    Private WithEvents resourceNameLabel As System.Windows.Forms.Label
    Private WithEvents deviceConfigureButton As System.Windows.Forms.Button
    Private WithEvents resetButton As System.Windows.Forms.Button
    Private WithEvents commandTextBox As System.Windows.Forms.TextBox
    Private WithEvents commandLabel As System.Windows.Forms.Label
    Private WithEvents enableSRQButton As System.Windows.Forms.Button
    Private WithEvents writeTextBox As System.Windows.Forms.TextBox
    Private WithEvents writeButton As System.Windows.Forms.Button
    Private WithEvents readTextBox As System.Windows.Forms.TextBox
    Private WithEvents clearButton As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(MainForm))
        Me.selectResourceLabel = New System.Windows.Forms.Label
        Me.resourceNameTextBox = New System.Windows.Forms.TextBox
        Me.configuringGroupBox = New System.Windows.Forms.GroupBox
        Me.enableSRQButton = New System.Windows.Forms.Button
        Me.commandTextBox = New System.Windows.Forms.TextBox
        Me.commandLabel = New System.Windows.Forms.Label
        Me.resetButton = New System.Windows.Forms.Button
        Me.deviceConfigureButton = New System.Windows.Forms.Button
        Me.resourceNameLabel = New System.Windows.Forms.Label
        Me.writingGroupBox = New System.Windows.Forms.GroupBox
        Me.writeButton = New System.Windows.Forms.Button
        Me.writeTextBox = New System.Windows.Forms.TextBox
        Me.readingGroupBox = New System.Windows.Forms.GroupBox
        Me.clearButton = New System.Windows.Forms.Button
        Me.readTextBox = New System.Windows.Forms.TextBox
        Me.toolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.configuringGroupBox.SuspendLayout()
        Me.writingGroupBox.SuspendLayout()
        Me.readingGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'selectResourceLabel
        '
        Me.selectResourceLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                               System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.selectResourceLabel.Location = New System.Drawing.Point(8, 8)
        Me.selectResourceLabel.Name = "selectResourceLabel"
        Me.selectResourceLabel.Size = New System.Drawing.Size(272, 56)
        Me.selectResourceLabel.TabIndex = 0
        Me.selectResourceLabel.Text = "Select the Resource Name associated with your device and press the Configure Devi" & 
        "ce button. Then enter the command string that enables SRQ and click the Enable S" & 
        "RQ button."
        '
        'resourceNameTextBox
        '
        Me.resourceNameTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                               System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.resourceNameTextBox.Location = New System.Drawing.Point(8, 32)
        Me.resourceNameTextBox.Name = "resourceNameTextBox"
        Me.resourceNameTextBox.Size = New System.Drawing.Size(256, 20)
        Me.resourceNameTextBox.TabIndex = 1
        Me.resourceNameTextBox.Text = "GPIB::2::INSTR"
        '
        'configuringGroupBox
        '
        Me.configuringGroupBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                               System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.configuringGroupBox.Controls.Add(Me.enableSRQButton)
        Me.configuringGroupBox.Controls.Add(Me.commandTextBox)
        Me.configuringGroupBox.Controls.Add(Me.commandLabel)
        Me.configuringGroupBox.Controls.Add(Me.resetButton)
        Me.configuringGroupBox.Controls.Add(Me.deviceConfigureButton)
        Me.configuringGroupBox.Controls.Add(Me.resourceNameLabel)
        Me.configuringGroupBox.Controls.Add(Me.resourceNameTextBox)
        Me.configuringGroupBox.Location = New System.Drawing.Point(8, 64)
        Me.configuringGroupBox.Name = "configuringGroupBox"
        Me.configuringGroupBox.Size = New System.Drawing.Size(272, 168)
        Me.configuringGroupBox.TabIndex = 2
        Me.configuringGroupBox.TabStop = False
        Me.configuringGroupBox.Text = "Configuring"
        '
        'enableSRQButton
        '
        Me.enableSRQButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.enableSRQButton.Location = New System.Drawing.Point(160, 136)
        Me.enableSRQButton.Name = "enableSRQButton"
        Me.enableSRQButton.Size = New System.Drawing.Size(104, 24)
        Me.enableSRQButton.TabIndex = 7
        Me.enableSRQButton.Text = "Enable SRQ"
        '
        'commandTextBox
        '
        Me.commandTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                          System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.commandTextBox.Location = New System.Drawing.Point(8, 136)
        Me.commandTextBox.Name = "commandTextBox"
        Me.commandTextBox.Size = New System.Drawing.Size(152, 20)
        Me.commandTextBox.TabIndex = 6
        Me.commandTextBox.Text = "*SRE 16"
        '
        'commandLabel
        '
        Me.commandLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                        System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.commandLabel.Location = New System.Drawing.Point(8, 96)
        Me.commandLabel.Name = "commandLabel"
        Me.commandLabel.Size = New System.Drawing.Size(256, 32)
        Me.commandLabel.TabIndex = 5
        Me.commandLabel.Text = "Type the command to enable the instrument's SRQ event on MAV:"
        '
        'resetButton
        '
        Me.resetButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.resetButton.Location = New System.Drawing.Point(160, 64)
        Me.resetButton.Name = "resetButton"
        Me.resetButton.Size = New System.Drawing.Size(104, 24)
        Me.resetButton.TabIndex = 4
        Me.resetButton.Text = "Reset"
        '
        'deviceConfigureButton
        '
        Me.deviceConfigureButton.Location = New System.Drawing.Point(8, 64)
        Me.deviceConfigureButton.Name = "deviceConfigureButton"
        Me.deviceConfigureButton.Size = New System.Drawing.Size(104, 24)
        Me.deviceConfigureButton.TabIndex = 3
        Me.deviceConfigureButton.Text = "Configure Device"
        '
        'resourceNameLabel
        '
        Me.resourceNameLabel.Location = New System.Drawing.Point(8, 16)
        Me.resourceNameLabel.Name = "resourceNameLabel"
        Me.resourceNameLabel.Size = New System.Drawing.Size(112, 16)
        Me.resourceNameLabel.TabIndex = 2
        Me.resourceNameLabel.Text = "Resource Name:"
        '
        'writingGroupBox
        '
        Me.writingGroupBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                           System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.writingGroupBox.Controls.Add(Me.writeButton)
        Me.writingGroupBox.Controls.Add(Me.writeTextBox)
        Me.writingGroupBox.Location = New System.Drawing.Point(8, 240)
        Me.writingGroupBox.Name = "writingGroupBox"
        Me.writingGroupBox.Size = New System.Drawing.Size(272, 56)
        Me.writingGroupBox.TabIndex = 3
        Me.writingGroupBox.TabStop = False
        Me.writingGroupBox.Text = "Writing"
        '
        'writeButton
        '
        Me.writeButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.writeButton.Location = New System.Drawing.Point(160, 24)
        Me.writeButton.Name = "writeButton"
        Me.writeButton.Size = New System.Drawing.Size(104, 23)
        Me.writeButton.TabIndex = 1
        Me.writeButton.Text = "Write"
        '
        'writeTextBox
        '
        Me.writeTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                        System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.writeTextBox.Location = New System.Drawing.Point(8, 24)
        Me.writeTextBox.Name = "writeTextBox"
        Me.writeTextBox.Size = New System.Drawing.Size(152, 20)
        Me.writeTextBox.TabIndex = 0
        Me.writeTextBox.Text = "*IDN?\n"
        '
        'readingGroupBox
        '
        Me.readingGroupBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or
                                            System.Windows.Forms.AnchorStyles.Left) Or
                                        System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.readingGroupBox.Controls.Add(Me.clearButton)
        Me.readingGroupBox.Controls.Add(Me.readTextBox)
        Me.readingGroupBox.Location = New System.Drawing.Point(8, 312)
        Me.readingGroupBox.Name = "readingGroupBox"
        Me.readingGroupBox.Size = New System.Drawing.Size(272, 120)
        Me.readingGroupBox.TabIndex = 4
        Me.readingGroupBox.TabStop = False
        Me.readingGroupBox.Text = "Reading"
        '
        'clearButton
        '
        Me.clearButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.clearButton.Location = New System.Drawing.Point(8, 88)
        Me.clearButton.Name = "clearButton"
        Me.clearButton.Size = New System.Drawing.Size(104, 24)
        Me.clearButton.TabIndex = 1
        Me.clearButton.Text = "Clear"
        '
        'readTextBox
        '
        Me.readTextBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or
                                        System.Windows.Forms.AnchorStyles.Left) Or
                                    System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.readTextBox.Location = New System.Drawing.Point(8, 24)
        Me.readTextBox.Multiline = True
        Me.readTextBox.Name = "readTextBox"
        Me.readTextBox.ReadOnly = True
        Me.readTextBox.Size = New System.Drawing.Size(256, 56)
        Me.readTextBox.TabIndex = 0
        Me.readTextBox.Text = ""
        '
        'MainForm
        '
        Me.ClientSize = New System.Drawing.Size(288, 448)
        Me.Controls.Add(Me.readingGroupBox)
        Me.Controls.Add(Me.writingGroupBox)
        Me.Controls.Add(Me.selectResourceLabel)
        Me.Controls.Add(Me.configuringGroupBox)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimumSize = New System.Drawing.Size(296, 482)
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Service Request"
        Me.configuringGroupBox.ResumeLayout(False)
        Me.writingGroupBox.ResumeLayout(False)
        Me.readingGroupBox.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private gpibSession As GpibSession
    Private lastResourceString As String

    <STAThread()> 
    Public Shared Sub Main()
        Application.Run(New MainForm())
    End Sub

    ' When the Configure button is pressed, the resource name of the
    ' device is set and the control attempts to connect to the device
    Private Sub deviceConfigureButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles deviceConfigureButton.Click
        If resourceNameTextBox.Enabled = True Then
            If lastResourceString <> Nothing Then
                resourceNameTextBox.Text = lastResourceString
            End If
            lastResourceString = resourceNameTextBox.Text

            Try
                gpibSession = CType(ResourceManager.GetLocalManager().Open(resourceNameTextBox.Text), GpibSession)

#If NETFX2_0 Then
                'For .NET Framework 2.0, use SynchronizeCallbacks to specify that the object 
                'marshals callbacks across threads appropriately.
                gpibSession.SynchronizeCallbacks = True
#Else
                'For .NET Framework 1.1, set SynchronizingObject to the Windows Form to specify 
                'that the object marshals callbacks across threads appropriately.
                gpibSession.SynchronizingObject = Me
#End If

                resourceNameTextBox.Enabled = False
                commandTextBox.Enabled = True
                writeTextBox.Enabled = False
            Catch ex As InvalidCastException
                MessageBox.Show("Resource selected must be a GPIB session")
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Else
            MessageBox.Show("You have to reset the currently open resource first.")
        End If
    End Sub

    ' The Enable SRQ button writes the string that tells the instrument to
    ' enable the SRQ bit
    Private Sub enableSRQButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles enableSRQButton.Click
        If commandTextBox.Enabled = True Then
            Try
                ' you have to register the handler before you enable event  
                AddHandler gpibSession.ServiceRequest, AddressOf OnServiceRequest
                gpibSession.EnableEvent(MessageBasedSessionEventType.ServiceRequest, EventMechanism.Handler)
                WriteToSession(commandTextBox.Text)
                commandTextBox.Enabled = False
                writeTextBox.Enabled = True
            Catch exp As Exception
                MessageBox.Show(exp.Message)
            End Try
        Else
            If resourceNameTextBox.Enabled = True Then
                MessageBox.Show("You have to first open a resource.")
            Else
                MessageBox.Show("Enter a command to send to the resource.")
            End If
        End If
    End Sub

    Private Sub resetButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles resetButton.Click
        If resourceNameTextBox.Enabled = False Then
            gpibSession.Dispose()
            resourceNameTextBox.Enabled = True
            writeTextBox.Enabled = False
            commandTextBox.Enabled = False
        Else
            MessageBox.Show("There is no open resource currently.")
        End If
    End Sub

    Private Sub writeButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles writeButton.Click
        If writeTextBox.Enabled = True Then
            WriteToSession(writeTextBox.Text)
        Else
            If resourceNameTextBox.Enabled = True Then
                MessageBox.Show("You have to first open a resource.")
            Else
                MessageBox.Show("You have to first send a command to enable the SRQ event on MAV.")
            End If
        End If
    End Sub

    Private Sub clearButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clearButton.Click
        readTextBox.Clear()
    End Sub

    Private Function ReplaceCommonEscapeSequences(ByVal s As String) As String
        If (s <> Nothing) Then
            Return s.Replace("\n", vbLf).Replace("\r", vbCr)
        Else
            Return Nothing
        End If
    End Function

    Private Function InsertCommonEscapeSequences(ByVal s As String) As String
        If (s <> Nothing) Then
            Return s.Replace(vbLf, "\n").Replace(vbCr, "\r")
        Else
            Return Nothing
        End If
    End Function

    Private Sub WriteToSession(ByVal txtWrite As String)
        Try
            Dim textToWrite As String = ReplaceCommonEscapeSequences(txtWrite)
            gpibSession.Write(textToWrite)
        Catch exp As Exception
            MessageBox.Show(exp.Message)
        End Try
    End Sub

    Private Sub OnServiceRequest(ByVal sender As Object, ByVal e As MessageBasedSessionEventArgs)
        Dim gs As GpibSession = CType(sender, GpibSession)
        If Not (gs Is Nothing) Then
            Try
                Dim sb As StatusByteFlags = gs.ReadStatusByte()
                If (sb & StatusByteFlags.MessageAvailable) <> 0 Then
                    Dim textRead As String = gs.ReadString()
                    readTextBox.Text = InsertCommonEscapeSequences(textRead)
                Else
                    MessageBox.Show("MAV in status register is not set, which means that message is not available. Make sure the command to enable SRQ is correct, and the instrument is 488.2 compatible.")
                End If
            Catch exp As Exception
                MessageBox.Show(exp.Message)
            End Try
        Else
            MessageBox.Show("Sender is not GPIB session")
        End If
    End Sub

End Class
