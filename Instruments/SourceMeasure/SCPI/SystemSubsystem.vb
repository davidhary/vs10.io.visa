Imports NationalInstruments
Imports isr.Core.Primitives
Namespace Scpi
    ''' <summary>
    ''' Defines a SCPI System Subsystem for a generic Source Measure instrument such as the Keithley 2400.
    ''' </summary>
    ''' <license>
    ''' (c) 2012 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    Public Class SystemSubsystem
        Inherits SystemSubsystemBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
        ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">message based
        ''' session</see>. </param>
        Public Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
            MyBase.New(statusSubsystem)
        End Sub

#End Region

#Region " I PRESETTABLE "

        ''' <summary> Sets subsystem values to their known execution clear state. </summary>
        Public Overrides Sub ClearExecutionState()
        End Sub

#End Region

#Region " PUBLISHER "

        ''' <summary> Publishes all values by raising the property changed events. </summary>
        Public Overrides Sub Publish()
            If Me.Publishable Then
                For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                    Me.AsyncNotifyPropertyChanged(p.Name)
                Next
            End If
        End Sub

#End Region

#Region " COMMAND SYNTAX "

        ''' <summary> Gets the clear error queue command. </summary>
        ''' <value> The clear error queue command. </value>
        Protected Overrides ReadOnly Property ClearErrorQueueCommand As String
            Get
                Return Visa.Scpi.Syntax.ClearSystemErrorQueueCommand
            End Get
        End Property

        ''' <summary> Gets the initialize memory command. </summary>
        ''' <value> The initialize memory command. </value>
        Protected Overrides ReadOnly Property InitializeMemoryCommand As String
            Get
                Return Visa.Scpi.Syntax.InitializeMemoryCommand
            End Get
        End Property

        ''' <summary> Gets the last error query command. </summary>
        ''' <value> The last error query command. </value>
        Protected Overrides ReadOnly Property LastErrorQueryCommand As String
            Get
                Return Visa.Scpi.Syntax.LastErrorQueryCommand
            End Get
        End Property

        ''' <summary> Gets line frequency query command. </summary>
        ''' <value> The line frequency query command. </value>
        Protected Overrides ReadOnly Property LineFrequencyQueryCommand As String
            Get
                Return Visa.Scpi.Syntax.ReadLineFrequencyCommand
            End Get
        End Property

        ''' <summary> Gets the preset command. </summary>
        ''' <value> The preset command. </value>
        Protected Overrides ReadOnly Property PresetCommand As String
            Get
                Return Visa.Scpi.Syntax.SystemPresetCommand
            End Get
        End Property

        ''' <summary> Gets or sets the scpi revision query command. </summary>
        ''' <value> The scpi revision query command. </value>
        Protected Overrides ReadOnly Property ScpiRevisionQueryCommand As String
            Get
                Return ":SYST:VERS?"
            End Get
        End Property

#End Region

#Region " AUTO ZERO ENABLED "

        ''' <summary> Queries the auto zero enabled state. </summary>
        ''' <returns> <c>True</c> if auto zero is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Overrides Function QueryAutoZeroEnabled() As Boolean?
            If Me.IsSessionOpen Then
                Me.AutoZeroEnabled = Me.Session.Query(True, ":SYST:AZER?")
            End If
            Return Me.AutoZeroEnabled
        End Function

        ''' <summary> Writes the auto zero enabled state without reading back the actual value. </summary>
        ''' <param name="value"> if set to <c>True</c> [value]. </param>
        ''' <returns> <c>True</c> if auto zero is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Overrides Function WriteAutoZeroEnabled(ByVal value As Boolean) As Boolean?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":SYST:AZER {0:'ON';'ON';'OFF'}", CType(value, Integer))
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.AutoZeroEnabled = New Boolean?
            Else
                Me.AutoZeroEnabled = value
            End If
            Return Me.AutoZeroEnabled
        End Function

#End Region

#Region " FOUR WIRE SENSE ENABLED "

        ''' <summary> Queries the Four Wire Sense enabled state. </summary>
        ''' <returns> <c>True</c> if Four Wire Sense is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Overrides Function QueryWireSenseEnabled() As Boolean?
            If Me.IsSessionOpen Then
                Me.FourWireSenseEnabled = Me.Session.Query(True, ":SYST:RSEN?")
            End If
            Return Me.FourWireSenseEnabled
        End Function

        ''' <summary> Writes the Four Wire Sense enabled state without reading back the actual value. </summary>
        ''' <param name="value"> if set to <c>True</c> [value]. </param>
        ''' <returns> <c>True</c> if Four Wire Sense is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Overrides Function WriteFourWireSenseEnabled(ByVal value As Boolean) As Boolean?
            Me.FourWireSenseEnabled = New Boolean?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":SYST:RSEN {0:'ON';'ON';'OFF'}", CType(value, Integer))
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.FourWireSenseEnabled = New Boolean?
            Else
                Me.FourWireSenseEnabled = value
            End If
            Return Me.FourWireSenseEnabled
        End Function

#End Region

    End Class

End Namespace
