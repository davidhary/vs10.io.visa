﻿Imports System
Imports System.Globalization
Imports System.Runtime.CompilerServices
Imports System.ComponentModel
Imports System.Windows.Forms

''' <summary> Extensions for GPIB Interface. </summary>
''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/21/2005" by="David" revision="1.0.1847.x"> Created. </history>
Public Module GpibInterfaceExtensions

#Region " GPIB INTERFACE "

    ''' <summary> Values that represent IEEE 488.2 Command Code. </summary>
    Private Enum Ieee4882CommandCode
        None = 0
        <Description("GTL")> GoToLocal = &H1
        <Description("SDC")> SelectiveDeviceClear = &H4
        <Description("GET")> GroupExecuteTrigger = &H8
        <Description("LLO")> LocalLockout = &H11
        <Description("DCL")> DeviceClear = &H14
        <Description("SPE")> SerialPollEnable = &H18
        <Description("SPD")> SerialPollDisable = &H19
        <Description("LAG")> ListenAddressGroup = &H20
        <Description("TAG")> TalkAddressGroup = &H40
        <Description("SCG")> SecondaryCommandGroup = &H60
        <Description("UNL")> Unlisten = &H3F
        <Description("UNT")> Untalk = &H5F
    End Enum

    ''' <summary> Returns all instruments to some default state. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> The value. </param>
    <Extension()>
    Public Sub ClearDevices(ByVal value As NationalInstruments.VisaNS.GpibInterface)

        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        ' Transmit the DCL command to the interface.
        Dim commands As Byte() = New Byte() {Convert.ToByte(Ieee4882CommandCode.Untalk),
                                             Convert.ToByte(Ieee4882CommandCode.Unlisten),
                                             Convert.ToByte(Ieee4882CommandCode.DeviceClear),
                                             Convert.ToByte(Ieee4882CommandCode.Untalk),
                                             Convert.ToByte(Ieee4882CommandCode.Unlisten)}
        value.SendCommand(commands)

    End Sub

    ''' <summary> Clears the specified device. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">       The value. </param>
    ''' <param name="gpibAddress"> The instrument address. </param>
    <Extension()>
    Public Sub SelectiveDeviceClear(ByVal value As NationalInstruments.VisaNS.GpibInterface, ByVal gpibAddress As Integer)
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If

        ' Transmit the SDC command to the interface.
        Dim commands As Byte() = New Byte() {Convert.ToByte(Ieee4882CommandCode.Untalk),
                                     Convert.ToByte(Ieee4882CommandCode.Unlisten),
                                     Convert.ToByte(Ieee4882CommandCode.ListenAddressGroup) Or Convert.ToByte(gpibAddress),
                                     Convert.ToByte(Ieee4882CommandCode.SelectiveDeviceClear),
                                     Convert.ToByte(Ieee4882CommandCode.Untalk),
                                     Convert.ToByte(Ieee4882CommandCode.Unlisten)}
        value.SendCommand(commands)

    End Sub

    ''' <summary> Clears the specified device. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">        The value. </param>
    ''' <param name="resourceName"> Name of the resource. </param>
    <Extension()>
    Public Sub SelectiveDeviceClear(ByVal value As NationalInstruments.VisaNS.GpibInterface, ByVal resourceName As String)
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        If String.IsNullOrWhiteSpace(resourceName) Then
            Throw New ArgumentNullException("resourceName")
        End If
        Dim gpibAddress As Integer
        Dim resourceType As NationalInstruments.VisaNS.HardwareInterfaceType
        Dim interfaceNumber As Short
        If NationalInstruments.VisaNS.ResourceManager.GetLocalManager.TryParseResource(resourceName, resourceType, interfaceNumber, gpibAddress) Then
            value.SelectiveDeviceClear(gpibAddress)
        End If
    End Sub

#End Region

End Module
