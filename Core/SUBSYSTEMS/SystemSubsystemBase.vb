Imports NationalInstruments
''' <summary> Defines the contract that must be implemented by System Subsystem. </summary>
''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="9/26/2012" by="David" revision="1.0.4652"> Created. </history>
Public MustInherit Class SystemSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Initializes a new instance of the <see cref="SystemSubsystemBase" /> class. </summary>
    ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">status subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me.LastError = New DeviceError("")
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
    Public Overrides Sub ResetKnownState()
        Me.LastError = New DeviceError("")
        Me.QueryLineFrequency()
    End Sub

#End Region

#Region " ERROR MANAGEMENT  "

    ''' <summary> Gets the clear error queue command. </summary>
    ''' <value> The clear error queue command. </value>
    Protected MustOverride ReadOnly Property ClearErrorQueueCommand As String

    ''' <summary> Clears messages from the error queue. </summary>
    ''' <remarks> Sends the <see cref="ClearErrorQueueCommand">clear error queue</see> message. </remarks>
    Public Sub ClearErrorQueue()
        MyBase.Execute(ClearErrorQueueCommand)
    End Sub

#End Region

#Region " INTERFACE "

    ''' <summary> Supports clear interface. </summary>
    ''' <returns> <c>True</c> if supports clearing the interface. </returns>
    Public Function SupportsClearInterface() As Boolean
        Return Me.Session.HardwareInterfaceType = NationalInstruments.VisaNS.HardwareInterfaceType.Gpib
    End Function

    ''' <summary> Clears the interface. </summary>
    Public Sub ClearInterface()
        If Me.IsSessionOpen AndAlso Me.SupportsClearInterface Then
            Using gpibInterface As New NationalInstruments.VisaNS.GpibInterface(Me.Session.InterfaceResourceName)
                gpibInterface.SendInterfaceClear()
            End Using
        End If
    End Sub

    ''' <summary> Clears the device (SDC). </summary>
    Public Sub ClearDevice()
        If Me.IsSessionOpen Then
            Me.Session.Clear()
            If Me.Session.HardwareInterfaceType = NationalInstruments.VisaNS.HardwareInterfaceType.Gpib Then
                Using gpibInterface As New NationalInstruments.VisaNS.GpibInterface(Me.Session.InterfaceResourceName)
                    gpibInterface.SelectiveDeviceClear(Me.ResourceName)
                End Using
            End If
        End If
    End Sub

#End Region

#Region " LAST ERROR "

    Private _LastErrorReading As String
    ''' <summary> Gets or sets the last error reading. </summary>
    ''' <value> The last error reading. </value>
    Public Property LastErrorReading As String
        Get
            Return Me._LastErrorReading
        End Get
        Set(ByVal value As String)
            Me._LastErrorReading = value
            MyBase.AsyncNotifyPropertySetChanged(Reflection.MethodInfo.GetCurrentMethod.Name)
        End Set
    End Property

    ''' <summary> Gets the last error query command. </summary>
    ''' <value> The last error query command. </value>
    Protected MustOverride ReadOnly Property LastErrorQueryCommand As String

    ''' <summary> The last error. </summary>
    Private _LastError As DeviceError

    ''' <summary> Gets or sets the last error. </summary>
    ''' <value> The last error. </value>
    Public Property LastError As DeviceError
        Get
            Return Me._LastError
        End Get
        Set(ByVal value As DeviceError)
            Me._LastError = value
            MyBase.AsyncNotifyPropertySetChanged(System.Reflection.MethodInfo.GetCurrentMethod.Name)
        End Set
    End Property

    ''' <summary> Parse last error. </summary>
    ''' <param name="value"> The value. </param>
    Public Overridable Sub ParseLastError(ByVal value As String)
        Me.LastError = New DeviceError(value)
    End Sub

    ''' <summary> Queries the last error from the device. </summary>
    ''' <returns> The <see cref="DeviceError">Device Error structure.</see> </returns>
    Public Function QueryLastError() As DeviceError
        Me.LastError = New DeviceError()
        Me.WriteLastErrorQuery()
        Return Me.ReadLastError
    End Function

    ''' <summary> Reads the last error from the device. </summary>
    ''' <returns> The <see cref="DeviceError">Device Error structure.</see> </returns>
    Public Function ReadLastError() As DeviceError
        Me.LastError = New DeviceError()
        If Me.IsSessionOpen Then
            Me.LastErrorReading = Me.Session.ReadLineTrimEnd()
        End If
        Me.ParseLastError(Me.LastErrorReading)
        Return Me.LastError
    End Function

    ''' <summary> Writes the last error query. </summary>
    Public Sub WriteLastErrorQuery()
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me.LastErrorQueryCommand) Then
            Me.Session.WriteLine(Me.LastErrorQueryCommand)
        End If
    End Sub

#End Region

#Region " LINE FREQUENCY "

    ''' <summary> Gets or sets the station line frequency. </summary>
    ''' <value> The station line frequency. </value>
    ''' <remarks>This value is shared and used for all systems on this station. </remarks>
    Public Shared Property StationLineFrequency As Double? = New Double?

    ''' <summary> The line frequency. </summary>
    Private _lineFrequency As Double?

    ''' <summary> Gets or sets the line frequency. </summary>
    ''' <value> The line frequency. </value>
    Public Property LineFrequency() As Double?
        Get
            Return Me._lineFrequency
        End Get
        Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.LineFrequency, value) Then
                SystemSubsystemBase.StationLineFrequency = value
                Me._lineFrequency = value
                Me.AsyncNotifyPropertySetChanged(System.Reflection.MethodInfo.GetCurrentMethod.Name)
            End If
        End Set
    End Property

    ''' <summary> Gets line frequency query command. </summary>
    ''' <value> The line frequency query command. </value>
    Protected MustOverride ReadOnly Property LineFrequencyQueryCommand As String

    ''' <summary> Reads the line frequency. </summary>
    ''' <returns> System.Nullable{System.Double}. </returns>
    ''' <remarks> Sends the <see cref="LineFrequencyQueryCommand"/> query. </remarks>
    Public Function QueryLineFrequency() As Double?
        If Me.IsSessionOpen Then
            If Not SystemSubsystemBase.StationLineFrequency.HasValue Then
                If String.IsNullOrWhiteSpace(Me.LineFrequencyQueryCommand) Then
                    Me.LineFrequency = 60
                Else
                    Me.LineFrequency = Me.Session.Query(0.0F, Me.LineFrequencyQueryCommand)
                End If
            End If
        End If
        Return Me.LineFrequency
    End Function

    ''' <summary> Returns the Integration period. </summary>
    ''' <param name="powerLineCycles"> The power line cycles. </param>
    ''' <returns> The integration period corresponding to the specified number of power line . </returns>
    Public Shared Function IntegrationPeriod(ByVal powerLineCycles As Double) As TimeSpan
        Return TimeSpan.FromSeconds(powerLineCycles / SystemSubsystemBase.StationLineFrequency.GetValueOrDefault(60))
    End Function

    ''' <summary> Returns the Power line cycles. </summary>
    ''' <param name="integrationPeriod"> The integration period. </param>
    ''' <returns> The number of power line cycles corresponding to the integration period. </returns>
    Public Shared Function PowerLineCycles(ByVal integrationPeriod As TimeSpan) As Double
        Return integrationPeriod.TotalSeconds * SystemSubsystemBase.StationLineFrequency.GetValueOrDefault(60)
    End Function

#End Region

#Region " MEMORY MANAGEMENT "

    ''' <summary> Gets or sets the initialize memory command. </summary>
    ''' <value> The initialize memory command. </value>
    Protected MustOverride ReadOnly Property InitializeMemoryCommand As String

    ''' <summary> Initializes battery backed RAM. This initializes trace, source list, user-defined
    ''' math, source-memory locations, standard save setups, and all call math expressions. </summary>
    ''' <remarks> Sends the <see cref="InitializeMemoryCommand"/> message. </remarks>
    Public Sub InitializeMemory()
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me.InitializeMemoryCommand) Then
            Me.Session.WriteLine(Me.InitializeMemoryCommand)
        End If
    End Sub

#End Region

End Class
