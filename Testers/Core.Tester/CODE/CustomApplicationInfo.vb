﻿Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Shared ReadOnly TraceEventId As Integer = isr.Core.Diagnosis.TraceEventIds.IsrIOVisaCoreTester

        Public Const AssemblyTitle As String = "Visa Core Tester"
        Public Const AssemblyDescription As String = "Visa Core Tester"
        Public Const AssemblyProduct As String = "Visa.Core.Tester.2014"

    End Class

End Namespace

