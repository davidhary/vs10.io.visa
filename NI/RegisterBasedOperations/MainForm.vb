'================================================================================================== 
' 
' Title      : MainForm.vb 
' Purpose    : This application illustrates how to use In/Out/MoveIn/MoveOut on register-based 
'               sessions.
'
'================================================================================================== 

Imports NationalInstruments.VisaNS

Public Class MainForm
    Inherits FormBase

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        PopulateComboBoxes()
        numOutputArray = New NumericUpDown(7) {output0NumericUpDown, output1NumericUpDown, output2NumericUpDown, output3NumericUpDown, output4NumericUpDown, output5NumericUpDown, output6NumericUpDown, output7NumericUpDown}
        numElementsNumericUpDown.Minimum = 1
        numElementsNumericUpDown.Maximum = 8

        SetupUI(False)
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
            If Not (rbSession Is Nothing) Then
                CType(rbSession, Session).Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Private rbSession As IRegisterBasedSession
    Private numOutputArray As System.Windows.Forms.NumericUpDown()

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Private WithEvents clearButton As System.Windows.Forms.Button
    Private WithEvents resourceNameLabel As System.Windows.Forms.Label
    Private WithEvents resultLabel As System.Windows.Forms.Label
    Private WithEvents endianGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents endianLabel As System.Windows.Forms.Label
    Private WithEvents spaceLabel As System.Windows.Forms.Label
    Private WithEvents widthLabel As System.Windows.Forms.Label
    Private WithEvents offsetLabel As System.Windows.Forms.Label
    Private WithEvents numElementsLabel As System.Windows.Forms.Label
    Private WithEvents outputLabel As System.Windows.Forms.Label
    Private WithEvents resourceNameTextBox As System.Windows.Forms.TextBox
    Private WithEvents openButton As System.Windows.Forms.Button
    Private WithEvents resetButton As System.Windows.Forms.Button
    Private WithEvents resultTextBox As System.Windows.Forms.TextBox
    Private WithEvents endianBigRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents endianLittleRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents moveInButton As System.Windows.Forms.Button
    Private WithEvents moveOutButton As System.Windows.Forms.Button
    Private WithEvents inButton As System.Windows.Forms.Button
    Private WithEvents outButton As System.Windows.Forms.Button
    Private WithEvents spaceComboBox As System.Windows.Forms.ComboBox
    Private WithEvents offsetNumericUpDown As System.Windows.Forms.NumericUpDown
    Private WithEvents widthComboBox As System.Windows.Forms.ComboBox
    Private WithEvents numElementsNumericUpDown As System.Windows.Forms.NumericUpDown
    Private WithEvents output0NumericUpDown As System.Windows.Forms.NumericUpDown
    Private WithEvents output1NumericUpDown As System.Windows.Forms.NumericUpDown
    Private WithEvents output2NumericUpDown As System.Windows.Forms.NumericUpDown
    Private WithEvents output3NumericUpDown As System.Windows.Forms.NumericUpDown
    Private WithEvents output5NumericUpDown As System.Windows.Forms.NumericUpDown
    Private WithEvents output4NumericUpDown As System.Windows.Forms.NumericUpDown
    Private WithEvents output7NumericUpDown As System.Windows.Forms.NumericUpDown
    Private WithEvents output6NumericUpDown As System.Windows.Forms.NumericUpDown
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(MainForm))
        Me.resourceNameLabel = New System.Windows.Forms.Label
        Me.resourceNameTextBox = New System.Windows.Forms.TextBox
        Me.openButton = New System.Windows.Forms.Button
        Me.resetButton = New System.Windows.Forms.Button
        Me.endianGroupBox = New System.Windows.Forms.GroupBox
        Me.endianBigRadioButton = New System.Windows.Forms.RadioButton
        Me.endianLabel = New System.Windows.Forms.Label
        Me.endianLittleRadioButton = New System.Windows.Forms.RadioButton
        Me.resultTextBox = New System.Windows.Forms.TextBox
        Me.resultLabel = New System.Windows.Forms.Label
        Me.clearButton = New System.Windows.Forms.Button
        Me.moveInButton = New System.Windows.Forms.Button
        Me.moveOutButton = New System.Windows.Forms.Button
        Me.inButton = New System.Windows.Forms.Button
        Me.outButton = New System.Windows.Forms.Button
        Me.spaceComboBox = New System.Windows.Forms.ComboBox
        Me.widthComboBox = New System.Windows.Forms.ComboBox
        Me.spaceLabel = New System.Windows.Forms.Label
        Me.widthLabel = New System.Windows.Forms.Label
        Me.offsetNumericUpDown = New System.Windows.Forms.NumericUpDown
        Me.numElementsNumericUpDown = New System.Windows.Forms.NumericUpDown
        Me.offsetLabel = New System.Windows.Forms.Label
        Me.numElementsLabel = New System.Windows.Forms.Label
        Me.output0NumericUpDown = New System.Windows.Forms.NumericUpDown
        Me.output7NumericUpDown = New System.Windows.Forms.NumericUpDown
        Me.output6NumericUpDown = New System.Windows.Forms.NumericUpDown
        Me.output2NumericUpDown = New System.Windows.Forms.NumericUpDown
        Me.output5NumericUpDown = New System.Windows.Forms.NumericUpDown
        Me.output4NumericUpDown = New System.Windows.Forms.NumericUpDown
        Me.output3NumericUpDown = New System.Windows.Forms.NumericUpDown
        Me.output1NumericUpDown = New System.Windows.Forms.NumericUpDown
        Me.outputLabel = New System.Windows.Forms.Label
        Me.endianGroupBox.SuspendLayout()
        CType(Me.offsetNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numElementsNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.output0NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.output7NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.output6NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.output2NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.output5NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.output4NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.output3NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.output1NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'resourceNameLabel
        '
        Me.resourceNameLabel.Location = New System.Drawing.Point(16, 8)
        Me.resourceNameLabel.Name = "resourceNameLabel"
        Me.resourceNameLabel.TabIndex = 0
        Me.resourceNameLabel.Text = "Resource Name:"
        '
        'resourceNameTextBox
        '
        Me.resourceNameTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                               System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.resourceNameTextBox.Location = New System.Drawing.Point(16, 24)
        Me.resourceNameTextBox.Name = "resourceNameTextBox"
        Me.resourceNameTextBox.Size = New System.Drawing.Size(136, 20)
        Me.resourceNameTextBox.TabIndex = 1
        Me.resourceNameTextBox.Text = "VXI0::0::INSTR"
        '
        'openButton
        '
        Me.openButton.Location = New System.Drawing.Point(16, 56)
        Me.openButton.Name = "openButton"
        Me.openButton.Size = New System.Drawing.Size(80, 24)
        Me.openButton.TabIndex = 2
        Me.openButton.Text = "Open"
        '
        'resetButton
        '
        Me.resetButton.Location = New System.Drawing.Point(16, 88)
        Me.resetButton.Name = "resetButton"
        Me.resetButton.Size = New System.Drawing.Size(80, 24)
        Me.resetButton.TabIndex = 3
        Me.resetButton.Text = "Reset"
        '
        'endianGroupBox
        '
        Me.endianGroupBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.endianGroupBox.Controls.Add(Me.endianBigRadioButton)
        Me.endianGroupBox.Controls.Add(Me.endianLabel)
        Me.endianGroupBox.Controls.Add(Me.endianLittleRadioButton)
        Me.endianGroupBox.Location = New System.Drawing.Point(160, 16)
        Me.endianGroupBox.Name = "endianGroupBox"
        Me.endianGroupBox.Size = New System.Drawing.Size(144, 128)
        Me.endianGroupBox.TabIndex = 4
        Me.endianGroupBox.TabStop = False
        Me.endianGroupBox.Text = "Endianness"
        '
        'endianBigRadioButton
        '
        Me.endianBigRadioButton.Checked = True
        Me.endianBigRadioButton.Location = New System.Drawing.Point(8, 72)
        Me.endianBigRadioButton.Name = "endianBigRadioButton"
        Me.endianBigRadioButton.Size = New System.Drawing.Size(88, 24)
        Me.endianBigRadioButton.TabIndex = 6
        Me.endianBigRadioButton.TabStop = True
        Me.endianBigRadioButton.Text = "Big"
        '
        'endianLabel
        '
        Me.endianLabel.Location = New System.Drawing.Point(8, 16)
        Me.endianLabel.Name = "endianLabel"
        Me.endianLabel.Size = New System.Drawing.Size(128, 56)
        Me.endianLabel.TabIndex = 5
        Me.endianLabel.Text = "Used only with VISA session to a VXI MEMACC or GPIB-VXI MEMACC resource type "
        '
        'endianLittleRadioButton
        '
        Me.endianLittleRadioButton.Location = New System.Drawing.Point(8, 96)
        Me.endianLittleRadioButton.Name = "endianLittleRadioButton"
        Me.endianLittleRadioButton.Size = New System.Drawing.Size(96, 24)
        Me.endianLittleRadioButton.TabIndex = 7
        Me.endianLittleRadioButton.Text = "Little"
        '
        'resultTextBox
        '
        Me.resultTextBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or
                                          System.Windows.Forms.AnchorStyles.Left) Or
                                      System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.resultTextBox.Location = New System.Drawing.Point(16, 160)
        Me.resultTextBox.Multiline = True
        Me.resultTextBox.Name = "resultTextBox"
        Me.resultTextBox.ReadOnly = True
        Me.resultTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.resultTextBox.Size = New System.Drawing.Size(288, 198)
        Me.resultTextBox.TabIndex = 5
        Me.resultTextBox.Text = ""
        '
        'resultLabel
        '
        Me.resultLabel.Location = New System.Drawing.Point(16, 144)
        Me.resultLabel.Name = "resultLabel"
        Me.resultLabel.Size = New System.Drawing.Size(64, 16)
        Me.resultLabel.TabIndex = 6
        Me.resultLabel.Text = "Result:"
        '
        'clearButton
        '
        Me.clearButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.clearButton.Location = New System.Drawing.Point(224, 366)
        Me.clearButton.Name = "clearButton"
        Me.clearButton.Size = New System.Drawing.Size(80, 24)
        Me.clearButton.TabIndex = 7
        Me.clearButton.Text = "Clear"
        '
        'moveInButton
        '
        Me.moveInButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.moveInButton.Location = New System.Drawing.Point(328, 24)
        Me.moveInButton.Name = "moveInButton"
        Me.moveInButton.Size = New System.Drawing.Size(80, 24)
        Me.moveInButton.TabIndex = 8
        Me.moveInButton.Text = "Move In"
        '
        'moveOutButton
        '
        Me.moveOutButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.moveOutButton.Location = New System.Drawing.Point(416, 24)
        Me.moveOutButton.Name = "moveOutButton"
        Me.moveOutButton.Size = New System.Drawing.Size(80, 24)
        Me.moveOutButton.TabIndex = 9
        Me.moveOutButton.Text = "Move Out"
        '
        'inButton
        '
        Me.inButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.inButton.Location = New System.Drawing.Point(328, 56)
        Me.inButton.Name = "inButton"
        Me.inButton.Size = New System.Drawing.Size(80, 24)
        Me.inButton.TabIndex = 10
        Me.inButton.Text = "In"
        '
        'outButton
        '
        Me.outButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.outButton.Location = New System.Drawing.Point(416, 56)
        Me.outButton.Name = "outButton"
        Me.outButton.Size = New System.Drawing.Size(80, 24)
        Me.outButton.TabIndex = 11
        Me.outButton.Text = "Out"
        '
        'spaceComboBox
        '
        Me.spaceComboBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spaceComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.spaceComboBox.Location = New System.Drawing.Point(328, 176)
        Me.spaceComboBox.Name = "spaceComboBox"
        Me.spaceComboBox.Size = New System.Drawing.Size(80, 21)
        Me.spaceComboBox.TabIndex = 12
        '
        'widthComboBox
        '
        Me.widthComboBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.widthComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.widthComboBox.Location = New System.Drawing.Point(328, 224)
        Me.widthComboBox.Name = "widthComboBox"
        Me.widthComboBox.Size = New System.Drawing.Size(80, 21)
        Me.widthComboBox.TabIndex = 13
        '
        'spaceLabel
        '
        Me.spaceLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spaceLabel.Location = New System.Drawing.Point(328, 160)
        Me.spaceLabel.Name = "spaceLabel"
        Me.spaceLabel.Size = New System.Drawing.Size(40, 16)
        Me.spaceLabel.TabIndex = 14
        Me.spaceLabel.Text = "Space:"
        '
        'widthLabel
        '
        Me.widthLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.widthLabel.Location = New System.Drawing.Point(328, 208)
        Me.widthLabel.Name = "widthLabel"
        Me.widthLabel.Size = New System.Drawing.Size(40, 16)
        Me.widthLabel.TabIndex = 15
        Me.widthLabel.Text = "Width:"
        '
        'offsetNumericUpDown
        '
        Me.offsetNumericUpDown.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.offsetNumericUpDown.Hexadecimal = True
        Me.offsetNumericUpDown.Location = New System.Drawing.Point(328, 272)
        Me.offsetNumericUpDown.Maximum = New Decimal(New Integer() {65535, 0, 0, 0})
        Me.offsetNumericUpDown.Name = "offsetNumericUpDown"
        Me.offsetNumericUpDown.Size = New System.Drawing.Size(80, 20)
        Me.offsetNumericUpDown.TabIndex = 16
        '
        'numElementsNumericUpDown
        '
        Me.numElementsNumericUpDown.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.numElementsNumericUpDown.Location = New System.Drawing.Point(328, 320)
        Me.numElementsNumericUpDown.Maximum = New Decimal(New Integer() {8, 0, 0, 0})
        Me.numElementsNumericUpDown.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.numElementsNumericUpDown.Name = "numElementsNumericUpDown"
        Me.numElementsNumericUpDown.Size = New System.Drawing.Size(80, 20)
        Me.numElementsNumericUpDown.TabIndex = 17
        Me.numElementsNumericUpDown.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'offsetLabel
        '
        Me.offsetLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.offsetLabel.Location = New System.Drawing.Point(328, 256)
        Me.offsetLabel.Name = "offsetLabel"
        Me.offsetLabel.Size = New System.Drawing.Size(72, 16)
        Me.offsetLabel.TabIndex = 18
        Me.offsetLabel.Text = "Offset:"
        '
        'numElementsLabel
        '
        Me.numElementsLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.numElementsLabel.Location = New System.Drawing.Point(328, 304)
        Me.numElementsLabel.Name = "numElementsLabel"
        Me.numElementsLabel.Size = New System.Drawing.Size(112, 16)
        Me.numElementsLabel.TabIndex = 19
        Me.numElementsLabel.Text = "Number of Elements:"
        '
        'output0NumericUpDown
        '
        Me.output0NumericUpDown.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.output0NumericUpDown.Hexadecimal = True
        Me.output0NumericUpDown.Location = New System.Drawing.Point(440, 176)
        Me.output0NumericUpDown.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.output0NumericUpDown.Name = "output0NumericUpDown"
        Me.output0NumericUpDown.Size = New System.Drawing.Size(80, 20)
        Me.output0NumericUpDown.TabIndex = 20
        '
        'output7NumericUpDown
        '
        Me.output7NumericUpDown.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.output7NumericUpDown.Hexadecimal = True
        Me.output7NumericUpDown.Location = New System.Drawing.Point(440, 344)
        Me.output7NumericUpDown.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.output7NumericUpDown.Name = "output7NumericUpDown"
        Me.output7NumericUpDown.Size = New System.Drawing.Size(80, 20)
        Me.output7NumericUpDown.TabIndex = 21
        '
        'output6NumericUpDown
        '
        Me.output6NumericUpDown.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.output6NumericUpDown.Hexadecimal = True
        Me.output6NumericUpDown.Location = New System.Drawing.Point(440, 320)
        Me.output6NumericUpDown.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.output6NumericUpDown.Name = "output6NumericUpDown"
        Me.output6NumericUpDown.Size = New System.Drawing.Size(80, 20)
        Me.output6NumericUpDown.TabIndex = 22
        '
        'output2NumericUpDown
        '
        Me.output2NumericUpDown.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.output2NumericUpDown.Hexadecimal = True
        Me.output2NumericUpDown.Location = New System.Drawing.Point(440, 224)
        Me.output2NumericUpDown.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.output2NumericUpDown.Name = "output2NumericUpDown"
        Me.output2NumericUpDown.Size = New System.Drawing.Size(80, 20)
        Me.output2NumericUpDown.TabIndex = 23
        '
        'output5NumericUpDown
        '
        Me.output5NumericUpDown.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.output5NumericUpDown.Hexadecimal = True
        Me.output5NumericUpDown.Location = New System.Drawing.Point(440, 296)
        Me.output5NumericUpDown.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.output5NumericUpDown.Name = "output5NumericUpDown"
        Me.output5NumericUpDown.Size = New System.Drawing.Size(80, 20)
        Me.output5NumericUpDown.TabIndex = 24
        '
        'output4NumericUpDown
        '
        Me.output4NumericUpDown.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.output4NumericUpDown.Hexadecimal = True
        Me.output4NumericUpDown.Location = New System.Drawing.Point(440, 272)
        Me.output4NumericUpDown.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.output4NumericUpDown.Name = "output4NumericUpDown"
        Me.output4NumericUpDown.Size = New System.Drawing.Size(80, 20)
        Me.output4NumericUpDown.TabIndex = 25
        '
        'output3NumericUpDown
        '
        Me.output3NumericUpDown.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.output3NumericUpDown.Hexadecimal = True
        Me.output3NumericUpDown.Location = New System.Drawing.Point(440, 248)
        Me.output3NumericUpDown.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.output3NumericUpDown.Name = "output3NumericUpDown"
        Me.output3NumericUpDown.Size = New System.Drawing.Size(80, 20)
        Me.output3NumericUpDown.TabIndex = 26
        '
        'output1NumericUpDown
        '
        Me.output1NumericUpDown.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.output1NumericUpDown.Hexadecimal = True
        Me.output1NumericUpDown.Location = New System.Drawing.Point(440, 200)
        Me.output1NumericUpDown.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.output1NumericUpDown.Name = "output1NumericUpDown"
        Me.output1NumericUpDown.Size = New System.Drawing.Size(80, 20)
        Me.output1NumericUpDown.TabIndex = 27
        '
        'outputLabel
        '
        Me.outputLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.outputLabel.Location = New System.Drawing.Point(440, 160)
        Me.outputLabel.Name = "outputLabel"
        Me.outputLabel.Size = New System.Drawing.Size(88, 16)
        Me.outputLabel.TabIndex = 28
        Me.outputLabel.Text = "Data for Output:"
        '
        'MainForm
        '
        Me.ClientSize = New System.Drawing.Size(536, 406)
        Me.Controls.Add(Me.outputLabel)
        Me.Controls.Add(Me.output1NumericUpDown)
        Me.Controls.Add(Me.output3NumericUpDown)
        Me.Controls.Add(Me.output4NumericUpDown)
        Me.Controls.Add(Me.output5NumericUpDown)
        Me.Controls.Add(Me.output2NumericUpDown)
        Me.Controls.Add(Me.output6NumericUpDown)
        Me.Controls.Add(Me.output7NumericUpDown)
        Me.Controls.Add(Me.output0NumericUpDown)
        Me.Controls.Add(Me.numElementsLabel)
        Me.Controls.Add(Me.offsetLabel)
        Me.Controls.Add(Me.numElementsNumericUpDown)
        Me.Controls.Add(Me.offsetNumericUpDown)
        Me.Controls.Add(Me.widthLabel)
        Me.Controls.Add(Me.spaceLabel)
        Me.Controls.Add(Me.widthComboBox)
        Me.Controls.Add(Me.spaceComboBox)
        Me.Controls.Add(Me.outButton)
        Me.Controls.Add(Me.inButton)
        Me.Controls.Add(Me.moveOutButton)
        Me.Controls.Add(Me.moveInButton)
        Me.Controls.Add(Me.clearButton)
        Me.Controls.Add(Me.resultLabel)
        Me.Controls.Add(Me.resultTextBox)
        Me.Controls.Add(Me.resourceNameTextBox)
        Me.Controls.Add(Me.endianGroupBox)
        Me.Controls.Add(Me.resetButton)
        Me.Controls.Add(Me.openButton)
        Me.Controls.Add(Me.resourceNameLabel)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimumSize = New System.Drawing.Size(544, 440)
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Register-Based Operations"
        Me.endianGroupBox.ResumeLayout(False)
        CType(Me.offsetNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numElementsNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.output0NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.output7NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.output6NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.output2NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.output5NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.output4NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.output3NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.output1NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub openButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles openButton.Click
        If Not (rbSession Is Nothing) Then
            CType(rbSession, Session).Dispose()
        End If

        Try
            rbSession = CType(ResourceManager.GetLocalManager().Open(resourceNameTextBox.Text), IRegisterBasedSession)
            CheckEndianness()
            SetupUI(True)
        Catch exp As InvalidCastException
            MessageBox.Show("Resource selected must be a register based session")
        Catch exp As Exception
            MessageBox.Show(exp.Message)
        End Try
    End Sub

    Private Sub resetButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles resetButton.Click
        Try
            If Not (rbSession Is Nothing) Then
                CType(rbSession, Session).Dispose()
                SetupUI(False)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub clearButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clearButton.Click
        resultTextBox.Clear()
    End Sub

    ' Performs an "InXX" operation
    Private Sub inButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles inButton.Click
        Try
            Dim space As AddressSpace = GetAddressSpace(CStr(spaceComboBox.SelectedItem))
            Dim offset As Integer = CInt(offsetNumericUpDown.Value)
            Dim width As DataWidth = GetDataWidth(CStr(widthComboBox.SelectedItem))

            Select Case width
                Case DataWidth.D08
                    ' create a new scope for declaring byte data
                    resultTextBox.AppendText(GetOperationText("In8"))
                    Dim data As Byte = rbSession.In8(space, offset)
                    resultTextBox.AppendText(GetDataText(data.ToString("x")))
                Case DataWidth.D16
                    ' create a new scope for declaring short data
                    resultTextBox.AppendText(GetOperationText("In16"))
                    Dim data As Short = rbSession.In16(space, offset)
                    resultTextBox.AppendText(GetDataText(data.ToString("x")))
                Case DataWidth.D32
                    ' create a new scope for declaring int data
                    resultTextBox.AppendText(GetOperationText("In32"))
                    Dim data As Integer = rbSession.In32(space, offset)
                    resultTextBox.AppendText(GetDataText(data.ToString("x")))
            End Select
            ScrollToBottomOfResultTextBox()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    ' Perform a "MoveInXX" operation.
    Private Sub moveInButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles moveInButton.Click
        Try
            Dim space As AddressSpace = GetAddressSpace(CStr(spaceComboBox.SelectedItem))
            Dim offset As Integer = CInt(offsetNumericUpDown.Value)
            Dim width As DataWidth = GetDataWidth(CStr(widthComboBox.SelectedItem))
            Dim length As Integer = CInt(numElementsNumericUpDown.Value)

            Select Case width
                Case DataWidth.D08
                    ' create a new scope for declaring byte[] data
                    Dim data() As Byte
                    resultTextBox.AppendText(GetOperationText("MoveIn8"))
                    data = rbSession.MoveIn8(space, offset, length)
                    ShowArray(data)
                Case DataWidth.D16
                    ' create a new scope for declaring short[] data
                    Dim data() As Short
                    resultTextBox.AppendText(GetOperationText("MoveIn16"))
                    data = rbSession.MoveIn16(space, offset, length)
                    ShowArray(data)
                Case DataWidth.D32
                    ' create a new scope for declaring int[] data
                    Dim data() As Integer
                    resultTextBox.AppendText(GetOperationText("MoveIn32"))
                    data = rbSession.MoveIn32(space, offset, length)
                    ShowArray(data)
            End Select
            ScrollToBottomOfResultTextBox()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    ' performs an "OutXX" operation
    Private Sub outButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles outButton.Click
        Try
            Dim space As AddressSpace = GetAddressSpace(CStr(spaceComboBox.SelectedItem))
            Dim offset As Integer = CInt(offsetNumericUpDown.Value)
            Dim width As DataWidth = GetDataWidth(CStr(widthComboBox.SelectedItem))

            Select Case width
                Case DataWidth.D08
                    ' create a new scope for declaring byte data
                    Dim data As Byte
                    data = CByte(output0NumericUpDown.Value)
                    resultTextBox.AppendText(GetOperationText("Out8"))
                    rbSession.Out8(space, offset, data)
                    resultTextBox.AppendText(GetDataText(data.ToString("x")))
                Case DataWidth.D16
                    ' create a new scope for declaring short data
                    Dim data As Short
                    data = CShort(output0NumericUpDown.Value)
                    resultTextBox.AppendText(GetOperationText("Out16"))
                    rbSession.Out16(space, offset, data)
                    resultTextBox.AppendText(GetDataText(data.ToString("x")))
                Case DataWidth.D32
                    ' create a new scope for declaring int data
                    Dim data As Integer
                    data = CInt(output0NumericUpDown.Value)
                    resultTextBox.AppendText(GetOperationText("Out32"))
                    rbSession.Out32(space, offset, data)
                    resultTextBox.AppendText(GetDataText(data.ToString("x")))
            End Select
            ScrollToBottomOfResultTextBox()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub moveOutButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles moveOutButton.Click
        Try
            Dim space As AddressSpace = GetAddressSpace(CStr(spaceComboBox.SelectedItem))
            Dim offset As Integer = CInt(offsetNumericUpDown.Value)
            Dim width As DataWidth = GetDataWidth(CStr(widthComboBox.SelectedItem))
            Dim length As Integer = CInt(numElementsNumericUpDown.Value)

            Select Case width
                Case DataWidth.D08
                    ' create a new scope for declaring byte[] data
                    Dim data() As Byte
                    resultTextBox.AppendText(GetOperationText("MoveOut8"))
                    data = BuildByteOutputdata()
                    ShowArray(data)
                    rbSession.MoveOut8(space, offset, length, data)
                Case DataWidth.D16
                    ' create a new scope for declaring short[] data
                    Dim data() As Short
                    resultTextBox.AppendText(GetOperationText("MoveOut16"))
                    data = BuildShortOutputdata()
                    ShowArray(data)
                    rbSession.MoveOut16(space, offset, length, data)
                Case DataWidth.D32
                    ' create a new scope for declaring int[] data
                    Dim data() As Integer
                    resultTextBox.AppendText(GetOperationText("MoveOut32"))
                    data = BuildIntOutputdata()
                    ShowArray(data)
                    rbSession.MoveOut32(space, offset, length, data)
            End Select
            ScrollToBottomOfResultTextBox()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    ' Enable or disable some of the NumericUpDown for numOutput0-7
    Private Sub numElementsNumericUpDown_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles numElementsNumericUpDown.ValueChanged
        Try
            If Not numOutputArray Is Nothing Then
                Dim i As Integer
                For i = 0 To numOutputArray.Length - 1
                    numOutputArray(i).Enabled = i < numElementsNumericUpDown.Value
                Next i
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub PopulateComboBoxes()
        Dim names As String() = [Enum].GetNames(GetType(AddressSpace))
        Dim name As String
        For Each name In names
            spaceComboBox.Items.Add(name)
        Next name
        spaceComboBox.SelectedIndex = 0

        names = [Enum].GetNames(GetType(DataWidth))
        For Each name In names
            widthComboBox.Items.Add(name)
        Next name
        widthComboBox.SelectedIndex = 0
    End Sub

    Private Sub SetupUI(ByVal sessionActive As Boolean)
        resourceNameTextBox.ReadOnly = sessionActive
        openButton.Enabled = Not sessionActive
        resetButton.Enabled = sessionActive
        moveInButton.Enabled = sessionActive
        moveOutButton.Enabled = sessionActive
        inButton.Enabled = sessionActive
        outButton.Enabled = sessionActive
        spaceLabel.Enabled = sessionActive
        spaceComboBox.Enabled = sessionActive
        widthLabel.Enabled = sessionActive
        widthComboBox.Enabled = sessionActive
        offsetLabel.Enabled = sessionActive
        offsetNumericUpDown.Enabled = sessionActive
        numElementsLabel.Enabled = sessionActive
        numElementsNumericUpDown.Enabled = sessionActive
        outputLabel.Enabled = sessionActive

        Dim endiannessApplicable As Boolean = sessionActive AndAlso Not (rbSession Is Nothing) AndAlso TypeOf rbSession Is VxiMemory
        endianGroupBox.Enabled = endiannessApplicable

        If sessionActive Then
            numElementsNumericUpDown_ValueChanged(Me, EventArgs.Empty)
        Else
            Dim i As Integer
            For i = 0 To numOutputArray.Length - 1
                numOutputArray(i).Enabled = False
            Next i
        End If
    End Sub

    Private Sub ScrollToBottomOfResultTextBox()
        resultTextBox.SelectAll()
    End Sub


    Private Function GetAddressSpace(ByVal val As String) As AddressSpace
        Return CType([Enum].Parse(GetType(AddressSpace), val, True), AddressSpace)
    End Function


    Private Function GetDataWidth(ByVal val As String) As DataWidth
        Return CType([Enum].Parse(GetType(DataWidth), val, True), DataWidth)
    End Function


    Private Function GetOperationText(ByVal operation As String) As String
        Return operation + Environment.NewLine
    End Function


    Private Function GetDataText(ByVal data As String) As String
        Return String.Format("Data = {0}", data) + Environment.NewLine
    End Function

    Private Sub ShowArray(ByVal data As Array)
        Dim i As Integer = 0
        Dim o As Object
        For Each o In data
            Dim formattedValue As String = String.Empty
            If TypeOf o Is Byte Then
                formattedValue = CByte(o).ToString("x")
            ElseIf TypeOf o Is Short Then
                formattedValue = CShort(o).ToString("x")
            ElseIf TypeOf o Is Integer Then
                formattedValue = CShort(o).ToString("x")
            End If
            resultTextBox.AppendText((String.Format("Data({0} = {1})", i, formattedValue) + Environment.NewLine))
            i += 1
        Next o
    End Sub

    Private Sub CheckEndianness()
        If TypeOf rbSession Is VxiMemory Then ' This is a session to a VXI MEMACC or GPIB-VXI MEMACC resource type 
            Dim vm As VxiMemory = CType(rbSession, VxiMemory)
            If endianBigRadioButton.Checked Then
                vm.DestinationByteOrder = ByteOrder.BigEndian
            Else
                vm.DestinationByteOrder = ByteOrder.LittleEndian
            End If
        End If
    End Sub

    Private Function BuildByteOutputdata() As Byte()
        Dim numElements As Integer = CType(numElementsNumericUpDown.Value, Integer)
        Dim od(numElements - 1) As Byte
        Dim i As Integer
        For i = 0 To numElements - 1
            od(i) = CType(numOutputArray(i).Value, Byte)
        Next i
        Return od
    End Function

    Private Function BuildShortOutputdata() As Short()
        Dim numElements As Integer = CType(numElementsNumericUpDown.Value, Integer)
        Dim od(numElements - 1) As Short
        Dim i As Integer
        For i = 0 To numElements - 1
            od(i) = CType(numOutputArray(i).Value, Short)
        Next i
        Return od
    End Function

    Private Function BuildIntOutputdata() As Integer()
        Dim numElements As Integer = CType(numElementsNumericUpDown.Value, Integer)
        Dim od(numElements - 1) As Integer
        Dim i As Integer
        For i = 0 To numElements - 1
            od(i) = CType(numOutputArray(i).Value, Integer)
        Next i
        Return od
    End Function

End Class
