Imports NationalInstruments
Namespace EG2000

    ''' <summary> Defines a System Subsystem for a EG2000 Prober. </summary>
    ''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="10/01/2013" by="David" revision="3.0.5022"> Created. </history>
    Public Class SystemSubsystem
        Inherits Visa.R2D2.SystemSubsystemBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
        ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">message based
        ''' session</see>. </param>
        Public Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
            MyBase.New(statusSubsystem)
        End Sub

#End Region

#Region " I PRESETTABLE "

        ''' <summary> Sets subsystem values to their known execution clear state. </summary>
        Public Overrides Sub ClearExecutionState()
        End Sub

#End Region

#Region " PUBLISHER "

        ''' <summary> Publishes all values by raising the property changed events. </summary>
        Public Overrides Sub Publish()
            If Me.Publishable Then
                For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                    Me.AsyncNotifyPropertyChanged(p.Name)
                Next
            End If
        End Sub

#End Region

#Region " COMMAND SYNTAX "

        ''' <summary> Gets the clear error queue command. </summary>
        ''' <value> The clear error queue command. </value>
        Protected Overrides ReadOnly Property ClearErrorQueueCommand As String
            Get
                Return ""
            End Get
        End Property

        ''' <summary> Gets the initialize memory command. </summary>
        ''' <value> The initialize memory command. </value>
        Protected Overrides ReadOnly Property InitializeMemoryCommand As String
            Get
                Return ""
            End Get
        End Property

        ''' <summary> Gets the last error query command. </summary>
        ''' <value> The last error query command. </value>
        Protected Overrides ReadOnly Property LastErrorQueryCommand As String
            Get
                Return "?E"
            End Get
        End Property

        ''' <summary> Gets line frequency query command. </summary>
        ''' <value> The line frequency query command. </value>
        Protected Overrides ReadOnly Property LineFrequencyQueryCommand As String
            Get
                Return ""
            End Get
        End Property

        ''' <summary> Gets the preset command. </summary>
        ''' <value> The preset command. </value>
        Protected Overrides ReadOnly Property PresetCommand As String
            Get
                Return Visa.Scpi.Syntax.SystemPresetCommand
            End Get
        End Property

#End Region

    End Class

End Namespace
