Imports NationalInstruments
Namespace Scpi

    ''' <summary> Defines the contract that must be implemented by SCPI System Subsystem. </summary>
    ''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/26/2012" by="David" revision="1.0.4652"> Created. </history>
    Public MustInherit Class SystemSubsystemBase
        Inherits Visa.SystemSubsystemBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary> Initializes a new instance of the <see cref="SystemSubsystemBase" /> class. </summary>
        ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">status subsystem</see>. </param>
        Protected Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
            MyBase.New(statusSubsystem)
        End Sub

#End Region

#Region " I PRESETTABLE "

        ''' <summary> Performs a reset and additional custom setting for the subsystem. </summary>
        ''' <remarks> Override this method to customize the reset.<para>
        '''           Additional Actions: </para><para>
        '''           Clears Error Queue.
        '''           </para></remarks>
        Public Overrides Sub InitializeKnownState()
            MyBase.InitializeKnownState()
            Me.ClearErrorQueue()
        End Sub

        ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
        Public Overrides Sub ResetKnownState()
            MyBase.ResetKnownState()
            Me.ScpiRevision = New Double?
        End Sub

#End Region

#Region " SCPI VERSION "

        ''' <summary> The SCPI revision. </summary>
        Private _scpiRevision As Double?

        ''' <summary> Gets the cached version level of the SCPI standard implemented by the device. </summary>
        ''' <value> The SCPI revision. </value>
        Public Property ScpiRevision() As Double?
            Get
                Return Me._scpiRevision
            End Get
            Protected Set(ByVal value As Double?)
                If Not Nullable.Equals(Me.ScpiRevision, value) Then
                    Me._scpiRevision = value
                    Me.AsyncNotifyPropertySetChanged(System.Reflection.MethodInfo.GetCurrentMethod.Name)
                End If
            End Set
        End Property

        ''' <summary> Gets or sets the scpi revision query command. </summary>
        ''' <value> The scpi revision query command. </value>
        Protected MustOverride ReadOnly Property ScpiRevisionQueryCommand As String

        ''' <summary> Queries the version level of the SCPI standard implemented by the device. </summary>
        ''' <returns> System.Nullable{System.Double}. </returns>
        ''' <remarks> Sends the ':SYST:VERS?' query. </remarks>
        Public Function QueryScpiRevision() As Double?
            If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me.ScpiRevisionQueryCommand) Then
                Me.ScpiRevision = Me.Session.Query(0.0F, Me.ScpiRevisionQueryCommand)
            End If
            Return Me.ScpiRevision
        End Function

#End Region

    End Class

End Namespace
