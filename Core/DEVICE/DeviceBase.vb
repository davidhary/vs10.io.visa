﻿Imports NationalInstruments
Imports System.Threading
Imports System.ComponentModel
Imports isr.Core.Diagnosis
Imports isr.Core.Diagnosis.DiagnosticsExtensions

''' <summary> Defines the contract that must be implemented by Devices. </summary>
''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="9/26/2012" by="David" revision="1.0.4652"> Created. </history>
Public MustInherit Class DeviceBase
    Inherits SubsystemBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Initializes a new instance of the <see cref="DeviceBase" /> class. </summary>
    Protected Sub New()
        MyBase.New()
        Me._Subsystems = New PresettablePublisherCollection
        Me._ResourcesSearchPattern = ResourceManagerExtensions.InstrumentSearchPattern
        Me._enabled = True
        Me._InitializeTimeout = TimeSpan.FromMilliseconds(30000)
        Me.UsingSyncServiceRequestHandler = True
    End Sub

#Region " I Disposable Support "

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged resources;
    ''' False if this method releases only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    Try
                        Me.SessionMessagesTraceEnabled = False
                    Catch ex As Exception
                        Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                    End Try
                    Try
                        Me.SessionPropertyChangeHandlerEnabled = False
                    Catch ex As Exception
                        Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                    End Try
                    Me.DisableServiceRequestEventHandler()
                    If Me.ServiceRequestedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.ServiceRequestedEvent.GetInvocationList
                            Try
                                RemoveHandler Me.ServiceRequested, CType(d, Global.System.EventHandler(Of NationalInstruments.VisaNS.MessageBasedSessionEventArgs))
                            Catch ex As Exception
                                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                            End Try
                        Next
                    End If
                    For Each d As [Delegate] In Me.OpeningEvent.GetInvocationList
                        Try
                            RemoveHandler Me.Opening, CType(d, Global.System.EventHandler(Of System.EventArgs))
                        Catch ex As Exception
                            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                        End Try
                    Next
                    For Each d As [Delegate] In Me.OpenedEvent.GetInvocationList
                        Try
                            RemoveHandler Me.Opened, CType(d, Global.System.EventHandler(Of System.EventArgs))
                        Catch ex As Exception
                            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                        End Try
                    Next
                    For Each d As [Delegate] In Me.ClosingEvent.GetInvocationList
                        Try
                            RemoveHandler Me.Closing, CType(d, Global.System.EventHandler(Of System.EventArgs))
                        Catch ex As Exception
                            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                        End Try
                    Next
                    For Each d As [Delegate] In Me.ClosedEvent.GetInvocationList
                        Try
                            RemoveHandler Me.Closed, CType(d, Global.System.EventHandler(Of System.EventArgs))
                        Catch ex As Exception
                            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                        End Try
                    Next
                    Try
                        Me.CloseSession()
                    Catch ex As Exception
                        Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                    End Try
                End If
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#End Region

#Region " I PRESETTABLE "

    Private _InitializeTimeout As TimeSpan
    ''' <summary> Gets or sets the time out for doing a reset and clear on the instrument. </summary>
    ''' <value> The connect timeout. </value>
    Public Property InitializeTimeout() As TimeSpan
        Get
            Return Me._InitializeTimeout
        End Get
        Set(ByVal value As TimeSpan)
            If Not value.Equals(Me.InitializeTimeout) Then
                Me._InitializeTimeout = value
                Me.AsyncNotifyPropertyChanged("InitializeTimeout")
            End If
        End Set
    End Property

    ''' <summary> Clears the Device. Issues <see cref="StatusSubsystemBase.ClearActiveState">Selective device clear</see>. </summary>
    Public MustOverride Sub ClearActiveState()

    ''' <summary> Clears the queues and resets all registers to zero. Sets the subsystem properties to
    ''' the following CLS default values:<para>
    ''' </para> </summary>
    ''' <remarks> *CLS. </remarks>
    Public Overrides Sub ClearExecutionState()
        Me.Subsystems.ClearExecutionState()
    End Sub

    ''' <summary> Initializes the Device. Performs a reset and additional custom setting by the parent
    ''' Devices. </summary>
    ''' <remarks> Use this to customize the reset. </remarks>
    Public Overrides Sub InitializeKnownState()
        MyBase.InitializeKnownState()
        Me.Subsystems.InitializeKnownState()
    End Sub

    ''' <summary> Resets the Device to its known state. </summary>
    ''' <remarks> *RST. </remarks>
    Public Overrides Sub PresetKnownState()
        Me.Subsystems.PresetKnownState()
    End Sub

    ''' <summary> Resets the Device to its known state. </summary>
    ''' <remarks> *RST. </remarks>
    Public Overrides Sub ResetKnownState()
        Me.Subsystems.ResetKnownState()
        ' allow the initialized properties to consume the property change events.
        For i As Integer = 1 To 1000 : Windows.Forms.Application.DoEvents() : Next
    End Sub

    ''' <summary> Resets and clears the subsystem. Starts with issuing a selective-device-clear, reset
    ''' (RST), Clear Status (CLS, and clear error queue). </summary>
    Public Overridable Sub ResetAndClear()

        ' issues selective device clear.
        Me.ClearActiveState()

        ' Initialize and reset the device
        If Not Me.IsSessionOpen OrElse Me.Session.LastStatus >= VisaNS.VisaStatusCode.Success Then
            Me.InitializeKnownState()
        End If

        ' Clear the device Status and set more defaults
        If Not Me.IsSessionOpen OrElse Me.Session.LastStatus >= VisaNS.VisaStatusCode.Success Then
            Me.ClearExecutionState()
        End If

    End Sub

    ''' <summary> Resets and clears the subsystem. Starts with issuing a selective-device-clear, reset
    ''' (RST), Clear Status (CLS, and clear error queue). </summary>
    ''' <param name="timeout"> The timeout to use. This allows using a longer timeout than the
    ''' minimal timeout set for the session. Typically, a source meter requires a 5000 milliseconds
    ''' timeout. </param>
    Public Overridable Sub ResetAndClear(ByVal timeout As TimeSpan)
        If Me.IsDeviceOpen Then
            Try
                If Me.IsSessionOpen Then Me.Session.StoreTimeout(timeout)
                Me.ResetAndClear()
            Catch
                Throw
            Finally
                If Me.IsSessionOpen Then Me.Session.RestoreTimeout()
            End Try
        End If
    End Sub

#End Region

#Region " PUBLISHER "

    ''' <summary> Resumes the property events. </summary>
    Public Overrides Sub ResumePublishing()
        MyBase.ResumePublishing()
        Me.Subsystems.ResumePublishing()
    End Sub

    ''' <summary> Suppresses the property events. </summary>
    Public Overrides Sub SuspendPublishing()
        MyBase.SuspendPublishing()
        Me.Subsystems.SuspendPublishing()
    End Sub

#End Region

#Region " COMMAND SYNTAX "

    ''' <summary> Gets the preset command. </summary>
    ''' <value> The preset command. </value>
    Protected Overrides ReadOnly Property PresetCommand As String
        Get
            Return ""
        End Get
    End Property

#End Region

#Region " RESOURCE NAME PATTERN "

    ''' <summary> Gets or sets the resources search pattern. </summary>
    ''' <value> The resources search pattern. </value>
    Public Property ResourcesSearchPattern As String

#End Region

#Region " SESSION "

    Private _enabled As Boolean

    ''' <summary> Gets or sets the Enabled sentinel of the device.
    '''           A device is enabled when hardware can be used. </summary>
    ''' <value> <c>True</c> if hardware device is enabled; <c>False</c> otherwise. </value>
    Public Property Enabled As Boolean
        Get
            Return Me._enabled
        End Get
        Set(ByVal value As Boolean)
            If Not Me.Enabled.Equals(value) Then
                Me._enabled = value
                MyBase.AsyncNotifyPropertyChanged("Enabled")
                Windows.Forms.Application.DoEvents()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets (Protected) a value indicating whether the device is open. This is
    '''           required when the device is used in emulation. </summary>
    ''' <value> <c>True</c> if the device has an open session; otherwise, <c>False</c>. </value>
    Public MustOverride Property IsDeviceOpen As Boolean

    ''' <summary> Allows the derived device to take actions before opening. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    ''' <remarks> This override should occur as the first call of the overriding method. </remarks>
    Protected Overridable Sub OnOpening(ByVal e As ComponentModel.CancelEventArgs)
        Me.SuspendPublishing()
        Me.SyncNotifyOpening(System.EventArgs.Empty)
    End Sub

    ''' <summary> Allows the derived device to take actions after opening. </summary>
    ''' <remarks> This override should occur as the last call of the overriding method. </remarks>
    Protected Overridable Sub OnOpened()
        Me.ResumePublishing()
        If Me.Enabled Then
            MyBase.AsyncNotifyPropertyChanged("IsSessionOpen")
        End If
        Me.IsDeviceOpen = True
        Me.SyncNotifyOpened(System.EventArgs.Empty)
        Me.Publish()
    End Sub

    ''' <summary> Creates the session. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    Public Overridable Sub CreateSession(ByVal resourceName As String)
        Me.Session = New Visa.Session(resourceName)
        If Me.IsSessionOpen Then
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                       "Opened session to resource {0} with access mode {1};. ",
                                       resourceName, Me.Session.ResourceLockState)
        Else
            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                       "Failed opening session to resource '{0}';. ", resourceName)
        End If
    End Sub

    ''' <summary> Opens the session. </summary>
    ''' <exception cref="OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="resourceName"> Name of the resource. </param>
    Public Overridable Sub OpenSession(ByVal resourceName As String)
        Try
            Dim e As New ComponentModel.CancelEventArgs
            Me.OnOpening(e)
            If e Is Nothing OrElse Not e.Cancel Then
                If Me.Subsystems IsNot Nothing Then Me.Subsystems.Clear()
                If Me.Enabled Then
                    Me.CreateSession(resourceName)
                    If Me.IsSessionOpen Then
                        Dim lastStatus As VisaNS.VisaStatusCode = Me.Session.LastStatus
                        If lastStatus >= VisaNS.VisaStatusCode.Success Then
                            Me.OnOpened()
                        Else
                            Me.TryCloseSession()
                            Throw New OperationFailedException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                             "Failed opening session to '{0}'; VISA status: '{1}'", resourceName,
                                                                             VisaException.BuildVisaStatusDetails(lastStatus)))
                        End If
                    Else
                        Me.TryCloseSession()
                        Throw New OperationFailedException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                         "Failed opening session to '{0}'.", resourceName))
                    End If
                Else
                    Me.OnOpened()
                End If
            End If
        Catch ex As OperationFailedException
            Throw
        Catch ex As VisaException
            Me.TryCloseSession()
            Throw New OperationFailedException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                             "VISA exception occurred opening session to '{0}'.", resourceName),
                                                         ex)
        Catch ex As Exception
            Me.TryCloseSession()
            Throw New OperationFailedException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                             "Exception occurred opening session to '{0}'.", resourceName),
                                                         ex)
        End Try
    End Sub

    ''' <summary> Try open session. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> <c>True</c> if successful <see cref="VisaNS.VisaStatusCode">VISA Status Code</see>
    ''' was returned; <c>False</c> otherwise. </returns>
    Public Overridable Function TryOpenSession(ByVal resourceName As String) As Boolean
        Try
            Me.OpenSession(resourceName)
        Catch ex As OperationFailedException
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                       "Exception occurred opening session;. Details: {0}", ex)
            Return False
        End Try
        Return Me.IsDeviceOpen
    End Function

    ''' <summary> Allows the derived device to take actions before closing. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    ''' <remarks> This override should occur as the first call of the overriding method. </remarks>
    Protected Overridable Sub OnClosing(ByVal e As ComponentModel.CancelEventArgs)
        Me.SuspendPublishing()
        Me.SyncNotifyClosing(System.EventArgs.Empty)
    End Sub

    ''' <summary> Allows the derived device to take actions after closing. </summary>
    ''' <remarks> This override should occur as the last call of the overriding method. </remarks>
    Protected Overridable Sub OnClosed()
        Me.ResumePublishing()
        If Me.Enabled Then
            MyBase.AsyncNotifyPropertyChanged("IsSessionOpen")
        End If
        Me.IsDeviceOpen = False
        Me.SyncNotifyClosed(System.EventArgs.Empty)
    End Sub

    ''' <summary> Closes the session. </summary>
    ''' <exception cref="OperationFailedException"> Thrown when operation failed to execute. </exception>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overridable Sub CloseSession()
        Try
            Dim e As New ComponentModel.CancelEventArgs
            Me.OnClosing(e)
            If e.Cancel Then Return
            If Me.Enabled Then
                If Me.IsSessionOpen Then
                    Me.DisableServiceRequestEventHandler()
                    Me.Session.Dispose()
                    Try
                        ' Trying to null the session raises an ObjectDisposedException 
                        ' if session service request handler was not released. 
                        Me.Session = Nothing
                    Catch ex As ObjectDisposedException
                        Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                    End Try
                    Me.OnClosed()
                End If
            Else
                Me.OnClosed()
            End If
        Catch ex As VisaException
            Throw New OperationFailedException("VISA Exception occurred closing the session.", ex)
        Catch ex As Exception
            Throw New OperationFailedException("Exception occurred closing the session.", ex)
        End Try
    End Sub

    ''' <summary> Try close session. </summary>
    ''' <returns> <c>True</c> if session closed; otherwise <c>False</c>. </returns>
    Public Overridable Function TryCloseSession() As Boolean
        Try
            Me.CloseSession()
        Catch ex As OperationFailedException
            OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred closing session;. Details: {0}", ex)
            Return False
        End Try
        Return Not Me.IsDeviceOpen
    End Function

#End Region

#Region " SESSION: PROPERTY CHANGES AND MESSAGES EVENTS  "

    Private _SessionPropertyChangeHandlerEnabled As Boolean

    ''' <summary> Gets or sets the session property change handler enabled. </summary>
    ''' <value> The session property change handler enabled. </value>
    Public Property SessionPropertyChangeHandlerEnabled As Boolean
        Get
            Return _SessionPropertyChangeHandlerEnabled
        End Get
        Set(value As Boolean)
            If Me.IsSessionOpen Then
                If Not value.Equals(Me.SessionPropertyChangeHandlerEnabled) Then
                    If value Then
                        AddHandler Me.Session.PropertyChanged, AddressOf Me.SessionPropertyChanged
                    Else
                        RemoveHandler Me.Session.PropertyChanged, AddressOf Me.SessionPropertyChanged
                    End If
                    Me._SessionPropertyChangeHandlerEnabled = value
                    Me.AsyncNotifyPropertyChanged("SessionPropertyChangeHandlerEnabled")
                End If
            End If
        End Set
    End Property

    Private _SessionMessagesTraceEnabled As Boolean

    ''' <summary> Gets or sets the session messages trace enabled. </summary>
    ''' <value> The session messages trace enabled. </value>
    Public Property SessionMessagesTraceEnabled As Boolean
        Get
            Return Me._SessionMessagesTraceEnabled
        End Get
        Set(value As Boolean)
            If Not value.Equals(Me.SessionMessagesTraceEnabled) Then
                Me._SessionMessagesTraceEnabled = value
                Me.AsyncNotifyPropertyChanged("SessionMessagesTraceEnabled")
            End If
        End Set
    End Property

    ''' <summary> Session property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub SessionPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Try
            If sender IsNot Nothing AndAlso
                e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
                Dim session As Session = CType(sender, Session)
                Select Case e.PropertyName
                    Case "LastMessageReceived"
                        Dim value As String = session.LastMessageReceived
                        If Me.SessionMessagesTraceEnabled AndAlso Not String.IsNullOrWhiteSpace(value) Then
                            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "{0} sent: '{1}'.", Me.ResourceName,
                                                       isr.Core.Primitives.StringEscapeSequencesExtensions.InsertCommonEscapeSequences(value))
                        End If
                    Case "LastMessageSent"
                        Dim value As String = session.LastMessageSent
                        If Me.SessionMessagesTraceEnabled AndAlso Not String.IsNullOrWhiteSpace(value) Then
                            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "{0} received: '{1}'.", Me.ResourceName, value)
                        End If
                End Select
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling property '{0}'. Details: {1}.",
                         e.PropertyName, ex.Message)
        End Try
    End Sub

#End Region

#Region " SESSION: SERVICE REQUEST MANAGEMENT "

    Private _ServiceRequestEventHandlerEnabled As Boolean
    ''' <summary> Gets the service request event handler enabled. </summary>
    ''' <value> The service request event handler enabled. </value>
    Public Property ServiceRequestEventHandlerEnabled As Boolean
        Get
            Return _ServiceRequestEventHandlerEnabled
        End Get
        Set(value As Boolean)
            If Not Boolean.Equals(value, Me.ServiceRequestEventHandlerEnabled) Then
                Me._ServiceRequestEventHandlerEnabled = value
                Me.AsyncNotifyPropertyChanged("ServiceRequestEventHandlerEnabled")
            End If
        End Set
    End Property

    Private _UsingSyncServiceRequestHandler As Boolean

    ''' <summary> Gets or sets the sentinel indicating how service request is handled: Synchronously or Asynchronously. </summary>
    ''' <value>  <c>True</c> if service request is handled Synchronously; <c>False</c> if Asynchronously. </value>
    Public Property UsingSyncServiceRequestHandler As Boolean
        Get
            Return Me._UsingSyncServiceRequestHandler
        End Get
        Set(value As Boolean)
            If value <> Me.UsingSyncServiceRequestHandler Then
                If Me.IsSessionOpen Then
                    If Me.UsingSyncServiceRequestHandler Then
                        RemoveHandler Me.Session.ServiceRequest, AddressOf Me.SyncSessionServiceRequestHandler
                    Else
                        RemoveHandler Me.Session.ServiceRequest, AddressOf Me.AsyncSessionServiceRequestHandler
                    End If
                End If
                Me._UsingSyncServiceRequestHandler = value
                If Me.IsSessionOpen Then
                    If Me.UsingSyncServiceRequestHandler Then
                        AddHandler Me.Session.ServiceRequest, AddressOf Me.SyncSessionServiceRequestHandler
                    Else
                        AddHandler Me.Session.ServiceRequest, AddressOf Me.AsyncSessionServiceRequestHandler
                    End If
                End If
                Me.AsyncNotifyPropertyChanged("UsingSyncServiceRequestHandler")
            End If
        End Set
    End Property

    ''' <summary> Enable service request event handler. </summary>
    Public Sub EnableServiceRequestEventHandler()
        If Me.IsDeviceOpen AndAlso Not Me.ServiceRequestEventHandlerEnabled Then
            Me._ServiceRequestEventHandlerEnabled = True
            If Me.IsSessionOpen Then
                If Me.UsingSyncServiceRequestHandler Then
                    AddHandler Me.Session.ServiceRequest, AddressOf Me.SyncSessionServiceRequestHandler
                Else
                    AddHandler Me.Session.ServiceRequest, AddressOf Me.AsyncSessionServiceRequestHandler
                End If
                Me.Session.EnableEvent(VisaNS.MessageBasedSessionEventType.ServiceRequest, VisaNS.EventMechanism.Handler)
            End If
        End If
    End Sub

    ''' <summary> Disable service request event handler. </summary>
    Public Sub DisableServiceRequestEventHandler()
        If Me.IsDeviceOpen AndAlso Me.ServiceRequestEventHandlerEnabled Then
            Me._ServiceRequestEventHandlerEnabled = False
            If Me.IsSessionOpen Then
                If Me.UsingSyncServiceRequestHandler Then
                    RemoveHandler Me.Session.ServiceRequest, AddressOf Me.SyncSessionServiceRequestHandler
                Else
                    RemoveHandler Me.Session.ServiceRequest, AddressOf Me.AsyncSessionServiceRequestHandler
                End If
                Me.Session.DisableEvent(VisaNS.MessageBasedSessionEventType.ServiceRequest, VisaNS.EventMechanism.Handler)
            End If
        End If
    End Sub

#End Region

#Region " I TRACE MESSAGE PUBLISHER "

    ''' <summary> Raises the core. diagnosis. trace message event received from another source. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    Public Overloads Sub OnTraceMessageAvailable(ByVal sender As Object, ByVal e As Core.Diagnosis.TraceMessageEventArgs)
        If e IsNot Nothing Then
            Me.OnTraceMessageAvailable(e.TraceMessage)
        End If
    End Sub

#End Region

#Region " SUBSYSTEMS "

    ''' <summary> Adds a subsystem. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub AddSubsystem(ByVal value As SubsystemBase)
        Me.Subsystems.Add(value)
    End Sub

    Private _Subsystems As PresettablePublisherCollection
    ''' <summary> Enumerates the Presettable subsystems. </summary>
    Protected ReadOnly Property Subsystems As PresettablePublisherCollection
        Get
            Return Me._Subsystems
        End Get
    End Property

#End Region

#Region " EVENTS: SERVICE REQUESTED "

    Public Property ServiceRequestFailureMessage As String

    ''' <summary> Reads the event registers after receiving a service request. </summary>
    Protected MustOverride Sub ProcessServiceRequest()

    ''' <summary> Reads the event registers after receiving a service request. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Function TryProcessServiceRequest() As Boolean
        Try
            Me.ProcessServiceRequest()
            If Me.IsSessionOpen Then
                If Me.Session.LastStatus <> VisaNS.VisaStatusCode.Success Then
                    Me.ServiceRequestFailureMessage = Me.Session.BuildVisaStatusDetails
                End If
                Return Me.Session.LastStatus >= VisaNS.VisaStatusCode.Success
            End If
            Return True
        Catch ex As VisaNS.VisaException
            Me.ServiceRequestFailureMessage = Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                                                         "Visa Exception occurred processing visa service request. VISA status={0}. Details: {1}.",
                                                                         ex.ErrorCode, ex).Details
            Return False
        Catch ex As Exception
            Me.ServiceRequestFailureMessage = Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                                                         "Exception occurred processing visa service request. Details: {0}.",
                                                                         ex).Details
            Return False
        End Try
    End Function

    ''' <summary> Occurs when service is requested. </summary>
    Public Event ServiceRequested As EventHandler(Of VisaNS.MessageBasedSessionEventArgs)

#Region " INVOKE "

    ''' <summary> Synchronously Invokes the <see cref="ServiceRequested">Service Requested Event</see>. Must
    ''' be called with the <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="VisaNS.MessageBasedSessionEventArgs" /> instance containing the event data. </param>
    Private Sub InvokeServiceRequested(ByVal e As VisaNS.MessageBasedSessionEventArgs)
        Dim evt As EventHandler(Of VisaNS.MessageBasedSessionEventArgs) = Me.ServiceRequestedEvent
        If evt IsNot Nothing Then evt.Invoke(Me, e)
    End Sub

    ''' <summary> Synchronously Invokes the <see cref="ServiceRequested">Service Requested Event</see>. Must
    ''' be called with the <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub InvokeServiceRequested(ByVal obj As Object)
        Me.InvokeServiceRequested(CType(obj, VisaNS.MessageBasedSessionEventArgs))
    End Sub

#End Region

#Region " SAFE EVENTS "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) an
    ''' <see cref="EventHandler(Of VisaNS.MessageBasedSessionEventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    Private Overloads Sub SafeInvoke(ByVal handler As EventHandler(Of VisaNS.MessageBasedSessionEventArgs), ByVal e As VisaNS.MessageBasedSessionEventArgs)
        If handler IsNot Nothing Then
            For Each d As [Delegate] In handler.GetInvocationList
                If d.Target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, e})
                Else
                    Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    If target Is Nothing Then
                        d.DynamicInvoke(New Object() {Me, e})
                    Else
                        target.Invoke(d, New Object() {Me, e})
                    End If
                End If
            Next
        End If
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies an 
    ''' <see cref="EventHandler(Of VisaNS.MessageBasedSessionEventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    Private Overloads Sub SafeBeginInvoke(ByVal handler As EventHandler(Of VisaNS.MessageBasedSessionEventArgs), ByVal e As VisaNS.MessageBasedSessionEventArgs)
        If handler IsNot Nothing Then
            For Each d As [Delegate] In handler.GetInvocationList
                If d.Target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, e})
                Else
                    Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    If target Is Nothing Then
                        d.DynamicInvoke(New Object() {Me, e})
                    Else
                        target.BeginInvoke(d, New Object() {Me, e})
                    End If
                End If
            Next
        End If
    End Sub

#End Region

#Region " SYNCHRONOUS HANDLER "

    ''' <summary> Synchronously handles the Service Request event of the <see cref="Session">session</see> control. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="VisaNS.MessageBasedSessionEventArgs" /> instance
    ''' containing the event data. </param>
    Private Sub SyncSessionServiceRequestHandler(ByVal sender As Object, ByVal e As VisaNS.MessageBasedSessionEventArgs)
        Me.TryProcessServiceRequest()
        If DeviceBase.CurrentSyncContext Is Nothing Then
            ' Even though the current sync context is nothing, one of the targets might 
            ' still require invocation. Therefore, save invoke is implemented.
            Me.SafeInvoke(Me.ServiceRequestedEvent, e)
        Else
            DeviceBase.CurrentSyncContext.Send(New SendOrPostCallback(AddressOf InvokeServiceRequested), e)
        End If
    End Sub

#End Region

#Region " ASYNCHRONOUS HANDLER "

    ''' <summary> Asynchronously handles the Service Request event of the
    ''' <see cref="Session">session</see> control. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="VisaNS.MessageBasedSessionEventArgs" /> instance
    ''' containing the event data. </param>
    Private Sub AsyncSessionServiceRequestHandler(ByVal sender As Object, ByVal e As VisaNS.MessageBasedSessionEventArgs)
        Me.TryProcessServiceRequest()
        If DeviceBase.CurrentSyncContext Is Nothing Then
            ' Even though the current sync context is nothing, one of the targets might 
            ' still require invocation. Therefore, save invoke is implemented.
            Me.SafeBeginInvoke(Me.ServiceRequestedEvent, e)
        Else
            DeviceBase.CurrentSyncContext.Post(New SendOrPostCallback(AddressOf InvokeServiceRequested), e)
        End If
    End Sub

#End Region

#End Region

#Region " DEVICE OPEN AND CLOSE EVENTS "

#Region " OPENING "

    ''' <summary> Occurs when Opening. </summary>
    Public Event Opening As EventHandler(Of System.EventArgs)

    ''' <summary> Safely and synchronously <see cref="SynchronizationContext.Send">sends</see> or
    ''' invokes the <see cref="Opening">Opening Event</see>. </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub SyncNotifyOpening(ByVal e As System.EventArgs)
        If DeviceBase.CurrentSyncContext Is Nothing Then
            ' Even though the current sync context is nothing, one of the targets might 
            ' still require invocation. Therefore, save invoke is implemented.
            MyBase.SafeInvoke(Me.OpeningEvent)
        Else
            DeviceBase.CurrentSyncContext.Send(New SendOrPostCallback(AddressOf Me.InvokeOpening), e)
        End If
    End Sub

#Region " INVOKE "

    ''' <summary> Synchronously Invokes the <see cref="Opening">Opening Event</see>. Must be
    ''' called with the <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing
    ''' the event data. </param>
    Private Sub InvokeOpening(ByVal e As System.EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.OpeningEvent
        If evt IsNot Nothing Then evt.Invoke(Me, e)
    End Sub

    ''' <summary> Synchronously Invokes the <see cref="Opening">Opening Event</see>. Must be
    ''' called with the <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub InvokeOpening(ByVal obj As Object)
        Me.InvokeOpening(CType(obj, System.EventArgs))
    End Sub

#End Region

#End Region

#Region " OPENED "

    ''' <summary> Occurs when Opened. </summary>
    Public Event Opened As EventHandler(Of System.EventArgs)

    ''' <summary> Safely and synchronously <see cref="SynchronizationContext.Send">sends</see> or
    ''' invokes the <see cref="Opened">Opened Event</see>. </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub SyncNotifyOpened(ByVal e As System.EventArgs)
        If DeviceBase.CurrentSyncContext Is Nothing Then
            ' Even though the current sync context is nothing, one of the targets might 
            ' still require invocation. Therefore, save invoke is implemented.
            MyBase.SafeInvoke(Me.OpenedEvent)
        Else
            DeviceBase.CurrentSyncContext.Send(New SendOrPostCallback(AddressOf Me.InvokeOpened), e)
        End If
    End Sub

#Region " INVOKE "

    ''' <summary> Synchronously Invokes the <see cref="Opened">Opened Event</see>. Must be
    ''' called with the <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing
    ''' the event data. </param>
    Private Sub InvokeOpened(ByVal e As System.EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.OpenedEvent
        If evt IsNot Nothing Then evt.Invoke(Me, e)
    End Sub

    ''' <summary> Synchronously Invokes the <see cref="Opened">Opened Event</see>. Must be
    ''' called with the <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub InvokeOpened(ByVal obj As Object)
        Me.InvokeOpened(CType(obj, System.EventArgs))
    End Sub

#End Region

#End Region

#Region " CLOSING "

    ''' <summary> Occurs when Closing. </summary>
    Public Event Closing As EventHandler(Of System.EventArgs)

    ''' <summary> Safely and synchronously <see cref="SynchronizationContext.Send">sends</see> or
    ''' invokes the <see cref="Closing">Closing Event</see>. </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub SyncNotifyClosing(ByVal e As System.EventArgs)
        If DeviceBase.CurrentSyncContext Is Nothing Then
            ' Even though the current sync context is nothing, one of the targets might 
            ' still require invocation. Therefore, save invoke is implemented.
            MyBase.SafeInvoke(Me.ClosingEvent)
        Else
            DeviceBase.CurrentSyncContext.Send(New SendOrPostCallback(AddressOf Me.InvokeClosing), e)
        End If
    End Sub

#Region " INVOKE "

    ''' <summary> Synchronously Invokes the <see cref="Closing">Closing Event</see>. Must be
    ''' called with the <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing
    ''' the event data. </param>
    Private Sub InvokeClosing(ByVal e As System.EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.ClosingEvent
        If evt IsNot Nothing Then evt.Invoke(Me, e)
    End Sub

    ''' <summary> Synchronously Invokes the <see cref="Closing">Closing Event</see>. Must be
    ''' called with the <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub InvokeClosing(ByVal obj As Object)
        Me.InvokeClosing(CType(obj, System.EventArgs))
    End Sub

#End Region

#End Region

#Region " CLOSED "

    ''' <summary> Occurs when Closed. </summary>
    Public Event Closed As EventHandler(Of System.EventArgs)

    ''' <summary> Safely and synchronously <see cref="SynchronizationContext.Send">sends</see> or
    ''' invokes the <see cref="Closed">Closed Event</see>. </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub SyncNotifyClosed(ByVal e As System.EventArgs)
        If DeviceBase.CurrentSyncContext Is Nothing Then
            ' Even though the current sync context is nothing, one of the targets might 
            ' still require invocation. Therefore, save invoke is implemented.
            MyBase.SafeInvoke(Me.ClosedEvent)
        Else
            DeviceBase.CurrentSyncContext.Send(New SendOrPostCallback(AddressOf Me.InvokeClosed), e)
        End If
    End Sub

#Region " INVOKE "

    ''' <summary> Synchronously Invokes the <see cref="Closed">Closed Event</see>. Must be
    ''' called with the <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing
    ''' the event data. </param>
    Private Sub InvokeClosed(ByVal e As System.EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.ClosedEvent
        If evt IsNot Nothing Then evt.Invoke(Me, e)
    End Sub

    ''' <summary> Synchronously Invokes the <see cref="Closed">Closed Event</see>. Must be
    ''' called with the <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub InvokeClosed(ByVal obj As Object)
        Me.InvokeClosed(CType(obj, System.EventArgs))
    End Sub

#End Region

#End Region

#End Region

End Class
