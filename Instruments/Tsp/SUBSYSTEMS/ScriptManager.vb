﻿''' <summary> Script Manager. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="12/14/2013" by="David" revision=""> Created. </history>
Public Class ScriptManager
    Inherits isr.IO.Visa.Tsp.ScriptManagerBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Initializes a new instance of the <see cref="ScriptManager" /> class. </summary>
    ''' <param name="statusSubsystem">  A reference to a <see cref="IO.Visa.Tsp.StatusSubsystemBase">status
    ''' subsystem</see>. </param>
    ''' <param name="displaySubsystem"> A reference to a <see cref="IO.Visa.Tsp.DisplaySubsystemBase">display
    ''' subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As IO.Visa.Tsp.StatusSubsystemBase, ByVal displaySubsystem As IO.Visa.Tsp.DisplaySubsystemBase)
        MyBase.New(statusSubsystem, displaySubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets subsystem values to their known execution clear state. </summary>
    Public Overrides Sub ClearExecutionState()
    End Sub

    ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
    Public Overrides Sub ResetKnownState()
    End Sub

#End Region

#Region " PUBLISHER "

    ''' <summary> Publishes all values by raising the property changed events. </summary>
    Public Overrides Sub Publish()
        If Me.Publishable Then
            For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                Me.AsyncNotifyPropertyChanged(p.Name)
            Next
        End If
    End Sub

#End Region

#Region " COMMAND SYNTAX "

    ''' <summary> Gets the preset command. </summary>
    ''' <value> The preset command. </value>
    Protected Overrides ReadOnly Property PresetCommand As String
        Get
            Return ""
        End Get
    End Property

#End Region

End Class
