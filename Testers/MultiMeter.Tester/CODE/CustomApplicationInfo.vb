﻿Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Shared ReadOnly TraceEventId As Integer = isr.Core.Diagnosis.TraceEventIds.IsrIOVisaMultimeterTester

        Public Const AssemblyTitle As String = ""
        Public Const AssemblyDescription As String = ""
        Public Const AssemblyProduct As String = ""

    End Class

End Namespace

